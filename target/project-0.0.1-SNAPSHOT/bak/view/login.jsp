<%@page pageEncoding="utf-8"  contentType="text/html; charset=utf-8"%>
<HTML lang=en>
<HEAD>
	<TITLE>HUNDSUN LOGIN SERVICE</TITLE>
	<META charset=UTF-8>
	<!--LINK rel=icon type=image/x-icon href="/cas/favicon.ico"-->
	<LINK rel=stylesheet type=text/css href="css/login.css">
	<!--LINK rel=stylesheet type=text/css href="css/jquerycss/jquery-ui.css">
	<LINK rel=stylesheet type=text/css href="css/ie_cas.css">
	<SCRIPT type=text/javascript src="js/common_rosters.js"></SCRIPT>
	<SCRIPT type=text/javascript src="js/jquery.js"></SCRIPT>
	<SCRIPT type=text/javascript src="js/jquery-ui.js"></SCRIPT-->
</HEAD>
<BODY>
	<FORM id=fm1 class="fm-v clearfix" method=post action=login.do>
		<DIV id=main_login>
			<DIV id=logo>
				<A id=logoImge></A>
			</DIV>
			<DIV id=input_name>
				<INPUT accessKey=n id=username tabIndex=1 type=text name=username autocomplete="off"> 
			</DIV>
			<DIV id=input_psd>
				<INPUT accessKey=p id=password tabIndex=2 value="" type=password name=password autocomplete="off"> 
			</DIV>
			<DIV id=bottom>
				<INPUT accessKey=l class=btn-submit tabIndex=3 type=submit name=submit> 
			</DIV>
			<DIV id=login_message>
				<span class=message>${login_failed}</span>
			</DIV>
		</DIV>
	</FORM>
</BODY>
</HTML>  