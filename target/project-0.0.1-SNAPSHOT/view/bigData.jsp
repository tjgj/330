<%@page pageEncoding="utf-8"  contentType="text/html; charset=utf-8"%>
<html class="no-js">
<head>
<meta charset="UTF-8" />
<title>统计中心</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="css/reset.css" rel="stylesheet" />
<link href="css/main.css" rel="stylesheet" />
<link rel="stylesheet" href="css/bootstrap.min.css"/>
<script src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
	    //加载初始化数据
	    loadInit();
	    
		$("button").click(function(e){
			var domId = e.target.id;
			var topicId = domId.split("_")
			if(topicId[0]=='B'){
				window.location.href = "toWriteTopic.do?id="+topicId[1];				
			}else{
				window.location.href = "toCheckTopic.do?id="+topicId[1];
			}

		});
	});
	//加载初始化数据
	function loadInit(){
		$.ajaxSettings.async = false; //设置为同步，不然button的onclick事件无法绑定上去
		$.post("getBigTopic.do",function(data){
			for(i = 0; i < data.length; i++){
				var s = data[i];
				$('#tb1').append('<tr class="text-center"><td>' + s.id + '</td><td>' + s.release_time + '</td><td>'
						+ s.topic_name + '</td><td>' + s.complete_time + '</td><td  style="width:200px" colspan="4" class="text-right">'
						+ '<div style="width:90px;float:left"><button id=B_'+ s.id +' class="btn btn-danger"  style="height:35px;width:80px;">编写</button></div>'
						+'<div style="width:90px;float:left"><button id=C_'+ s.id +' class="btn btn-danger"  style="height:35px;width:80px;">查询</button></div></td></tr>');
			}}
		, 'json');
	}

</script>
</head>
<body>
<div class="i_header header">
	<div class="wrap">
		<!--div class="logo"><img src="images/logo.jpg"/></a></div-->
		<div class="nav">
			<ul>
				<form name='index_form' action='toTopicIndex.do' method='post'>  
					<li><a href='toNewTopic.do'>发起统计</a></li>
					<li><a href='toCustomers.do'>客户关系</a></li>
					<li><a href='toBigData.do'>大数据</a></li>
					<li><a href='javascript:document.index_form.submit();' title="首页">首页</a></li>
					<li><a>${user_name}</a></li>
				</form> 
			</ul>
		</div>
	</div>
</div>
<div class="h30"></div>
<div class="main" style="text-align:center;">
	<table id="table1" class="table table-bordered table-hover">
	<thead>
	<tr align="center" valign="middle">
		<td>序号</td>
		<td>统计日期</td>
		<td>统计名称</td>
		<td>截止日期</td>
		<td style="width:100px">&nbsp</td>
    </tr> 
    </thead>   
    <tbody id="tb1" align="center" valign="middle">
    </tbody> 
</table>
</div>
<div class="h90"></div>
<div style="text-align:center;">
	<p>版权所有：资管PB实施团队</a></p>
</div>
</body>
</html>