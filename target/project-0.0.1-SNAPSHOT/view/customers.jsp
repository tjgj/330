<%@page pageEncoding="utf-8"  contentType="text/html; charset=utf-8"%>
<html lang=en>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>客户关系</title>
    <link rel="stylesheet" href="css/bootstrap-3.3.4.css"/>
    <link href="css/reset.css" rel="stylesheet" />
	<link href="css/main2.css" rel="stylesheet" />
	
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/Tabullet.js"></script>

	<script type="text/javascript" src="js/excelJs/jszip.js"></script>
	<script type="text/javascript" src="js/excelJs/FileSaver.js"></script>
	<script type="text/javascript" src="js/excelJs/excel-gen.js"></script>
	<link href="http://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="css/demo.css">
	
    <style>
    body {
        background-color: #fafafa;
    }

    .container {
        margin: 150px auto;
    }
    </style>
	
    <script type="text/javascript">
    $(function() {    	
        $.ajax({
            type: "POST",
            url: "toAllCustomer.do",
            traditional: true,
            data:{
            	username:"吴俊"
            }, 
            cache: false,
            async: false,
            //dataType: "json", 后台不是纯正的json，所以不太敢加这个参数。
            beforeSend: function () {
                	//alert("begin");
            },
            success: function (data) {
            	//alert("success");
            	jsonstr = data.toString();
            	//alert(jsonstr);
            },
            error: function (errorThrown) {
            	//alert("stop");
            	alert(errorThrown.readystate);
            	alert(errorThrown.status);
            }
        });
        
    	//var source = JSON.parse(jsonstr);格式要求高
    	var source = eval("(" + jsonstr + ")");//要添加"("")"不然不行
        //source = [{customerId:"100",customerName:"海通证券",customerArea:"华东",system:"PB",systemVersion:"PB_20170331B",firstCommissioner:"国栋",secondCommissioner:"吴俊"},{customerId:"100",customerName:"兴业证券",customerArea:"华东",system:"PB",systemVersion:"PB_20170331B",firstCommissioner:"国栋",secondCommissioner:"吴俊"},];
        //alert(source); 验证控件需求的数据是json，返回object，字符串不行
    	function resetTabullet() {
            $("#tableForExcel").tabullet({
                data: source,
                action: function(mode, data) {
                    console.dir(mode);
                    if (mode === 'save') {
                    	//data.id = 111;
                        //alert(data.id);
                          $.ajax({
                            type: "POST",
                            url: "toSaveCustomer.do",
                            traditional: true,
                            data:{
                            	//postData:JSON.stringify(data),//JSON字符串数组,没有成功，data不是josn???
                        		postCustomerId:data.customerId,
                      	 	    postCustomerName:data.customerName,
                     		    postCustomerArea:data.customerArea,
                      			postSystem:data.system,
                        		postSystemVersion:data.systemVersion,
                       			postFirstCommissioner:data.firstCommissioner,
                                postSecondCommissioner:data.secondCommissioner,
                            	username:"吴俊"
                            },
                            cache: false,
                            async: false,//设置为同步
                            //dataType: "json", 
                            beforeSend: function () {
                                //alert("begin");
                            },
                            success: function (dataId) {
                            	//alert("success");
                            	var idInt = parseInt(dataId);
                            	alert(idInt);
                            	data.id = idInt;
                            },
                            error: function (errorThrown) {
                            	alert("AJAX-ERROR");
                            	alert(errorThrown.readystate);
                            	alert(errorThrown.status);
                            }
                        }); 
                        //alert(data.customerName);
                        source.push(data);
                    }
                    if (mode === 'edit') {
                        for (var i = 0; i < source.length; i++) {
                            if (source[i].id == data.id) {
                                $.ajax({
                                    type: "POST",
                                    url: "toUpdateCustomer.do",
                                    traditional: true,
                                    data:{
                                    	//postData:JSON.stringify(data),//JSON字符串数组,没有成功，data不是josn???
                                        postId:data.id,
                                		postCustomerId:data.customerId,
                              	 	    postCustomerName:data.customerName,
                             		    postCustomerArea:data.customerArea,
                              			postSystem:data.system,
                                		postSystemVersion:data.systemVersion,
                               			postFirstCommissioner:data.firstCommissioner,
                                        postSecondCommissioner:data.secondCommissioner,
                                    	username:"吴俊"
                                    },
                                    cache: false,
                                    async: false,//设置为同步
                                    //dataType: "json", 
                                    beforeSend: function () {
                                        //alert("begin");
                                    },
                                    success: function (dataId) {
                                    	//alert("success");
                                    	var idInt = parseInt(dataId);
                                    	alert(idInt);
                                    },
                                    error: function (errorThrown) {
                                    	alert("AJAX-ERROR");
                                    	alert(errorThrown.readystate);
                                    	alert(errorThrown.status);
                                    }
                                });                             	
                                source[i] = data;
                                //alert(data.id);
                                //alert(source[i].customerName);
                            }
                        }
                    }
                    if (mode == 'delete') {
                        for (var i = 0; i < source.length; i++) {
                            if (source[i].id == data) {
                                //alert(source[i].id);
                                $.ajax({
                                    type: "POST",
                                    url: "toDeleteCustomer.do",
                                    traditional: true,
                                    data:{
                                		postId:source[i].id,                        
                                    	username:"吴俊"
                                    },
                                    cache: false,
                                    async: false,
                                    //dataType: "json", 
                                    beforeSend: function () {
                                        //alert("begin");
                                    },
                                    success: function (dataId) {
                                    	//alert("success");
                                    	alert("删除成功，后台编号："+dataId);
                                    },
                                    error: function (errorThrown) {
                                    	//alert("AJAX-ERROR");
                                    	alert(errorThrown.readystate);
                                    	alert(errorThrown.status);
                                    }
                                }); 
                                //alert(source[i].customerName);
                                source.splice(i, 1);
                                break;
                            }
                        }
                    }
                    resetTabullet();
                    //重新给excel赋值，不然导出缺少修改后的数据
                    excel = new ExcelGen({
                        "src_id": "tableForExcel",
                        "show_header": true
                    });
                }
            });
        }
        resetTabullet();
        
        //调用excel控件
        //一定要放在tabullet控件后面，必然界面还没有加载完DOM元素，导出是空表
        excel = new ExcelGen({
            "src_id": "tableForExcel",
            "show_header": true
        });
        $("#generate-excel").click(function () {
            resetTabullet();
            excel.generate();
        });
        
    });
    </script>
</head>

<body>
<div class="i_header header">
	<div class="wrap">
		<!--div class="logo"><img src="images/logo.jpg"/></a></div-->
		<div class="nav">
			<ul>
				<form name='index_form' action='toTopicIndex.do' method='post'>  
					<li><a href='toNewTopic.do'>发起统计</a></li>
					<li><a href='toCustomers.do'>客户关系</a></li>
					<li><a href='toBigData.do'>大数据</a></li>
					<li><a href='javascript:document.index_form.submit();' title="首页">首页</a></li>
					<li><a>${user_name}</a></li>
				</form> 
			</ul>
		</div>
	</div>
</div>
<div class="container">
    <div class="row">
    	<div class="col-md-3" style="padding:2em 0;">
			<button type="button" class="btn btn-success btn-block" id="generate-excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> 将表格转换为Excel</button>
		</div>
        <div class="col-sm-12">
            <table class="table table-hover" id="tableForExcel">
                <thead>
                    <tr data-tabullet-map="id">
                        <th width="50" data-tabullet-map="_index" data-tabullet-readonly="true">No</th>
                        <th data-tabullet-map="customerId">客户编号</th>
                        <th data-tabullet-map="customerName">客户名称</th>
                        <th data-tabullet-map="customerArea">客户片区</th>
                        <th data-tabullet-map="system">系统</th>
                        <th data-tabullet-map="systemVersion">系统版本</th>
                        <th data-tabullet-map="firstCommissioner">第一维护专员</th>
                        <th data-tabullet-map="secondCommissioner">第二维护专员</th>
                        <th width="50" data-tabullet-type="edit"></th>
                        <th width="50" data-tabullet-type="delete"></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
</body>
</html>
