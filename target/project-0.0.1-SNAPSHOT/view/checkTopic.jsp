<%@page pageEncoding="utf-8"  contentType="text/html; charset=utf-8"%>
<html lang=en>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>table</title>
    <link href="http://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="js/tableExcel/excel-bootstrap-table-filter-style.css" />
    <link rel="stylesheet" type="text/css" href="css/demoCheckTopic.css">
    <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="js/tableExcel/excel-bootstrap-table-filter-bundle.js"></script>

	<script type="text/javascript" src="js/excelJs/jszip.js"></script>
	<script type="text/javascript" src="js/excelJs/FileSaver.js"></script>
	<script type="text/javascript" src="js/excelJs/excel-gen.js"></script>	
	<link href="http://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

</head>
<body>
<div class="i_header header">
	<div class="wrap">
		<!--div class="logo"><img src="images/logo.jpg"/></a></div-->
		<div class="nav">
			<ul>
				<form name='index_form' action='toTopicIndex.do' method='post'>  
					<li><a href='toNewTopic.do'>发起统计</a></li>
					<li><a href='toCustomers.do'>客户关系</a></li>
					<li><a href='toBigData.do'>大数据</a></li>
					<li><a href='javascript:document.index_form.submit();' title="首页">首页</a></li>
					<li><a>${user_name}</a></li>
				</form> 
			</ul>
		</div>
	</div>
</div>
<div class="h130"></div>
     <div class="container" id="box">
         <div class="col-md-3" style="padding:2em 0;float:left;">
			<button type="button" class="btn btn-success btn-block" id="generate-excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> 将表格转换为Excel</button>
		 </div>
		 <div class="h3 text-info" style="float:left;margin-left:200px;">
		     <p id="topicId" hidden="hidden">${topic_id}</p>
             <caption id="topicName" class="h3 text-info">${topic_name}</caption> 
		 </div>
         <table id="table" class="table table-bordered table-intel">  
			  <thead id="th">
  			  </thead>   
              <tbody id="tb" align="center" valign="middle">
              </tbody> 
         </table>
     </div>
</body>
<script type="text/javascript">
$(function() {   
	var topicId = $("#topicId").text(); 
	//获取统计ID的所有统计信息
    $.ajax({
        type: "POST",
        url: "toCheckTopicData.do",
        traditional: true,
        data:{
        	topicId:topicId
        }, 
        cache: false,
        async: false,
        beforeSend: function () {
            	//alert("begin");
        },
        success: function (data) {
        	var thStr = '';
        	var tbStr = '';
        	thStr = thStr + '<tr>';
        	for(i = 0; i < data[0].length; i++){
        		//alert(data[0][i]);
        		thStr = thStr + '<th class="filter" style="min-width:200px;">'+data[0][i]+'</th>';
        	}
        	thStr = thStr + '</tr>';
        	$('#th').append(thStr);
        	
        	for(var j=1;j<data.length;j++){
        		tbStr = tbStr + '<tr>';
        		for(var index=0;index<data[j].length;index++){
        			//alert(data[j][index]);
        			tbStr = tbStr + '<td>'+data[j][index]+'</td>'
        		}
        		tbStr = tbStr + '</tr>';
        	}
        	$('#tb').append(tbStr);
        },
        error: function (errorThrown) {
        	//alert("stop");
        	alert(errorThrown.readystate);
        	alert(errorThrown.status);
        }
    });
	
    //导出excel控件
    excel = new ExcelGen({
        "src_id": "table",
        "show_header": true
    });
    $("#generate-excel").click(function () {
        excel.generate();
    });
    
	//界面excel展示控件
    $('#table').excelTableFilter({
    	'captions':{ a_to_z: '升序排列', z_to_a: '降序排列', search: '搜索', select_all: '全部选择' }
      });
    
});

</script>
</html>