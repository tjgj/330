<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<HTML lang=en>
<head>
    <base href="<%=basePath%>">
	<TITLE>NEW TOPIC</TITLE>
    <link rel="stylesheet" href="css/bootstrap.min.css"/>    
    <link rel="stylesheet" href="css/borain-timeChoice.css">  
    <script src="js/jquery-1.7.2.min.js"></script><!-- 1.10.2版本会导致一个功能丢失 -->>
    <script src="js/bootstrap.js"></script>
    <script src="js/vue.js"></script>
    <script src="js/vue-resource.min.js"></script>
    <script src="js/timeJs/borain-timeChoice.js"></script>
    
	<style>
.tags {
	background-color: #fff;
	border: 1px solid #d5d5d5;
	color: #777;
	padding: 4px 6px;
	width: 406px;
	margin:10px auto;
}
.tags:hover {
	border-color: #f59942;
	outline: 0 none;
}
.tags[class*="span"] {
	float: none;
	margin-left: 0;
}
.tags input[type="text"], .tags input[type="text"]:focus {
	border: 0 none;
	box-shadow: none;
	display: inline;
	line-height: 22px;
	margin: 0;
	outline: 0 none;
	padding: 4px 6px; 
}
.tags .tag {
	background-color: #91b8d0;
	color: #fff;
	display: inline-block;
	font-size: 12px;
	font-weight: normal;
	margin-bottom: 3px;
	margin-right: 3px;
	padding: 4px 22px 5px 9px;
	position: relative;
	text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.15);
	transition: all 0.2s ease 0s;
	vertical-align: baseline;
	white-space: nowrap;
}
.tags .tag .close {
	bottom: 0;
	color: #fff;
	float: none;
	font-size: 12px;
	line-height: 20px;
	opacity: 1;
	position: absolute;
	right: 0;
	text-align: center;
	text-shadow: none;
	top: 0;
	width: 18px;
}
.tags .tag .close:hover {
	background-color: rgba(0, 0, 0, 0.2);
}
.close {
	color: #000;
	float: right;
	font-size: 21px;
	font-weight: bold;
	line-height: 1;
	opacity: 0.2;
	text-shadow: 0 1px 0 #fff;
}
.close:hover, .close:focus {
	color: #000;
	cursor: pointer;
	opacity: 0.5;
	text-decoration: none;
}
button.close {
	background: transparent none repeat scroll 0 0;
	border: 0 none;
	cursor: pointer;
	padding: 0;
}
.tags .tag-warning {
	background-color: #ffb752;
}
</style>
    <script>
          window.onload = function(){
              new Vue({
                  el:'#box',
                  data:{
                     myData:[],
                     fieldname:"",
                     fieldcontent:"",
                     nowIndex:-100
                  },
                  methods:{
                     add:function(){
						  username = $("#fieldname").val();
						  var jionStr="";
						  var elem_child = $("#tags").children("span");
						  for(var i=0; i<elem_child.length;i++){
						  var iStr = elem_child[i].innerHTML.split("<");
						  if(i==elem_child.length-1){
						  jionStr = jionStr+iStr[0];
						  }else{
						  jionStr = jionStr+iStr[0]+";";
						  }
						  }
                          this.myData.push({
                              fieldname:username,
                              fieldcontent:jionStr//this.wjage
                          })
						  for(var i=0; i<elem_child.length;i++){
						  elem_child.empty();
						  elem_child.remove();
						  }
						  $("#fieldname").val("");
                     },
                      del:function(n){
                          if(n ==-2){
                              this.myData="";
                          }{
                              this.myData.splice(n,1);
                          }
                      }
                  }
              })
          }
    </script>
	<script type="text/javascript">
$(function() {
	$(".tags_enter").blur(function() { //焦点失去触发 
		var txtvalue=$(this).val().trim();
		if(txtvalue!=''){
			addTag($(this));
			$(this).parents(".tags").css({"border-color": "#d5d5d5"})
		}
	}).keydown(function(event) {
		var key_code = event.keyCode;
		var txtvalue=$(this).val().trim(); 
		if (key_code == 13&& txtvalue != '') { //enter
			addTag($(this));
		}
		if (key_code == 32 && txtvalue!='') { //space
			addTag($(this));
		}
	});
	$(".close").live("click", function() {
		$(this).parent(".tag").remove();
	});
	$(".tags").click(function() {
		$(this).css({"border-color": "#f59942"})
	}).blur(function() {
		$(this).css({"border-color": "#d5d5d5"})
	})
})
function addTag(obj) {
	var tag = obj.val();
	if (tag != '') {
		var i = 0;
		$(".tag").each(function() {
			if ($(this).text() == tag + "×") {
				$(this).addClass("tag-warning");
				setTimeout("removeWarning()", 400);
				i++;
			}
		})
		obj.val('');
		if (i > 0) { //说明有重复
			return false;
		}
		$("#fieldcontent").before("<span class='tag'>" + tag + "<button class='close' type='button'>×</button></span>"); //添加标签
	}
}
function removeWarning() {
	$(".tag-warning").removeClass("tag-warning");
}

function clearMsg(){
	var elem_child = $("#tags").children("span");
	for(var i=0; i<elem_child.length;i++){
	elem_child.empty();
	elem_child.remove();
}
	$("#fieldname").val("");
}
</script>
</head>
<body>
	<div class="i_header header">
		<div class="wrap">
			<div class="logo"><img src="images/logo.jpg"/></a></div>
			<div class="nav">
				<ul>
					<form name='index_form' action='toTopicIndex.do' method='post'>  
					<li><a href='toNewTopic.do'>发起统计</a></li>
					<li><a href='toCustomers.do'>客户关系</a></li>
					<li><a href='toBigData.do'>大数据</a></li>
					<li><a href='javascript:document.index_form.submit();' title="首页">首页</a></li>
					<li><a>${user_name}</a></li>
					</form> 
				</ul>
			</div>
		</div>
	</div>
	<div class="h130"></div>
    <div class="container" id="box">
	 <div class="tags" id="topic" tabindex="1" >
		     <input id="topicName" type="text" style="width:300px" placeholder="请输入统计主题 ..." />
     </div>	 
	 <div> 
			<div class="tags"  style="width:330px;float:left;">
	             <label for="completeTime">时间：</label>
                 <input type="text" style="width:250px" class="start-two" id="completeTime" placeholder="选择时间"/>
			</div>
			<div class="tags" tabindex="1" style="width:775px;float:right;margin-left:10px;">
	             <label for="remark">统计原因：</label>
                 <input type="text" style="width:600px" id="remark" placeholder="输入统计原因"/>
			</div>
     </div>
	 <div> 
			<div class="tags" style="width:330px;float:left;">
	             <label for="fieldname">字段：</label>
                 <input type="text" style="width:250px" id="fieldname" placeholder="输入字段"/>
			</div>
			<div class="tags" id="tags" tabindex="1" style="width:775px;float:right;margin-left:10px;">
	             <label for="fieldcontent">字段选择框：</label>
                 <input type="text" id="fieldcontent" class="form-control"  v-model="fieldcontent" placeholder="输入字段"  value="Tag Input Control" name="tags" style="display: none;"/>				 
				 <input type="text" placeholder="输入统计字段多选值项 ..." class="tags_enter" autocomplete="off"/>
			</div>

     </div>
     <form role="form">
             <div  style="clear:both" class="form-group">
                 <input type="button"  value="添加" class="btn btn-primary" v-on:click="add()"/>
                 <input type="button"  value="重置" class="btn btn-danger" onclick="clearMsg()" />
             </div>
      </form>
         <hr/>
         <table id="tbList" class="table table-bordered table-hover">
             <tr>
                 <th id="xuhao" class="text-center">序号</th>
                 <th class="text-center">字段名</th>
                 <th class="text-center">字段选项</th>
                 <th class="text-center">操作</th>
             </tr>
             <tr class="text-center" v-for="item in myData">
                 <td>{{$index+1}}</td>
                 <td>{{item.fieldname}}</td>
                 <td>{{item.fieldcontent}}</td>
                 <td>
                     <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#layer" v-on:click="nowIndex=$index">删除</button>
                 </td>
             </tr>
             <tr v-show="myData.length!=0">
                 <td colspan="4" class="text-right">
                     <button class="btn btn-danger"  data-toggle="modal" data-target="#layer" v-on:click="nowIndex=-2">删除全部</button>
					 <a class="btn btn-danger"  onclick="topicPost()">保存</a>
                 </td>
             </tr>
             <tr v-show="myData.length==0">
                 <td colspan="4" class="text-center text-info">
                     <p>暂无数据...</p>
                 </td>
             </tr>
         </table>
         <div style="float:right;margin-left:10px;">
      		<form method="POST"  enctype="multipart/form-data" id="form1" action="upload.do">
 	 		<table>
 	 	 	<tr>
 	 	 		<td>上传文件: </td>
 	 	 		<td> <input id="upfile" type="file" name="upfile"></td>
 	 		 </tr>
  			<tr>
  				<td>点击上传: </td>
 	 	 		<td><input type="submit" value="上传" onclick="return checkData()"></td>
 	 		 </tr>
  			</table>	
		</form>
	</div>
         <!--模态框 弹出框-->
         <div role="dialog" class="modal fade" id="layer" data-index="{{nowIndex}}">
         <div class="h250"></div>
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button class="close" data-dismiss="modal">
                              <span>&times;</span>
                          </button>
                          <h4 class="modal-title">确认删除吗？</h4>
                      </div>
                      <div class="modal-body text-right">
                          <button class="btn btn-primary btn-sm" data-dismiss="modal">取消</button>
                          <button class="btn btn-danger btn-sm" data-dismiss="modal" v-on:click="del(nowIndex)">确认</button>
                      </div>
                  </div>
              </div>
         </div>
     </div>
</body>
    <script type="text/javascript">
        function topicPost(){
   			//alert("hello");
   			//遍历获取table数据，返回数组
   			var tableData = [];
   			var mytable = document.getElementById("tbList");
   			for(var i=0,rows=mytable.rows.length; i<rows; i++){
   			  for(var j=0,cells=mytable.rows[i].cells.length; j<cells; j++){
   			    if(!tableData[i]){
   			      tableData[i] = new Array();
   			    }
   			    tableData[i][j] = mytable.rows[i].cells[j].innerHTML;
   			  }
   			}
   			//遍历数组数据为json
   			/*var tableData = [];
   			var mytable = document.getElementById("tbList");
   			for(var i=0,rows=mytable.rows.length; i<rows; i++){
   				var index = mytable.rows[i].cells[0].innerHTML;
   				var fieldname = mytable.rows[i].cells[1].innerHTML;
   				var fieldcontent = mytable.rows[i].cells[2].innerHTML;
   				var data1={"index":index,"fieldname":fieldname,"fieldcontent":fieldcontent};  
   			  	tableData.push(data1);
   			}*/
   			
   			//alert(tableData[1][1]);
   			var topicName = $("#topicName").val();
   			var completeTime = $("#completeTime").val();
   			var remark = $("#remark").val();
   			//var arry = ['1','2','3'];
   			//alert(topicName);
   			//alert(completeTime);
            $.ajax({
                type: "POST",
                url: "postTopic.do",
                traditional: true,
                data:{
                    data:tableData,
                    topicName:topicName,
                    completeTime:completeTime,
                    remark:remark
                }, 
                cache: false,
                async: false,
                beforeSend: function () {
                    	//alert("begin");
                },
                success: function (data) {
                    if (data != "") {
                        idList = data.split(',');
                        alert(idList[0]+idList[1]+idList[2]);
                    }else{
                    	alert("发起失败");
                    }
                	location.reload();
                },
                error: function (errorThrown) {
                	//alert("stop");
                	alert(errorThrown.readystate);
                	alert(errorThrown.status);
                }
            });
   			
   		 }
        
    //  level分为 YM YMD H HM 四个有效值，分别表示年月 年月日 年月日时 年月日时分,less表示是否不可小于当前时间。年-月-日 时:分 时为24小时制
    //  为确保控件结构只出现一次，在有需要的时候进行一次调用。
        onLoadTimeChoiceDemo();

        borainTimeChoice({
            start:".start-two",
            end:"",
            level:"H",
            less:false
        });
        
        //JS校验form表单信息
	     function checkData(){
	     	var fileDir = $("#upfile").val();
	     	var suffix = fileDir.substr(fileDir.lastIndexOf("."));
	     	if("" == fileDir){
	     		alert("选择需要导入的Excel文件！");
	     		return false;
	     	}
	     	if(".xls" != suffix && ".xlsx" != suffix ){
	     		alert("选择Excel格式的文件导入！");
	     		return false;
	     	}
	     	return true;
	     }
    </script>
</html>