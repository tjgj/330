<%@page pageEncoding="utf-8"  contentType="text/html; charset=utf-8"%>
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<title>HUNDSUN REGISTER</title>
<link rel="stylesheet" type="text/css" href="css/normalize.css" />
<link rel="stylesheet" type="text/css" href="css/demo.css" />
<!--必要样式-->
<link rel="stylesheet" type="text/css" href="css/component.css" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
</head>
<body>
		<div class="container demo-1">
			<div class="content">
				<div id="large-header" class="large-header">
					<canvas id="demo-canvas"></canvas>
					<div class="logo_box">
						<h3>注册</h3>
						<form action="register.do" name="f" method="post">
							<div class="input_outer">
								<span class="u_account"></span>
								<input name="userAccount" class="text" style="color: #FFFFFF !important" type="text" placeholder="请输入注册账户">
								<span class=message>${account_exits}</span>
							</div>
							<div class="input_outer">
								<span class="u_name"></span>
								<input name="userName" class="text" style="color: #FFFFFF !important" type="text" placeholder="请输入姓名">
							</div>
							<div class="input_outer">
								<span class="u_password"></span>
								<input name="password" class="text" style="color: #FFFFFF !important; position:absolute; z-index:100;"value="" type="password" placeholder="请输入密码">
							</div>
							<div class="input_outer">
								<span class="u_email"></span>
								<input name="userEmail" class="text" style="color: #FFFFFF !important" type="text" placeholder="请输入注册邮箱">
							</div>
							<div>
								<input type="submit" name="request_type" value="完成" style="width:150px;background: #0096e6;border-radius: 50px;line-height: 40px;color: #FFFFFF; border:none;font-size: 20px">
								<input type="submit" name="request_type" value="返回" style="width:150px;background: #0096e6;border-radius: 50px;line-height: 40px;color: #FFFFFF; border:none;font-size: 20px; margin-left:25px">${login_failed}</input>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div><!-- /container -->
		<script src="js/TweenLite.min.js"></script>
		<script src="js/EasePack.min.js"></script>
		<script src="js/rAF.js"></script>
		<script src="js/demo-1.js"></script>
		<div style="text-align:center;">
</div>
	</body>
</html>