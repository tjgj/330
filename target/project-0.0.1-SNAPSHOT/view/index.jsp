<%@page pageEncoding="utf-8"  contentType="text/html; charset=utf-8"%>
<html class="no-js">
<head>
<meta charset="UTF-8" />
<title>HUNDSUN</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="css/reset.css" rel="stylesheet" />
<link href="css/main.css" rel="stylesheet" />
</head>
<body>
<div class="i_header header">
	<div class="wrap">
		<div class="logo"><img src="images/logo.jpg"/></a></div>
		<div class="nav">
			<ul>
				<form name='index_form' action='toIndex.do' method='post'>  
				<li><a href='javascript:document.index_form.submit();' title="首页">首页</a></li>
				<li><a>${user_name}</a></li>
				</form> 
			</ul>
		</div>
	</div>
</div>
<div class="h30"></div>
<div class="main">
	<form name="statistic_form" action="toTopicIndex.do" method="post">
	<a href="javascript:document.statistic_form.submit();" class="btn btn_css3">
		<p class="pic"><img src="images/bt_p1.png"/></p>
		<h2 class="txt">统计中心</h2>
	</a>
	</form>
	<a href="#" class="btn btn_css3">
		<p class="pic"><img src="images/bt_p2.png"/></p>
		<h2 class="txt">文档论坛</h2>
	</a>
	<a href="#" class="btn btn_css3">
		<p class="pic"><img src="images/bt_p3.png"/></p>
		<h2 class="txt">待办事项</h2>
	</a>
	<a href="#" class="btn btn_css3">
		<p class="pic"><img src="images/bt_p4.png"/></p>
		<h2 class="txt">头脑王者</h2>
	</a>
	<div class="clear"></div>
</div>
<div class="h90"></div>
<!-- The Scripts -->
<script src="js/jquery.js"></script> 
<script src="js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript">
jQuery(".banner").hover(function(){ jQuery(this).find(".prev,.next").stop(true,true).fadeTo("show",0.2) },function(){ jQuery(this).find(".prev,.next").fadeOut() });
jQuery(".banner").slide({ mainCell:".pic",effect:"fold", autoPlay:true, delayTime:600, trigger:"click"});//banner图
</script>
<div style="text-align:center;">
<p>版权所有：资管PB实施团队</a></p>
</div>
</body>
</html>
