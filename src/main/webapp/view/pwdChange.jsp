<%@page pageEncoding="utf-8"  contentType="text/html; charset=utf-8"%>
<html class="no-js">
<head>
<meta charset="UTF-8" />
<title>HUNDSUN</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="css/reset.css" rel="stylesheet" />
<link href="css/main.css" rel="stylesheet" />
<script src="js/jquery-3.2.1.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
	    //绑定事件
	    
	});
	function showsub(obj){   
		var submenu=obj.getElementsByTagName("ul")[0];   
		submenu.style.display="block";  
	}  
	function hidesub(obj){   
		var submenu=obj.getElementsByTagName("ul")[0];   
		submenu.style.display="none";  
	}
</script> 
</head>
<body>
	<div class="i_header header" style="background-color:White">
		<div class="wrap">
			<!--div class="logo"><img src="images/logo.jpg"/></a></div-->
			<div class="nav">
				<ul>
					<form name='index_form' action='toIndex.do' method='post'>  
					<li><a href='javascript:document.index_form.submit();' title="首页">首页</a></li>
					<li onmouseover="showsub(this)" onmouseout="hidesub(this)" style="width: 130px;"><a>系统设置</a>
						<ul style="display:none;float:none;top-margin:0">     
							<li><a href="toPrivilegeSet.do" style="width: 130px; height: 30px; border: 1px solid #d2d2d2;background-color:White ">权限设置</a></li> 
							<li><a href='toRegister.do' style="width: 130px; height: 30px; border: 1px solid #d2d2d2;background-color:White ">人员添加</a></li>
						</ul> 
					</li>
					<li onmouseover="showsub(this)" onmouseout="hidesub(this)" style="width: 120px;"><a>${user_name}</a-->
						<ul style="display:none;float:none;top-margin:0">     
							<li><a href="toPwdChange.do" style="width: 130px; height: 30px; border: 1px solid #d2d2d2; ">密码修改</a></li> 
							<li><a href="toLogin.do" style="width: 130px; height: 30px; border: 1px solid #d2d2d2; ">退出</a></li>
						</ul> 
					</li>
					<!--li><a href='toLogin.do'>退出</a></li-->
					</form> 
				</ul>
			</div>
		</div>
	</div>
<div class="h30"></div>
<div class="h10"></div>
<div>
	<form class="pwdChangeForm" action="pwdChange.do" method='post'>
		<p style="margin-left:40%">输入旧登录密码：
			<input type="password" name="oldPassword"/>
		</p>
		<p style="margin-left:40%">输入新登录密码：
			<input type="password" name="newPassword"/>
		</p>
		<p style="margin-left:40%">再次输入新密码：
			<input type="password" name="newPasswordAgain"/>
		</p>
		<p style="margin-left:50%">
			<button type="commit" >确认修改</button>
		</p>
		<span id="wrong_warn" style="margin-left:50%;">${warn_message}</span>
	</form>
</div>
<div class="h10"></div>
<div class="h5"></div>
<div style="text-align:center;">
<p>版权所有：资管PB实施团队</a></p>
</div>
</body>
</html>
