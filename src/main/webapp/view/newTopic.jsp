<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<HTML lang=en>
<head>
    <base href="<%=basePath%>">
	<TITLE>发起统计</TITLE>
    <link rel="stylesheet" href="css/bootstrap.min.css"/>    
    <script src="js/jeDate/jedate.js"></script>
    <script src="js/jquery-1.7.2.min.js"></script><!-- 1.10.2版本会导致一个功能丢失 -->>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript">
	function showsub(obj){   
		var submenu=obj.getElementsByTagName("ul")[0];   
		submenu.style.display="block";  
	}  
	function hidesub(obj){   
		var submenu=obj.getElementsByTagName("ul")[0];   
		submenu.style.display="none";  
	}
</script> 
    <script src="js/vue.js"></script>
    <script src="js/vue-resource.min.js"></script>
    <script type="text/javascript" src="js/laydate.js"></script>

<style>
.tags {
	background-color: #fff;
	border: 1px solid #d5d5d5;
	color: #777;
	padding: 4px 6px;
	width: 406px;
	margin:10px auto;
}
.tags:hover {
	border-color: #f59942;
	outline: 0 none;
}
.tags[class*="span"] {
	float: none;
	margin-left: 0;
}
.tags input[type="text"], .tags input[type="text"]:focus {
	border: 0 none;
	box-shadow: none;
	display: inline;
	line-height: 22px;
	margin: 0;
	outline: 0 none;
	padding: 4px 6px; 
}
.tags .tag {
	background-color: #91b8d0;
	color: #fff;
	display: inline-block;
	font-size: 12px;
	font-weight: normal;
	margin-bottom: 3px;
	margin-right: 3px;
	padding: 4px 22px 5px 9px;
	position: relative;
	text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.15);
	transition: all 0.2s ease 0s;
	vertical-align: baseline;
	white-space: nowrap;
}
.tags .tag .close {
	bottom: 0;
	color: #fff;
	float: none;
	font-size: 12px;
	line-height: 20px;
	opacity: 1;
	position: absolute;
	right: 0;
	text-align: center;
	text-shadow: none;
	top: 0;
	width: 18px;
}
.tags .tag .close:hover {
	background-color: rgba(0, 0, 0, 0.2);
}
.close {
	color: #000;
	float: right;
	font-size: 21px;
	font-weight: bold;
	line-height: 1;
	opacity: 0.2;
	text-shadow: 0 1px 0 #fff;
}
.close:hover, .close:focus {
	color: #000;
	cursor: pointer;
	opacity: 0.5;
	text-decoration: none;
}
button.close {
	background: transparent none repeat scroll 0 0;
	border: 0 none;
	cursor: pointer;
	padding: 0;
}
.tags .tag-warning {
	background-color: #ffb752;
}
</style>
    <script>
          window.onload = function(){
        	  laydate.skin('danlan');//切换皮肤，请查看skins下面皮肤库
              new Vue({
                  el:'#box',
                  data:{
                     myData:[],
                     fieldname:"",
                     fieldcontent:"",
                     nowIndex:-100
                  },
                  methods:{
                     add:function(){
						  username = $("#fieldname").val();
						  var jionStr="";
						  var elem_child = $("#tags").children("span");
						  for(var i=0; i<elem_child.length;i++){
						  var iStr = elem_child[i].innerHTML.split("<");
						  if(i==elem_child.length-1){
						  jionStr = jionStr+iStr[0];
						  }else{
						  jionStr = jionStr+iStr[0]+";";
						  }
						  }
                          this.myData.push({
                              fieldname:username,
                              fieldcontent:jionStr//this.wjage
                          })
						  for(var i=0; i<elem_child.length;i++){
						  elem_child.empty();
						  elem_child.remove();
						  }
						  $("#fieldname").val("");
                     },
                      del:function(n){
                          if(n ==-2){
                              this.myData="";
                          }{
                              this.myData.splice(n,1);
                          }
                      }
                  }
              })
          }
    </script>
	<script type="text/javascript">
$(function() {
	$(".tags_enter").blur(function() { //焦点失去触发 
		var txtvalue=$(this).val().trim();
		if(txtvalue!=''){
			addTag($(this));
			$(this).parents(".tags").css({"border-color": "#d5d5d5"})
		}
	}).keydown(function(event) {
		var key_code = event.keyCode;
		var txtvalue=$(this).val().trim(); 
		if (key_code == 13&& txtvalue != '') { //enter
			addTag($(this));
		}
		if (key_code == 32 && txtvalue!='') { //space
			addTag($(this));
		}
	});
	$(".close").live("click", function() {
		$(this).parent(".tag").remove();
	});
	$(".tags").click(function() {
		$(this).css({"border-color": "#f59942"})
	}).blur(function() {
		$(this).css({"border-color": "#d5d5d5"})
	})
})
function addTag(obj) {
	var tag = obj.val();
	if (tag != '') {
		var i = 0;
		$(".tag").each(function() {
			if ($(this).text() == tag + "×") {
				$(this).addClass("tag-warning");
				setTimeout("removeWarning()", 400);
				i++;
			}
		})
		obj.val('');
		if (i > 0) { //说明有重复
			return false;
		}
		$("#fieldcontent").before("<span class='tag'>" + tag + "<button class='close' type='button'>×</button></span>"); //添加标签
	}
}
function removeWarning() {
	$(".tag-warning").removeClass("tag-warning");
}

function clearMsg(){
	var elem_child = $("#tags").children("span");
	for(var i=0; i<elem_child.length;i++){
	elem_child.empty();
	elem_child.remove();
}
	$("#fieldname").val("");
}
</script>
</head>
<body>
	<div class="i_header header" style="background-color:White">
		<div class="wrap">
			<div class="nav">
				<ul>
					<form name='index_form' action='toIndex.do' method='post'>  
					<li><a style="color:#1bbbff;" href='toNewTopic.do'>发起统计</a></li>
					<li><a href='toCustomers.do'>客户关系</a></li>
					<li><a>数据中心</a></li>
					<li onmouseover="showsub(this)" onmouseout="hidesub(this)" style="width: 130px;"><a>我的统计</a>
						<ul style="display:none;float:none;top-margin:0">     
							<li><a href="toTopicIndex.do" style="width: 130px; height: 30px; border: 1px solid #d2d2d2;background-color:White ">普通统计</a></li> 
							<li><a href='toBigData.do' style="width: 130px; height: 30px; border: 1px solid #d2d2d2;background-color:White ">特殊统计</a></li>
						</ul> 
					</li>
					<li><a href='javascript:document.index_form.submit();' title="首页">首页</a></li>
					<li onmouseover="showsub(this)" onmouseout="hidesub(this)" style="width: 120px;"><a>${user_name}</a-->
						<ul style="display:none;float:none;top-margin:0">     
							<li><a href="#" style="width: 130px; height: 30px; border: 1px solid #d2d2d2; ">密码修改</a></li> 
							<li><a href="toLogin.do" style="width: 130px; height: 30px; border: 1px solid #d2d2d2; ">退出</a></li>
						</ul> 
					</li>
					</form> 
				</ul>
			</div>
		</div>
	</div>
	<div class="h130"></div>
    <div class="container" id="box">
     <div id="topicStatus" style="width:70px;float:left;margin-left:250px;">
         <input type="radio" name="radio" value="1" checked>公&nbsp&nbsp&nbsp开  
         <input type="radio" name="radio" value="2">非公开
     </div>
	 <div class="tags" id="topic" tabindex="1" style="width:400px;float:left;">
		     <input id="topicName" style="width:350px" type="text" placeholder="请输入统计主题 ..." />
     </div>	 
     <form method="POST"  enctype="multipart/form-data" id="attachmentForm" action="uploadAttachment.do"> 
     <div class="tags" tabindex="1" style="width:300px;float:left;margin-left:50px;" >
		   <div style="float:left"> 附件：</div>
		   <div><input id="attachment" type="file" name="attachment" style="width:200px;"></div>	   
     </div>	
     <input id="topicInfo" type="text" name="topicInfo" hidden="hidden">	
     </form>
	 <div style="clear:both"> 
			<div class="tags"  style="width:330px;float:left;">
	             <label for="completeTime">截止：</label>
	             <input id="completeTime" style="width:250px" placeholder="请输入日期" class="laydate-icon" onClick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
			</div>
			<div class="tags" tabindex="1" style="width:775px;float:right;margin-left:10px;">
	             <label for="remark">统计原因：</label>
                 <input type="text" style="width:600px" id="remark" placeholder="输入统计原因"/>
			</div>
     </div>
	 <div> 
			<div class="tags" style="width:330px;float:left;">
	             <label for="fieldname">字段：</label>
                 <input type="text" style="width:250px" id="fieldname" placeholder="输入字段"/>
			</div>
			<div class="tags" id="tags" tabindex="1" style="width:775px;float:right;margin-left:10px;">
	             <label for="fieldcontent">字段选择框：</label>
                 <input type="text" id="fieldcontent" class="form-control"  v-model="fieldcontent" placeholder="输入字段"  value="Tag Input Control" name="tags" style="display: none;"/>				 
				 <input type="text" placeholder="输入统计字段多选值项， 空格分段..." class="tags_enter" autocomplete="off" style="width:300px;"/>
			</div>

     </div>
     <form role="form">
             <div  style="clear:both" class="form-group">
                 <input type="button"  value="添加" class="btn btn-primary" v-on:click="add()"/>
                 <input type="button"  value="重置" class="btn btn-danger" onclick="clearMsg()" />
             </div>
      </form>
         <hr/>
         <table id="tbList" class="table table-bordered table-hover">
             <tr>
                 <th id="xuhao" class="text-center">序号</th>
                 <th class="text-center">字段名</th>
                 <th class="text-center">字段选项</th>
                 <th class="text-center">操作</th>
             </tr>
             <tr class="text-center" v-for="item in myData">
                 <td>{{$index+1}}</td>
                 <td>{{item.fieldname}}</td>
                 <td>{{item.fieldcontent}}</td>
                 <td>
                     <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#layer" v-on:click="nowIndex=$index">删除</button>
                 </td>
             </tr>
             <tr v-show="myData.length!=0">
                 <td colspan="4" class="text-right">
					 <a class="btn btn-danger"  onclick="topicPost()">提交</a>
                 </td>
             </tr>
             <tr v-show="myData.length==0">
                 <td colspan="4" class="text-center text-info">
                     <p>暂无数据...</p>
                 </td>
             </tr>
         </table>
         <div style="float:left;margin-left:10px;">
         <p>通过EXCEL导入数据中心：</p>
      		<form method="POST"  enctype="multipart/form-data" id="form1" action="upload.do">
 	 		<table>
 	 	 	<tr>
 	 	 		<td>上传文件: </td>
 	 	 		<td> <input id="upfile" type="file" name="upfile"></td>
 	 		 </tr>
  			<tr>
  				<td>点击上传: </td>
 	 	 		<td><input type="submit" value="上传" onclick="return checkData()">(字段修改界面不支持回车符号)</td>
 	 		 </tr>
  			</table>	
		</form>
	    </div>
         <!--模态框 弹出框-->
         <div role="dialog" class="modal fade" id="layer" data-index="{{nowIndex}}">
         <div class="h250"></div>
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button class="close" data-dismiss="modal">
                              <span>&times;</span>
                          </button>
                          <h4 class="modal-title">确认删除吗？</h4>
                      </div>
                      <div class="modal-body text-right">
                          <button class="btn btn-primary btn-sm" data-dismiss="modal">取消</button>
                          <button class="btn btn-danger btn-sm" data-dismiss="modal" v-on:click="del(nowIndex)">确认</button>
                      </div>
                  </div>
              </div>
         </div>
     </div>
<div class="h90"></div>
<div style="text-align:center;">
	<p>版权所有：资管PB实施团队</a></p>
</div>
</body>
    <script type="text/javascript">
        function topicPost(){
   			//alert("hello");
   			//遍历获取table数据，返回数组
   			var tableData = [];
   			var mytable = document.getElementById("tbList");
   			for(var i=0,rows=mytable.rows.length; i<rows; i++){
   			  for(var j=0,cells=mytable.rows[i].cells.length; j<cells; j++){
   			    if(!tableData[i]){
   			      tableData[i] = new Array();
   			    }
   			    tableData[i][j] = mytable.rows[i].cells[j].innerHTML;
   			  }
   			}
   			//遍历数组数据为json
   			/*var tableData = [];
   			var mytable = document.getElementById("tbList");
   			for(var i=0,rows=mytable.rows.length; i<rows; i++){
   				var index = mytable.rows[i].cells[0].innerHTML;
   				var fieldname = mytable.rows[i].cells[1].innerHTML;
   				var fieldcontent = mytable.rows[i].cells[2].innerHTML;
   				var data1={"index":index,"fieldname":fieldname,"fieldcontent":fieldcontent};  
   			  	tableData.push(data1);
   			}*/
   			
   			var topicName = $("#topicName").val();
   			var completeTime = $("#completeTime").val();
   			var remark = $("#remark").val();
   			var formData = new FormData($( "#uploadForm" )[0]);  
   			var topicStatus = $('#topicStatus input[name="radio"]:checked ').val();
   			//var arry = ['1','2','3'];
   			//alert(topicName);
   			//alert(completeTime);
            $.ajax({
                type: "POST",
                url: "postTopic.do",
                traditional: true,
                data:{
                    data:tableData,
                    topicName:topicName,
                    completeTime:completeTime,
                    remark:remark,
                    formData:formData,
                    topicStatus:topicStatus
                }, 
                cache: false,
                async: false,
                beforeSend: function () {
                    	//alert("begin");
                },
                success: function (data) {
                    if (data != "") {
                    	if(remark == ""){                      	
                        	var topicInfo = data+"_"+topicName +"_"+completeTime+"_"+"无";
                    	}else{                    	
                        	var topicInfo = data+"_"+topicName +"_"+completeTime+"_"+remark;
                    	}

                    	$("#topicInfo").val(topicInfo);
                        topicInfo = $("#topicInfo").val();
                    	$('#attachmentForm').submit();
                    	alert("发起成功");
                    }else{
                    	alert("发起失败");
                    }
                	//location.reload();
                },
                error: function (errorThrown) {
                	//alert("stop");
                	alert(errorThrown.readystate);
                	alert(errorThrown.status);
                }
            });
   			
   		 }
        
        //JS校验form表单信息
	     function checkData(){
	     	var fileDir = $("#upfile").val();
	     	var suffix = fileDir.substr(fileDir.lastIndexOf("."));
	     	if("" == fileDir){
	     		alert("选择需要导入的Excel文件！");
	     		return false;
	     	}
	     	if(".xls" != suffix && ".xlsx" != suffix ){
	     		alert("选择Excel格式的文件导入！");
	     		return false;
	     	}
	     	return true;
	     }
    </script>
</html>