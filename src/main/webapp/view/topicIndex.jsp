<%@page pageEncoding="utf-8"  contentType="text/html; charset=utf-8"%>
<html class="no-js">
<head>
<meta charset="UTF-8" />
<title>我的统计</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="stylesheet" href="css/bootstrap.min.css"/>
<script src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
	function showsub(obj){   
		var submenu=obj.getElementsByTagName("ul")[0];   
		submenu.style.display="block";  
	}  
	function hidesub(obj){   
		var submenu=obj.getElementsByTagName("ul")[0];   
		submenu.style.display="none";  
	}
</script> 
<script type="text/javascript">
	$(document).ready(function(){
	    //加载初始化数据
	    loadInit();
	    
		$("button").click(function(e){
			var domId = e.target.id;
			var topicId = domId.split("_")
			/*if(topicId[0]=='B'){
				window.location.href = "toWriteTopic.do?id="+topicId[1];				
			}else{
				window.location.href = "toCheckTopic.do?id="+topicId[1];
			}*/
			switch(topicId[0])
			{
			case 'B':
				window.location.href = "toWriteTopic.do?id="+topicId[1];
				break;
			case 'C':
				window.location.href = "toCheckTopic.do?id="+topicId[1];
				break;
			default:
				window.location.href = "toChangeTopic.do?id="+topicId[1];
			}

		});
	});
	//加载初始化数据
	function loadInit(){
		$.ajaxSettings.async = false; //设置为同步，不然button的onclick事件无法绑定上去
		$.post("getTopic.do",function(data){
			for(i = 0; i < data.length; i++){
				var s = data[i];
				if(s[1] > 0){
					$('#tb1').append('<tr class="text-center"><td>' + s[0] + '</td><td><font size="3" color="red">' + s[1] + '</font></td><td>'
							+ s[2] + '</td><td>' + s[3] + '</td><td><font size="3" color="red">' + s[4] + '</font></td><td  style="width:200px" colspan="4" class="text-right">'
							+ '<div style="width:60px;float:left"><button id=B_'+ s[0] +' class="btn btn-danger"  style="height:35px;width:50px;">编写</button></div>'
							+ '<div style="width:60px;float:left"><button id=C_'+ s[0] +' class="btn btn-danger"  style="height:35px;width:50px;">查询</button></div>'
							+'<div style="width:60px;float:left"><button id=L_'+ s[0] +' class="btn btn-danger"  style="height:35px;width:50px;">修改</button></div></td></tr>');				
				}else{
					$('#tb1').append('<tr class="text-center"><td>' + s[0] + '</td><td>' + s[1] + '</td><td>'
							+ s[2] + '</td><td>' + s[3] + '</td><td>' + s[4] + '</td><td  style="width:200px" colspan="4" class="text-right">'
							+ '<div style="width:60px;float:left"><button id=B_'+ s[0] +' class="btn btn-danger"  style="height:35px;width:50px;">编写</button></div>'
							+ '<div style="width:60px;float:left"><button id=C_'+ s[0] +' class="btn btn-danger"  style="height:35px;width:50px;">查询</button></div>'
							+'<div style="width:60px;float:left"><button id=L_'+ s[0] +' class="btn btn-danger"  style="height:35px;width:50px;">修改</button></div></td></tr>');				
				}
			  }
			}
		, 'json');
	}
	
	function findTopic(){
     	var field = $("#findField").val();
     	var topicName = $("#topicName").val();
        $.ajax({
            type: "POST",
            url: "toFindField.do",
            traditional: true,
            data:{
            	field:field,
            	topicName:topicName,
            	range:"1"
            }, 
            cache: false,
            async: false,
            beforeSend: function () {
            	//alert("begin");
            },
            success: function (data) {
            	//alert("success");
            	$('#th1').empty();
            	$('#th1').append('<tr align="center" valign="middle"><td>序号</td><td>发起日期</td><td>统计名称</td><td>截止日期</td><td style="width:200px">&nbsp</td></tr>' );
            	$('#tb1').empty();
    			for(i = 0; i < data.length; i++){
    				var s = data[i];
    				if("吴俊" == "吴俊"){
    					$('#tb1').append('<tr class="text-center"><td>' + s.id + '</td><td>' + s.release_time + '</td><td>'
    							+ s.topic_name + '</td><td>' + s.complete_time + '</td><td  style="width:200px" colspan="4" class="text-right">'
    							+ '<div style="width:60px;float:left"><button id=B_'+ s.id +' class="btn btn-danger"  style="height:35px;width:50px;">编写</button></div>'
    							+ '<div style="width:60px;float:left"><button id=C_'+ s.id +' class="btn btn-danger"  style="height:35px;width:50px;">查询</button></div>'
    							+'<div style="width:60px;float:left"><button id=L_'+ s.id +' class="btn btn-danger"  style="height:35px;width:50px;">修改</button></div></td></tr>');				
    				}else{
    					$('#tb1').append('<tr class="text-center"><td>' + s.id + '</td><td>' + s.release_time + '</td><td>'
    							+ s.topic_name + '</td><td>' + s.complete_time + '</td><td  style="width:200px" colspan="4" class="text-right">'
    							+ '<div style="width:90px;float:left"><button id=B_'+ s.id +' class="btn btn-danger"  style="height:35px;width:80px;">编写</button></div>'
    							+'<div style="width:90px;float:left"><button id=C_'+ s.id +' class="btn btn-danger"  style="height:35px;width:80px;">查询</button></div></td></tr>');						
    				}
    			  }
    			
    			$("button").click(function(e){
    				var domId = e.target.id;
    				var topicId = domId.split("_")
    				/*if(topicId[0]=='B'){
    					window.location.href = "toWriteTopic.do?id="+topicId[1];				
    				}else{
    					window.location.href = "toCheckTopic.do?id="+topicId[1];
    				}*/
    				switch(topicId[0])
    				{
    				case 'B':
    					window.location.href = "toWriteTopic.do?id="+topicId[1];
    					break;
    				case 'C':
    					window.location.href = "toCheckTopic.do?id="+topicId[1];
    					break;
    				default:
    					window.location.href = "toChangeTopic.do?id="+topicId[1];
    				}

    			});
            },
            error: function (errorThrown) {
            	//alert("stop");
            	alert(errorThrown.readystate);
            	alert(errorThrown.status);
            }
        });
	}
	
</script>
</head>
<body>
<div class="i_header header" style="background-color:White">
	<div class="wrap">
		<!--div class="logo"><img src="images/logo.jpg"/></a></div-->
		<div class="nav">
			<ul>
				<form name='index_form' action='toIndex.do' method='post'>  
					<li><a href='toNewTopic.do'>发起统计</a></li>
					<li><a href='toCustomers.do'>客户关系</a></li>
					<li><a>数据中心</a></li>
					<li onmouseover="showsub(this)" onmouseout="hidesub(this)" style="width: 130px;"><a>我的统计</a>
						<ul style="display:none;float:none;top-margin:0">     
							<li><a href="toTopicIndex.do" style="width: 130px; height: 30px; border: 1px solid #d2d2d2;background-color:White ">普通统计</a></li> 
							<li><a href='toBigData.do' style="width: 130px; height: 30px; border: 1px solid #d2d2d2;background-color:White ">特殊统计</a></li>
						</ul> 
					</li>
					<li><a href='javascript:document.index_form.submit();' title="首页">首页</a></li>
					<li onmouseover="showsub(this)" onmouseout="hidesub(this)" style="width: 120px;"><a>${user_name}</a>
						<ul style="display:none;float:none;top-margin:0">     
							<li><a href="#" style="width: 130px; height: 30px; border: 1px solid #d2d2d2;background-color:White ">密码修改</a></li> 
							<li><a href="toLogin.do" style="width: 130px; height: 30px; border: 1px solid #d2d2d2;background-color:White ">退出</a></li>
						</ul> 
					</li>
				</form> 
			</ul>
		</div>
	</div>
</div>
<div class="h130"></div>
	<div class="main container" style="text-align:center;">
	<div>
	     <label for="remark">统计名称：</label>
         <input type="text" style="width:300px" id="topicName" placeholder="输入想要搜索的统计名称"/>
         <label for="remark">统计字段：</label>
         <input type="text" style="width:300px" id="findField" placeholder="输入想要搜索的统计字段"/>
         <input type="button"  value="搜索" class="btn btn-danger" onclick="findTopic()" />
         <p><p>
	</div>
	<table id="table1" class="table table-bordered table-hover">
	<thead id="th1">
	<tr align="center" valign="middle">
		<td>统计编号</td>
		<td>未填写数</td>
		<td>统计日期</td>
		<td>统计名称</td>
		<td>截止日期</td>
		<td style="width:200px">&nbsp</td>
    </tr> 
    </thead>   
    <tbody id="tb1" align="center" valign="middle">
    </tbody> 
</table>
</div>
<div class="h90"></div>
<div style="text-align:center;">
	<p>版权所有：资管PB实施团队</a></p>
</div>
</body>
</html>