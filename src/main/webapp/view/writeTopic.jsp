<%@page pageEncoding="utf-8"  contentType="text/html; charset=utf-8"%>
<html lang=en>
<head lang="en">
    <meta charset="UTF-8">
    <title>填写统计</title>
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript">
	function showsub(obj){   
		var submenu=obj.getElementsByTagName("ul")[0];   
		submenu.style.display="block";  
	}  
	function hidesub(obj){   
		var submenu=obj.getElementsByTagName("ul")[0];   
		submenu.style.display="none";  
	}
</script> 
</head>
<body>
<div class="i_header header" style="background-color:White">
	<div class="wrap">
		<!--div class="logo"><img src="images/logo.jpg"/></a></div-->
		<div class="nav">
			<ul>
				<form name='index_form' action='toIndex.do' method='post'>  
					<li><a href='toNewTopic.do'>发起统计</a></li>
					<li><a href='toCustomers.do'>客户关系</a></li>
					<li><a href='toBigData.do'>数据中心</a></li>
					<li><a href='toTopicIndex.do'>我的统计</a></li>
					<li><a href='javascript:document.index_form.submit();' title="首页">首页</a></li>
					<li onmouseover="showsub(this)" onmouseout="hidesub(this)" style="width: 120px;"><a>${user_name}</a-->
						<ul style="display:none;float:none;top-margin:0">     
							<li><a href="#" style="width: 130px; height: 30px; border: 1px solid #d2d2d2; ">密码修改</a></li> 
							<li><a href="toLogin.do" style="width: 130px; height: 30px; border: 1px solid #d2d2d2; ">退出</a></li>
						</ul> 
					</li>
				</form> 
			</ul>
		</div>
	</div>
</div>
<div class="h130"></div>
     <div class="container" id="box">
         <table id="tbble" class="table table-bordered table-hover">
              <p id="topicId" hidden="hidden">${topic_id}</p>
              <caption id="topicName" class="h3 text-info">${topic_name}</caption> 
              <caption id="completeTime" class="h3 text-info">截止日期：${complete_time}</caption>      
             <tr bgcolor="#E0E0E0">
                 <th class="text-center">客户名称：
                 	<select id="cusSelect"  style="width:250px;height:30px">
					</select>
                 </th>
                 <th class="text-center">系统版本：
                    <input type="text" style="width:250px;" id="systemVersion"/>
                 </th>
                 <th class="text-center"><button class="btn btn-danger"  data-toggle="modal" data-target="#layer" onclick="changeVersion()">版本纠错</button></th>
             </tr>
             <tbody id="tb1" align="center" valign="middle">
             </tbody> 
             <tr>
                 <td colspan="4" class="text-right">
                     <button class="btn btn-danger"  data-toggle="modal" data-target="#layer"  onclick="submitTableValue()">提交</button>
                 </td>
             </tr>
         </table>
     </div>
<div class="h90"></div>
<div style="text-align:center;">
	<p>版权所有：资管PB实施团队</a></p>
</div>
</body>
<script type="text/javascript">
var systemVersionArray = new Array(); 
$(function() {   
	//获取用户维护的客户数量
    $.ajax({
        type: "POST",
        url: "toGetCustomersForWrite.do",
        traditional: true,
        data:{
        	topicId:topicId
        }, 
        cache: false,
        async: false,
        beforeSend: function () {
            	//alert("begin");
        },
        success: function (data) {
        	//alert("success");
            $('#systemVersion').val(data[0].system_version);
        	for(i = 0; i < data.length; i++){
    			var s = data[i];
    			 $("#cusSelect").append('<option value ="'+s.id+'">'+s.customer_name+'</option>');	  
    			 systemVersionArray.push(s.system_version); 
    			}
        },
        error: function (errorThrown) {
        	//alert("stop");
        	alert(errorThrown.readystate);
        	alert(errorThrown.status);
        }
    });
	
	loadTable();
});

//通过ajax将后台返回的数据，拼接成table
function loadTable(){
	var cusId = $('#cusSelect option:selected') .val();
	var topicId = $("#topicId").text(); 
    $.ajax({
        type: "POST",
        url: "toFillWriteTopic.do",
        traditional: true,
        data:{
        	topicId:topicId,
        	cusId:cusId
        }, 
        cache: false,
        async: false,
        beforeSend: function () {
            	//alert("begin");
        },
        success: function (data) {
        	//alert("success");
        	var index = 1;
        	for(i = 0;i < data.length;index++ ){
        		$("#tb1").append('<tr id="tr'+index+'" class="text-center">');
        		for(a = 0;i < data.length;i++,a++){
        			if (a==3){
        				break;
        			}else{
            			var s = data[i];
            			if(s.field_content==""){
                			$('#tr'+index).append('<td id="td'+i+'"><div style="width:330px;float:left;">'+
                					'<label  style="width:250px; for="'+s.id+'">'+s.field_name+'</label>'+
                					'<input type="text" style="width:250px;" id="'+s.id+'"/>'	
                			);
                			$('#tr'+index).append('</div></td>');	
                			$('#'+s.id).val(s.field_value);	
            			}else{
            				var arr = s.field_content.split(/[\,|;|；|、|:|，]+/); 
                			$('#tr'+index).append('<td id="td'+i+'"><div style="width:330px;float:left;">'+
                					'<label  style="width:250px; for="'+s.id+'">'+s.field_name+'</label>'+
                					'<select style="width:250px;height:30px" id="'+s.id+'"></select>');
 							for(sIndex=0;sIndex<arr.length;sIndex++){
 								$('#'+s.id).append('<option value ="'+arr[sIndex]+'">'+arr[sIndex]+'</option>');			
 							}
                			$('#tr'+index).append('</div></td>');	
                			$('#'+s.id).val(s.field_value);	
            			}
        			}
        		}
        		$("#tb1").append('</tr>');
        	}
        	
        },
        error: function (errorThrown) {
        	//alert("stop");
        	alert(errorThrown.readystate);
        	alert(errorThrown.status);
        }
    });
}

function submitTableValue(){
	var subData = new Array(); 
	$('#tb1 input').each(function(){
		subData.push($(this).attr("id"));
		subData.push($(this).val());
		});
	$('#tb1 select').each(function(){
		subData.push($(this).attr("id"));
		subData.push($(this).val());
		});		
        $.ajax({
            type: "POST",
            url: "toSubmitTopicData.do",
            traditional: true,
            data:{
            	data:subData
            },
            cache: false,
            async: false,//设置为同步
            //dataType: "json", 
            beforeSend: function () {
                //alert("begin");
            },
            success: function (dataId) {
            	//alert("success");
            	window.location.href = "toTopicIndex.do";
            	alert("提交成功");
            },
            error: function (errorThrown) {
            	alert("AJAX-ERROR");
            	alert(errorThrown.readystate);
            	alert(errorThrown.status);
            }
        });   
}

$('select#cusSelect').change(function(){
	var index = $('option:selected', '#cusSelect').index();
    $('#systemVersion').val(systemVersionArray[index]);
	$('#tb1').empty();
	loadTable();
});

function changeVersion(){
	var cusId = $('#cusSelect option:selected') .val();
	var index = $('option:selected', '#cusSelect').index();
	systemVersionArray[index] = $('#systemVersion').val();
    $.ajax({
        type: "POST",
        url: "toChangeVersion.do",
        traditional: true,
        data:{
        	cusId:cusId,
        	systemVersion:systemVersionArray[index]
        },
        cache: false,
        async: false,//设置为同步
        //dataType: "json", 
        beforeSend: function () {
            //alert("begin");
        },
        success: function (dataId) {
        	//alert("success");
        	alert("修改成功");
        },
        error: function (errorThrown) {
        	alert("AJAX-ERROR");
        	alert(errorThrown.readystate);
        	alert(errorThrown.status);
        }
    }); 
	}

</script>
</html>