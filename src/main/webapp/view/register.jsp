<%@page pageEncoding="utf-8"  contentType="text/html; charset=utf-8"%>
<html class="no-js">
<head>
<meta charset="UTF-8" />
<title>HUNDSUN</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="css/reset.css" rel="stylesheet" />
<link href="css/bootstrap-3.3.4.css" rel="stylesheet" />
<link href="css/main.css" rel="stylesheet" />
<script src="js/jquery-3.2.1.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
	    //绑定事件
	    
	});
	function showsub(obj){   
		var submenu=obj.getElementsByTagName("ul")[0];   
		submenu.style.display="block";  
	}  
	function hidesub(obj){   
		var submenu=obj.getElementsByTagName("ul")[0];   
		submenu.style.display="none";  
	}
</script> 
</head>
<body>
	<div class="i_header header" style="background-color:White">
		<div class="wrap">
			<!--div class="logo"><img src="images/logo.jpg"/></a></div-->
			<div class="nav">
				<ul>
					<form name='index_form' action='toIndex.do' method='post'>  
					<li><a href='javascript:document.index_form.submit();' title="首页">首页</a></li>
					<li onmouseover="showsub(this)" onmouseout="hidesub(this)" style="width: 130px;"><a>系统设置</a>
						<ul style="display:none;float:none;top-margin:0">     
							<li><a href="toPrivilegeSet.do" style="width: 130px; height: 30px; border: 1px solid #d2d2d2;background-color:White ">权限设置</a></li> 
							<li><a href='toRegister.do' style="width: 130px; height: 30px; border: 1px solid #d2d2d2;background-color:White ">人员添加</a></li>
						</ul> 
					</li>
					<li onmouseover="showsub(this)" onmouseout="hidesub(this)" style="width: 120px;"><a>${user_name}</a-->
						<ul style="display:none;float:none;top-margin:0">     
							<li><a href="toPwdChange.do" style="width: 130px; height: 30px; border: 1px solid #d2d2d2; ">密码修改</a></li> 
							<li><a href="toLogin.do" style="width: 130px; height: 30px; border: 1px solid #d2d2d2; ">退出</a></li>
						</ul> 
					</li>
					<!--li><a href='toLogin.do'>退出</a></li-->
					</form> 
				</ul>
			</div>
		</div>
	</div>
<div class="h30"></div>
<div>
	<form class="pwdChangeForm" action="register.do" method='post'>
		<p style="margin-left:40%">工号：
			<input type="text" name="useraccount"/>
		</p>
		<p style="margin-left:40%">姓名：
			<input type="text" name="username"/>
		</p>
		<p style="margin-left:40%">部门：
			<input type="text" name="userdepartment"/>
		</p>
		<p style="margin-left:40%">片区：
			<input type="text" name="userarea"/>
		</p>
		<p style="margin-left:40%">小组：
			<input type="text" name="usergroup"/>
		</p>
		<p style="margin-left:40%">邮箱：
			<input type="text" name="useremail"/>
		</p>
		<p style="margin-left:45%">
			<button class="btn btn-default" type="commit" style="width:70px;height:30;">添加</button>
			<button type="commit" >导入</button>
			<span id="wrong_warn" >${warn_message}</span>
		</p>
	</form>
</div>
<div id="popup_overlay" style="display: none; position: fixed; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #8FB0D1; -moz-opacity: 0.8; opacity: 0.8; z-index: 1001; filter: alpha(opacity=40); background: rgb(0, 0, 0); opacity: 0.5;"></div>   <div id="popup_container" style="display: none; position: fixed; z-index: 99999; padding: 0px; margin: 0px; min-width: 600px; max-width: 600px; top: 50px; left: 454.5px;">    <br />    <h1 id="popup_title" style="font-size: 20px;">信息</h1>    <div id="popup_content" class="confirm" style="margin-top: 0px;">     <div id="popup_message">      <div style="width: 500px;">       <hr style="margin: 10px 0;" />       <div id="divswitchinfo" style="margin-bottom: 8px;"></div>       <div style="height: 300px; width: 450px;" id="diviframe">          <div id="divContract">         <div id="pContract">          合同名称：<font color="red">*</font><input type="text" value="" id="txtContractName" style="width: 360px"><br />          起始时间：<font color="red">*</font><input type="text" value="" id="txtCStartTime" style="width: 150px" onfocus="WdatePicker({ el: 'txtCStartTime' })">-          <input type="text" value="" id="txtCEndTime" onfocus="WdatePicker({ el: 'txtCEndTime' })" style="width: 150px"><br />          合同附件：          <asp:FileUpload ID="fileID" runat="server" />         </div>        </div>        <input type="button" id="btnAdd" value='新增' />        <input type="hidden" id="hidValue" runat="server" />        <div id="UDFBlock">         <p id="udf_template">                人数<font color="red">*</font>：     <input type="text" value="" tag="txtNum01" style="width: 90px">＜X≤     <input type="text" value="" tag="txtNum02" style="width: 84px">          比例：<input type="text" value="" tag="txtPercent" style="width: 86px">%     <a class="UDF_Delete" style="cursor: pointer">删除</a>         </p>        </div>       </div>      </div>     </div>     <div id="popup_panel" style="clear: both">      <input type="button" class="btn btn-default" value=" 确定 " id="popup_ok2" />      <input type="button" class="btn btn-default" value=" 取消 " id="popup_cancel2" />     </div>    </div>   </div> 

<div class="h10"></div>
<div class="h5"></div>
<div style="text-align:center;">
<p>版权所有：资管PB实施团队</a></p>
</div>
</body>
</html>
