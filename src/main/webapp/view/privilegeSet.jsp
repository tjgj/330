<%@page pageEncoding="utf-8"  contentType="text/html; charset=utf-8"%>
<html class="no-js">
<head>
<meta charset="UTF-8" />
<title>权限设置</title>
<script src="js/jquery-3.2.1.js"></script>
<script type="text/javascript">
	function showsub(obj){   
		var submenu=obj.getElementsByTagName("ul")[0];   
		submenu.style.display="block";  
	}  
	function hidesub(obj){   
		var submenu=obj.getElementsByTagName("ul")[0];   
		submenu.style.display="none";  
	}
</script> 
<link href="css/reset.css" rel="stylesheet" />
<link href="css/main.css" rel="stylesheet" />
<link rel="stylesheet" href="css/bootstrap.min.css"/>
<script type="text/javascript">
	$(document).ready(function(){
	    //加载数据
	    loadInit();
	});
	
	function loadInit(){
		$.ajaxSettings.async = false; 
		$.post("getPrivilege.do",function(data){
			for(i = 0; i < data.length; i++){
				var s = data[i];
				$('#tb1').append('<tr class="text-center"><td><input type="checkbox" id=checkbox' + i + ' onchange="privChange(this);"></td><td>' + s[0].id + '</td><td>' + s[0].name + '</td><td>'
						+ s[1].sub_system_name + '</td><td>' + s[1].privId + '</td><td>' + s[1].privName + '</td></tr>');				
				judgePriv(i, s[0].id, s[1].privId);
				}
			}
		, 'json');
		getUsers();
		getModules();
	}
	
	function search(){
		var nameVal = $("#nameselect").val();
		var moduleVal = $("#moduleselect").val();
		var name = $("#nameselect").find("option:selected").text();
		var module = $("#moduleselect").find("option:selected").text();
		if(nameVal == 0 && moduleVal == 0){
			alert("请选择一项后再进行查询");
			return;
		}else if(nameVal == 0){
			alert("不允许单独使用板块条件");
			return;
		}else{
			$.post("getPrivilegeBySearch.do",{"name":name, "module":module},function(data){
				$("#tb1").empty(""); 
				for(i = 0; i < data.length; i++){
					var s = data[i];
					$('#tb1').append('<tr class="text-center"><td><input type="checkbox" id=checkbox' + i + ' onchange="privChange(this);"></td><td>' + s[0].id + '</td><td>' + s[0].name + '</td><td>'
							+ s[1].sub_system_name + '</td><td>' + s[1].privId + '</td><td>' + s[1].privName + '</td></tr>');
					judgePriv(i, s[0].id, s[1].privId);
				  }
				}
			, 'json');
		}
		
	}
	
	function getUsers(){
		$.post("getUsers.do",function(data){
			$("#nameselect").empty(""); 
			$('#nameselect').append("<option value=0>" + "--请选择--" + "</option>")
			for(i = 0; i < data.length ; i++){
				var s = data[i];
				$('#nameselect').append("<option value=" + (i+1) +">" + s.name + "</option>");				
			  }
			}
		, 'json');
	}
	
	function getModules(){
		$.post("getModules.do",function(data){
			$("#moduleselect").empty(""); 
			$('#moduleselect').append("<option value=0>" + "--请选择--" + "</option>")
			for(i = 0; i < data.length ; i++){
				var s = data[i];
				$('#moduleselect').append("<option value=" + (i+1) +">" + s + "</option>");				
			  }
			}
		, 'json');
	}
	
	function judgePriv(checkBoxId, userId, privId){
		//请求判断该用户是否有该权限
		$.post("judgePriv.do", {"userId":userId, "privId":privId},
		function(data){
			if(data == "true"){
				$("#checkbox" + checkBoxId).attr("checked",true);
			}
		});
		//请求返回true则勾选
	}
	
	function privChange(obj){
		checkBoxId = obj.id;
		userId = $(obj).parent().next().text();
		privId = $(obj).parent().next().next().next().next().text();
		if($("#" + checkBoxId).is(':checked')){
			//赋权
			$.post("setPrivilege.do", {"userId":userId, "privId":privId},
			function(data){
				
			});
		}else{
			//取消权限
			$.post("deletePrivilege.do", {"userId":userId, "privId":privId},
			function(data){
				
			});
		}
	}
</script>
</head>
<body>
<div class="i_header header" style="background-color:White">
	<div class="wrap">
		<div class="nav">
			<ul>
				<form name='index_form' action='toIndex.do' method='post'>  
					<li><a href='javascript:document.index_form.submit();' title="首页">首页</a></li>
					<li onmouseover="showsub(this)" onmouseout="hidesub(this)" style="width: 130px;"><a>系统设置</a>
						<ul style="display:none;float:none;top-margin:0">     
							<li><a href="toPrivilegeSet.do" style="width: 130px; height: 30px; border: 1px solid #d2d2d2;background-color:White ">权限设置</a></li> 
							<li><a href='toRegister.do' style="width: 130px; height: 30px; border: 1px solid #d2d2d2;background-color:White ">人员添加</a></li>
						</ul> 
					</li>
					<li onmouseover="showsub(this)" onmouseout="hidesub(this)" style="width: 120px;"><a>${user_name}</a-->
						<ul style="display:none;float:none;top-margin:0">     
							<li><a href="#" style="width: 130px; height: 30px; border: 1px solid #d2d2d2; ">密码修改</a></li> 
							<li><a href="toLogin.do" style="width: 130px; height: 30px; border: 1px solid #d2d2d2; ">退出</a></li>
						</ul> 
					</li>
				</form> 
			</ul>
		</div>
	</div>
</div>
	<div class="h30"></div>
	<div id="box_center" style="margin:auto;position:absolute;left:35%;right:0;">姓名
		<select id="nameselect" class="ui_select01">
            <option id="nameFirst" value="0">--请选择--</option>
        </select>
		板块
		<select id="moduleselect" class="ui_select01">
			<option id="moduleFirst" value="0">--请选择--</option>
		</select>
		<input type="button" value="查找" sytle="width:30px;height:20px" class="ui_input_btn01" onclick="search();" /> 
	</div>
	<div class="h40"></div>
	<div class="ui_content">
		<div class="ui_tb">
			<table id = "table1" class="table table-bordered" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
				<thead>
					<tr align="center" valign="middle">
						<td>是否有权限</td>
						<td>账号</td>
						<td>姓名</td>
						<td>板块</td>
						<td>权限编号</td>
						<td>权限内容</td>
					</tr> 
				</thead>   
				<tbody id="tb1" align="center" valign="middle">
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>
