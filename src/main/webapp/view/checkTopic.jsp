<%@page pageEncoding="utf-8"  contentType="text/html; charset=utf-8"%>
<html lang=en>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>查询统计</title>
    <link href="http://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="js/tableExcel/excel-bootstrap-table-filter-style.css" />
    <link rel="stylesheet" type="text/css" href="css/demoCheckTopic.css">
    <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="js/tableExcel/excel-bootstrap-table-filter-bundle.js"></script>
<script type="text/javascript">
	function showsub(obj){   
		var submenu=obj.getElementsByTagName("ul")[0];   
		submenu.style.display="block";  
	}  
	function hidesub(obj){   
		var submenu=obj.getElementsByTagName("ul")[0];   
		submenu.style.display="none";  
	}
</script> 
	<script type="text/javascript" src="js/excelJs/jszip.js"></script>
	<script type="text/javascript" src="js/excelJs/FileSaver.js"></script>
	<script type="text/javascript" src="js/excelJs/excel-gen.js"></script>	
	<link href="http://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<style type="text/css">


/* drag */
#drag{position:absolute;top:100px;left:100px;width:800px;height:320px;background:#e9e9e9;border:1px solid #444;border-radius:5px;box-shadow:0 1px 3px 2px #666;}
#drag .title{position:relative;height:27px;margin:5px;}
#drag .title h2{font-size:14px;height:27px;line-height:24px;border-bottom:1px solid #A1B4B0;}
#drag .title div{position:absolute;height:19px;top:2px;right:0;}
#drag .title a,a.open{float:left;width:21px;height:19px;display:block;margin-left:5px;background:url(images/tool.png) no-repeat;}
a.open{position:absolute;top:80px;left:85%;margin-left:-10px;background-position:0 0;}
a.open:hover{background-position:0 -29px;}
#drag .title a.min{background-position:-29px 0;}
#drag .title a.min:hover{background-position:-29px -29px;}
#drag .title a.max{background-position:-60px 0;}
#drag .title a.max:hover{background-position:-60px -29px;}
#drag .title a.revert{background-position:-149px 0;display:none;}
#drag .title a.revert:hover{background-position:-149px -29px;}
#drag .title a.close{background-position:-89px 0;}
#drag .title a.close:hover{background-position:-89px -29px;}
#drag .content{overflow:auto;margin:0 5px;}
#drag .resizeBR{position:absolute;width:14px;height:14px;right:0;bottom:0;overflow:hidden;cursor:nw-resize;background:url(images/resize.png) no-repeat;}
#drag .resizeL,#drag .resizeT,#drag .resizeR,#drag .resizeB,#drag .resizeLT,#drag .resizeTR,#drag .resizeLB{position:absolute;background:#000;overflow:hidden;opacity:0;filter:alpha(opacity=0);}
#drag .resizeL,#drag .resizeR{top:0;width:5px;height:100%;cursor:w-resize;}
#drag .resizeR{right:0;}
#drag .resizeT,#drag .resizeB{width:100%;height:5px;cursor:n-resize;}
#drag .resizeT{top:0;}
#drag .resizeB{bottom:0;}
#drag .resizeLT,#drag .resizeTR,#drag .resizeLB{width:8px;height:8px;background:#FF0;}
#drag .resizeLT{top:0;left:0;cursor:nw-resize;}
#drag .resizeTR{top:0;right:0;cursor:ne-resize;}
#drag .resizeLB{left:0;bottom:0;cursor:ne-resize;}
</style>
<script type="text/javascript">
/*-------------------------- +
  获取id, class, tagName
 +-------------------------- */
var get = {
	byId: function(id) {
		return typeof id === "string" ? document.getElementById(id) : id
	},
	byClass: function(sClass, oParent) {
		var aClass = [];
		var reClass = new RegExp("(^| )" + sClass + "( |$)");
		var aElem = this.byTagName("*", oParent);
		for (var i = 0; i < aElem.length; i++) reClass.test(aElem[i].className) && aClass.push(aElem[i]);
		return aClass
	},
	byTagName: function(elem, obj) {
		return (obj || document).getElementsByTagName(elem)
	}
};
var dragMinWidth = 800;
var dragMinHeight = 320;
/*-------------------------- +
  拖拽函数
 +-------------------------- */
function drag(oDrag, handle)
{
	var disX = dixY = 0;
	var oMin = get.byClass("min", oDrag)[0];
	var oMax = get.byClass("max", oDrag)[0];
	var oRevert = get.byClass("revert", oDrag)[0];
	var oClose = get.byClass("close", oDrag)[0];
	handle = handle || oDrag;
	handle.style.cursor = "move";
	handle.onmousedown = function (event)
	{
		var event = event || window.event;
		disX = event.clientX - oDrag.offsetLeft;
		disY = event.clientY - oDrag.offsetTop;
		
		document.onmousemove = function (event)
		{
			var event = event || window.event;
			var iL = event.clientX - disX;
			var iT = event.clientY - disY;
			var maxL = document.documentElement.clientWidth - oDrag.offsetWidth;
			var maxT = document.documentElement.clientHeight - oDrag.offsetHeight;
			
			iL <= 0 && (iL = 0);
			iT <= 0 && (iT = 0);
			iL >= maxL && (iL = maxL);
			iT >= maxT && (iT = maxT);
			
			oDrag.style.left = iL + "px";
			oDrag.style.top = iT + "px";
			
			return false
		};
		
		document.onmouseup = function ()
		{
			document.onmousemove = null;
			document.onmouseup = null;
			this.releaseCapture && this.releaseCapture()
		};
		this.setCapture && this.setCapture();
		return false
	};	
	//最大化按钮
	oMax.onclick = function ()
	{
		oDrag.style.top = oDrag.style.left = 0;
		oDrag.style.width = document.documentElement.clientWidth - 20 + "px";
		oDrag.style.height = document.documentElement.clientHeight - 120 + "px";
		var oDrag2 = document.getElementById("textareaBox");
		oDrag2.style.width = document.documentElement.clientWidth - 40 + "px";
		oDrag2.style.height = document.documentElement.clientHeight - 220 + "px";
		oDrag.style.top="100px";
		this.style.display = "none";
		oRevert.style.display = "block";
	};
	//还原按钮
	oRevert.onclick = function ()
	{		
		oDrag.style.width = dragMinWidth + "px";
		oDrag.style.height = dragMinHeight + "px";
		var oDrag2 = document.getElementById("textareaBox");
		oDrag2.style.width = dragMinWidth - 20 + "px";
		oDrag2.style.height = dragMinHeight - 100 + "px";
		oDrag.style.left = (document.documentElement.clientWidth - oDrag.offsetWidth) / 2 + "px";
		oDrag.style.top = (document.documentElement.clientHeight - oDrag.offsetHeight) / 2 + "px";
		this.style.display = "none";
		oMax.style.display = "block";
	};
	//最小化按钮
	oMin.onclick = oClose.onclick = function ()
	{
		oDrag.style.display = "none";
		var oA = document.createElement("a");
		oA.className = "open";
		oA.href = "javascript:;";
		oA.title = "展开统计反馈结果";
		oA.style = "position:absolute; top:200; right:200;"
		document.body.appendChild(oA);
		oA.onclick = function ()
		{
			oDrag.style.display = "block";
			document.body.removeChild(this);
			this.onclick = null;
		};
	};
	//阻止冒泡
	oMin.onmousedown = oMax.onmousedown = oClose.onmousedown = function (event)
	{
		this.onfocus = function () {this.blur()};
		(event || window.event).cancelBubble = true	
	};
}
/*-------------------------- +
  改变大小函数
 +-------------------------- */
function resize(oParent, oDrag2,handle, isLeft, isTop, lockX, lockY)
{
	handle.onmousedown = function (event)
	{
		var event = event || window.event;
		var disX = event.clientX - handle.offsetLeft;
		var disY = event.clientY - handle.offsetTop;	
		var iParentTop = oParent.offsetTop;
		var iParentLeft = oParent.offsetLeft;
		var iParentWidth = oParent.offsetWidth;
		var iParentHeight = oParent.offsetHeight;
		
		var iParentTop1 = oDrag2.offsetTop;
		var iParentLeft1 = oDrag2.offsetLeft;
		var iParentWidth1 = oDrag2.offsetWidth;
		var iParentHeight1 = oDrag2.offsetHeight;
		
		document.onmousemove = function (event)
		{
			var event = event || window.event;
			
			var iL = event.clientX - disX;
			var iT = event.clientY - disY;
			var maxW = document.documentElement.clientWidth - oParent.offsetLeft - 2;
			var maxH = document.documentElement.clientHeight - oParent.offsetTop - 2;			
			var iW = isLeft ? iParentWidth - iL : handle.offsetWidth + iL;
			var iH = isTop ? iParentHeight - iT : handle.offsetHeight + iT;
			
			
			isLeft && (oParent.style.left = iParentLeft + iL + "px");
			isTop && (oParent.style.top = iParentTop + iT + "px");
			isLeft && (oDrag2.style.left = iParentLeft + iL + "px");
			isTop && (oDrag2.style.top = iParentTop + iT + "px");
			
			iW < dragMinWidth && (iW = dragMinWidth);
			iW > maxW && (iW = maxW);
			lockX || (oParent.style.width = iW + "px");
			lockX || (oDrag2.style.width = iW - 20 + "px");
			
			iH < dragMinHeight && (iH = dragMinHeight);
			iH > maxH && (iH = maxH);
			lockY || (oParent.style.height = iH + "px");
			lockY || (oDrag2.style.height = iH - 100 + "px");
			
			if((isLeft && iW == dragMinWidth) || (isTop && iH == dragMinHeight)) document.onmousemove = null;
			
			return false;	
		};
		document.onmouseup = function ()
		{
			document.onmousemove = null;
			document.onmouseup = null;
		};
		return false;
	}
};
function loadResize()
{
	var oDrag = document.getElementById("drag");
	var oDrag2 = document.getElementById("textareaBox");
	
	var oTitle = get.byClass("title", oDrag)[0];
	var oL = get.byClass("resizeL", oDrag)[0];
	var oT = get.byClass("resizeT", oDrag)[0];
	var oR = get.byClass("resizeR", oDrag)[0];
	var oB = get.byClass("resizeB", oDrag)[0];
	var oLT = get.byClass("resizeLT", oDrag)[0];
	var oTR = get.byClass("resizeTR", oDrag)[0];
	var oBR = get.byClass("resizeBR", oDrag)[0];
	var oLB = get.byClass("resizeLB", oDrag)[0];
	
	drag(oDrag, oTitle);
	//四角
	resize(oDrag, oDrag2,oLT, true, true, false, false);
	resize(oDrag, oDrag2,oTR, false, true, false, false);
	resize(oDrag, oDrag2,oBR, false, false, false, false);
	resize(oDrag, oDrag2,oLB, true, false, false, false);
	
	//四边
	resize(oDrag, oDrag2,oL, true, false, false, true);
	resize(oDrag, oDrag2,oT, false, true, true, false);
	resize(oDrag, oDrag2,oR, false, false, false, true);
	resize(oDrag, oDrag2,oB, false, false, true, false);
	
	oDrag.style.left = "100px";
	oDrag.style.top = "200px";
	
	var oMin = get.byClass("min", oDrag)[0];
	oMin.click();
	
	var topicId = $("#topicId").text();  
    $.ajax({
        type: "POST",
        url: "toTopicResult.do",
        traditional: true,
        data:{
        	topicId:topicId,
        	result:"查询"
        }, 
        cache: false,
        async: false,
        beforeSend: function () {
            	//alert("begin");
        },
        success: function (data) {
        	//alert("success");
            $('#textareaBox').val(data);
        },
        error: function (errorThrown) {
        	//alert("stop");
        	alert(errorThrown.readystate);
        	alert(errorThrown.status);
        }
    });	
}
</script>

</head>
<body>
<div class="i_header header" style="background-color:White">
	<div class="wrap">
		<!--div class="logo"><img src="images/logo.jpg"/></a></div-->
		<div class="nav">
			<ul>
				<form name='index_form' action='toIndex.do' method='post'>  
					<li><a href='toNewTopic.do'>发起统计</a></li>
					<li><a href='toCustomers.do'>客户关系</a></li>
					<li><a href='toBigData.do'>数据中心</a></li>
					<li><a href='toTopicIndex.do'>我的统计</a></li>
					<li><a href='javascript:document.index_form.submit();' title="首页">首页</a></li>
					<li onmouseover="showsub(this)" onmouseout="hidesub(this)" style="width: 120px;"><a>${user_name}</a-->
						<ul style="display:none;float:none;top-margin:0">     
							<li><a href="#" style="width: 130px; height: 30px; border: 1px solid #d2d2d2; ">密码修改</a></li> 
							<li><a href="toLogin.do" style="width: 130px; height: 30px; border: 1px solid #d2d2d2; ">退出</a></li>
						</ul> 
					</li>
				</form> 
			</ul>
		</div>
	</div>
</div>
<div class="h130"></div>
     <div class="container" id="box">
         <div class="col-md-3" style="padding:2em 0;float:left;">
			<button type="button" class="btn btn-success btn-block" id="generate-excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> 将表格转换为Excel</button>
		 </div>
		 <div class="h3 text-info" style="float:left;margin-left:200px;">
		     <p id="topicId" hidden="hidden">${topic_id}</p>
             <caption id="topicName" class="h3 text-info">${topic_name}</caption> 
		 </div>
         <table id="table" class="table table-bordered table-intel">  
			  <thead id="th">
  			  </thead>   
              <tbody id="tb" align="center" valign="middle">
              </tbody> 
         </table>
     </div>
<div class="h90"></div>
<div style="text-align:center;">
	<p>版权所有：资管PB实施团队</a></p>
</div>
<div id="drag" hidden="hidden">
    <div class="title">
        <h2>统计结果反馈</h2>
        <div>
            <a class="min" href="javascript:;" title="最小化"></a>
            <a class="max" href="javascript:;" title="最大化"></a>
            <a class="revert" href="javascript:;" title="还原"></a>
            <a class="close" href="javascript:;" title="关闭"></a>
        </div>
    </div>
    <div class="resizeL"></div>
    <div class="resizeT"></div>
    <div class="resizeR"></div>
    <div class="resizeB"></div>
    <div class="resizeLT"></div>
    <div class="resizeTR"></div>
    <div class="resizeBR"></div>
    <div class="resizeLB"></div>
    <div class="content">
        <textarea  type="text" style="width:780px;height:200px" id="textareaBox"></textarea>
        <h>统计原因:${topic_remark}</h>
    </div>    
</div>

</body>
<script type="text/javascript">
$(function() {   
	loadResize();
	var topicId = $("#topicId").text(); 
	//获取统计ID的所有统计信息
    $.ajax({
        type: "POST",
        url: "toCheckTopicData.do",
        traditional: true,
        data:{
        	topicId:topicId
        }, 
        cache: false,
        async: false,
        beforeSend: function () {
            	//alert("begin");
        },
        success: function (data) {
        	var thStr = '';
        	var tbStr = '';
        	thStr = thStr + '<tr>';
        	for(i = 0; i < data[0].length; i++){
        		//alert(data[0][i]);
        		thStr = thStr + '<th class="filter" style="min-width:200px;">'+data[0][i]+'</th>';
        	}
        	thStr = thStr + '</tr>';
        	$('#th').append(thStr);
        	
        	for(var j=1;j<data.length;j++){
        		tbStr = tbStr + '<tr>';
        		for(var index=0;index<data[j].length;index++){
        			var infoArr = data[j][index].split("__");
        			if(infoArr.length == 2){
        				switch(infoArr[1])
        				{
        				case "1":
        				  tbStr = tbStr + '<td><font size="3" color="green">'+infoArr[0]+'</font></td>';
        				  break;
        				case "2":
        			      tbStr = tbStr + '<td>'+infoArr[0]+'</td>';
        				  break;
        				default:
        				  tbStr = tbStr + '<td><font size="3" color="red">'+infoArr[0]+'</font></td>';
        				}
        			}else{
            			tbStr = tbStr + '<td>'+infoArr[0]+'</td>';
        			}
        		}
        		tbStr = tbStr + '</tr>';
        	}
        	$('#tb').append(tbStr);
        },
        error: function (errorThrown) {
        	//alert("stop");
        	alert(errorThrown.readystate);
        	alert(errorThrown.status);
        }
    });
	
    //导出excel控件
    excel = new ExcelGen({
        "src_id": "table",
        "show_header": true
    });
    $("#generate-excel").click(function () {
        excel.generate();
    });
    
	//界面excel展示控件
    $('#table').excelTableFilter({
    	'captions':{ a_to_z: '升序排列', z_to_a: '降序排列', search: '搜索', select_all: '全部选择' }
      });
    
});

</script>
</html>