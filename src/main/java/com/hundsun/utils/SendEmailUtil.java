package com.hundsun.utils;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Properties;
import javax.mail.internet.MimeMessage;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.multipart.MultipartFile;

/** 
 isValate:是否校验（或者授权）：true 
 to：邮件接收者地址数组。：{"***@qq.com","***@qq.com"}，因为是数组所以支持群发。 
 subject:邮件主题 
 context：邮件内容 
 */

public class SendEmailUtil{
	public static void sendEmails(String isValate, String[] to,String subject, String context) throws Exception{	
        //取配置文件里面的邮件发送人配置
		String sendMailStr = "";
        String sendMailNameStr ="";
        String sendMailPwdStr = "";
		try {
            //ini文件的存放位置
            String filepath = "D:\\mailPro.ini";
            //创建文件输入流
            FileInputStream fis = new FileInputStream(filepath);

            //创建Properties属性对象用来接收ini文件中的属性
            Properties pps = new Properties();
            //从文件流中加载属性
            pps.load(fis);
            //通过getProperty("属性名")获取key对应的值
            sendMailStr = pps.getProperty("sendMail");
            sendMailNameStr = pps.getProperty("sendMailName");
            sendMailPwdStr = pps.getProperty("sendMailPwd");

        }
        catch (Exception e) {
            e.printStackTrace();
        }
		
		//邮件服务器的配置信息  
		JavaMailSenderImpl senderImpl = new JavaMailSenderImpl();
		senderImpl.setHost("mail.hundsun.com");//发件服务器HOST
		// 根据自己的情况,设置username  
		senderImpl.setUsername(sendMailNameStr);//发件人名称  
		// 根据自己的情况, 设置password  
		senderImpl.setPassword(sendMailPwdStr);//发件邮箱密码 
		
		Properties pp = new Properties();
		pp.put("mail.smtp.auth", isValate);//是否校验（或者授权）：true  
		senderImpl.setJavaMailProperties(pp);
		
		// 获取邮件的样板 
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setFrom(sendMailStr);// 发邮件邮箱  
		msg.setTo(to);
		msg.setSubject(subject);// 邮件主题  
		msg.setText(context);
		
		MimeMessage mimeMsg = senderImpl.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMsg, true, "UTF-8");
		helper.setTo(msg.getTo());//邮件接收地址  
		helper.setSubject(msg.getSubject());//邮件主题  
		helper.setFrom(msg.getFrom(), "统计发起人");//邮件发送人-别名  
		helper.setText(msg.getText(), true);//邮件内容 
		senderImpl.send(mimeMsg);//发送邮件  
	}
	public static void sendAttachmentEmails(String isValate, String[] to,String subject, String context,MultipartFile file) throws Exception{
        //取配置文件里面的邮件发送人配置
		String sendMailStr = "";
        String sendMailNameStr ="";
        String sendMailPwdStr = "";
		try {
            //ini文件的存放位置
            String filepath = "D:\\mailPro.ini";
            //创建文件输入流
            FileInputStream fis = new FileInputStream(filepath);

            //创建Properties属性对象用来接收ini文件中的属性
            Properties pps = new Properties();
            //从文件流中加载属性
            pps.load(fis);
            //通过getProperty("属性名")获取key对应的值
            sendMailStr = pps.getProperty("sendMail");
            sendMailNameStr = pps.getProperty("sendMailName");
            sendMailPwdStr = pps.getProperty("sendMailPwd");

        }
        catch (Exception e) {
            e.printStackTrace();
        }
		
		//邮件服务器的配置信息  
		JavaMailSenderImpl senderImpl = new JavaMailSenderImpl();
		senderImpl.setHost("mail.hundsun.com");//发件服务器HOST
		// 根据自己的情况,设置username  
		senderImpl.setUsername(sendMailNameStr);//发件人名称  
		// 根据自己的情况, 设置password  
		senderImpl.setPassword(sendMailPwdStr);//发件邮箱密码 
		
		Properties pp = new Properties();
		pp.put("mail.smtp.auth", isValate);//是否校验（或者授权）：true  
		senderImpl.setJavaMailProperties(pp);
		
		// 获取邮件的样板 
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setFrom(sendMailStr);// 发邮件邮箱  
		msg.setTo(to);
		msg.setSubject(subject);// 邮件主题  
		msg.setText(context);
		
		MimeMessage mimeMsg = senderImpl.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMsg, true, "UTF-8");
		helper.setTo(msg.getTo());//邮件接收地址  
		helper.setSubject(msg.getSubject());//邮件主题  
		helper.setFrom(msg.getFrom(), "统计发起人");//邮件发送人-别名  
		helper.setText(msg.getText(), true);//邮件内容 
		helper.addAttachment(file.getOriginalFilename(), file);
		senderImpl.send(mimeMsg);//发送邮件  
	}
}
