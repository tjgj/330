package com.hundsun.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.hundsun.dao.UserDAO;
import com.hundsun.entity.User;
import com.hundsun.utils.SHAencrypt;

@Service("loginService")
public class LoginServiceImpl implements LoginService {
	
	@Resource(name="userDAO")
	private UserDAO uDAO;

	public User checkLogin(int userID, String pwd) throws Exception {
		User user = uDAO.findByUserCode(userID);
		SHAencrypt sha = new SHAencrypt();
		if(user == null){
				//帐号不存在,抛一个应用异常。
				//注：应用异常，指的是因为用户
				//不正确的操作引起的异常，比如
				//帐号、密码填写错误。需要明确
				//提示用户采取正确的操作。
			throw new ApplicationException(
						"帐号错误");
		}
		if(!sha.checkPassword(pwd, user.getPwd())){
				//密码错误
			throw new ApplicationException("密码错误");
		}
		//登录成功
		return user;
	}

	public User register(int userId, String pwd, String name, String email,String department, String area, String group) {
		User user = uDAO.findByUserCode(userId);
		if(user == null) {
			User registerUser = new User();
			registerUser.setId(userId);
			registerUser.setName(name);
			registerUser.setPwd(pwd);
			registerUser.setEmail(email);
			registerUser.setDepartment(department);
			registerUser.setArea(area);
			registerUser.setGroup(group);
			uDAO.save(registerUser);
			user = registerUser;
		}else {
			return null;
		}
		return user;
	}

}
