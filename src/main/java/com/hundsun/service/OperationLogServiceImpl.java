package com.hundsun.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.swing.text.html.StyleSheet.ListPainter;

import org.springframework.stereotype.Service;

import com.hundsun.dao.OperationLogDAO;
import com.hundsun.entity.OperationLog;


@Service("operationLogService")
public class OperationLogServiceImpl implements OperationLogService {
	
	@Resource(name="operationLogDAO")
	private OperationLogDAO operationLogDAO;

	public void saveOperationLog(String user, int style, int status, String target, String log) {
		// TODO Auto-generated method stub
		OperationLog operationLog = new OperationLog();
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String curdate = simpleDateFormat.format(date);
		String changeTime = curdate;
		
		operationLog.setOperation_user(user);
		operationLog.setOperation_style(style);
		operationLog.setOperation_status(status);
		operationLog.setOperation_target(target);
		operationLog.setOperation_log(log);
		operationLog.setOperation_time(changeTime);
		operationLogDAO.save(operationLog);
		
	}
	

}
