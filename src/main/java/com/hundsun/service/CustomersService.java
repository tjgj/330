package com.hundsun.service;

import java.util.List;

import com.hundsun.entity.Customers;
import com.hundsun.entity.User;

public interface CustomersService {
	
	public Customers findCustomerById(int id) ;
	
	public List<Customers> getCustomersByCommissionerService(String firstCommissioner,String secondCommissioner) ;
	
	//public List<Customers> getAllCustomersService() ;
	public String getAllCustomersToJson(User user);
	
	public int saveCustomerService(int customerId,String customerName,String customerArea,String system,String systemVersion,
			int systemStatus,String firstCommissioner,String secondCommissioner) ;
	public void saveCustomerService(List<List<Object>> cuslist);
	
	public int updateCustomerService(int id,int customerId,String customerName,String customerArea,String system,String systemVersion,
			int systemStatus,String firstCommissioner,String secondCommissioner) ;
	
	public void stopCustomerService(Customers customer) ;
	public void updateCustomerSystemVersion(String id,String systemVersion,String changeUser) ;
	public int deleteCustomerService(int id) ;
	
	
	
}
