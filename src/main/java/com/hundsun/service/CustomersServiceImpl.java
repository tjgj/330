package com.hundsun.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.hundsun.dao.CustomersDAO;
import com.hundsun.dao.DataSheetDAO;
import com.hundsun.dao.TopicDAO;
import com.hundsun.dao.TopicFieldDAO;
import com.hundsun.entity.Customers;
import com.hundsun.entity.DataSheet;
import com.hundsun.entity.User;
import com.hundsun.entity.Topic;
import com.hundsun.entity.TopicField;
import com.hundsun.service.OperationLogService;

@Service("customersService")
public class CustomersServiceImpl implements CustomersService {
	
	@Resource(name="customersDAO")
	private CustomersDAO customersDAO;
	@Resource(name="topicDAO")
	private TopicDAO topicDAO;	
	@Resource(name="topicFieldDAO")
	private TopicFieldDAO topicFieldDAO;
	@Resource(name="dataSheetDAO")
	private DataSheetDAO dataSheetDAO;
	
	@Resource(name="operationLogService")
	private OperationLogService operationLogService;

	public Customers findCustomerById(int id) {
		return customersDAO.findByCustomersId(id);
	}
	
	public List<Customers> getCustomersByCommissionerService(String firstCommissioner,String secondCommissioner) {
		// TODO Auto-generated method stub
		List<Customers> list  = customersDAO.findByCommissioner(firstCommissioner, secondCommissioner);
		return list;
	}
	
	//public List<Customers> getAllCustomersService() {
	//	List<Customers> list  = customersDAO.findAllCustomers();
	//	return list;
	//}
	
	public String getAllCustomersToJson(User user) {
		List<Customers> list  = customersDAO.findAllNormalCustomers(user.getDepartment());
	
		StringBuffer buf=new StringBuffer();
		buf.append("[");
		for (int a = 0; a < list.size();a++) {
			//json拼接""
			buf.append("{");

			buf.append("id:");
			buf.append(list.get(a).getId());
			buf.append(",");
			
			buf.append("customerId:");
			buf.append("\"");
			buf.append(list.get(a).getCustomer_id());
			buf.append("\"");
			buf.append(",");
			
			buf.append("customerName:");
			buf.append("\"");
			buf.append(list.get(a).getCustomer_name());
			buf.append("\"");
			buf.append(",");
			
			buf.append("customerArea:");
			buf.append("\"");
			buf.append(list.get(a).getCustomer_area());
			buf.append("\"");
			buf.append(",");
			
			buf.append("system:");
			buf.append("\"");
			buf.append(list.get(a).getSystem());
			buf.append("\"");
			buf.append(",");
			
			buf.append("systemVersion:");
			buf.append("\"");
			buf.append(list.get(a).getSystem_version());
			buf.append("\"");
			buf.append(",");
			
			buf.append("firstCommissioner:");	
			buf.append("\"");
			buf.append(list.get(a).getFirst_commissioner());
			buf.append("\"");
			buf.append(",");
			
			buf.append("secondCommissioner:");	
			buf.append("\"");
			buf.append(list.get(a).getSecond_commissioner());
			buf.append("\"");
			buf.append("}");
			//最后一个“,”本该去掉，但是前台控件能兼容，所以不管了
			buf.append(",");
		}
		buf.append("]");
		return buf.toString();
	}
	
	public void saveCustomerService(List<List<Object>> cuslist) {
		//循环数组插入统计话题字段。
		int systemStatus = 1;
		int customerId = 0;

		for(int i=0;i<cuslist.size();i++) {
			//customerId转换int
			try {
				customerId = Integer.parseInt(cuslist.get(i).get(0).toString());
			} catch (NumberFormatException e) {
			    e.printStackTrace();
			}
			//保存客户
			this.saveCustomerService(customerId, cuslist.get(i).get(1).toString(),cuslist.get(i).get(2).toString(), 
					cuslist.get(i).get(3).toString(), cuslist.get(i).get(4).toString(), systemStatus,
					cuslist.get(i).get(5).toString(), cuslist.get(i).get(6).toString());	
		}
	}

	public int saveCustomerService(int customerId, String customerName, String customerArea, String system,
			String systemVersion, int systemStatus, String firstCommissioner, String secondCommissioner) {
		// TODO Auto-generated method stub
		int id = 0;
		Customers customer = new Customers();
		customer.setCustomer_id(customerId);
		customer.setCustomer_name(customerName);
		customer.setCustomer_area(customerArea);
		customer.setSystem(system);
		customer.setSystem_version(systemVersion);
		customer.setSystem_status(systemStatus);
		customer.setFirst_commissioner(firstCommissioner);
		customer.setSecond_commissioner(secondCommissioner);
		customersDAO.save(customer);
		//增加新客户bigdata数据
		//查询已有bigdata统计数据
		List<Topic> topicList = topicDAO.getAllExcelTopic(system);
		
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String curdate = simpleDateFormat.format(date);

		//循环查找TopicField数据，插入datasheet。
		String fieldValue = "";
		DataSheet dataSheet = new DataSheet();
		for(int i = 0; i< topicList.size();i++) {
			List<TopicField> topicFieldList = topicFieldDAO.getTopicFieldByTopicId(topicList.get(i).getId());
			for(int j=0;j<topicFieldList.size();j++) {
				dataSheet.setDataSheet(topicList.get(i).getId(), topicList.get(i).getTopic_name(), customer.getId(), system, 
						customerName, firstCommissioner,topicFieldList.get(j).getFieldSequence(), 
						topicFieldList.get(j).getTopicFieldName(), topicFieldList.get(j).getFieldContent(),fieldValue,curdate);	
				dataSheetDAO.save(dataSheet);
			}
		}
		
		id = customer.getId();
		return id;
	}

	public int updateCustomerService(int id, int customerId, String customerName, String customerArea, String system,
			String systemVersion, int systemStatus, String firstCommissioner, String secondCommissioner) {
		// TODO Auto-generated method stub
		Customers customer = new Customers();
		customer.setCustomer_id(customerId);
		customer.setCustomer_name(customerName);
		customer.setCustomer_area(customerArea);
		customer.setSystem(system);
		customer.setSystem_version(systemVersion);
		customer.setSystem_status(systemStatus);
		customer.setFirst_commissioner(firstCommissioner);
		customer.setSecond_commissioner(secondCommissioner);
		customer.setId(id);
		customersDAO.update(customer);
		dataSheetDAO.updateSystemByCusId(id, system);
		return id;
	}
	
	public void stopCustomerService(Customers customer) {
		customersDAO.update(customer);
		dataSheetDAO.updateSystemByCusId(customer.getId(), "");
	}
	
	public void updateCustomerSystemVersion(String id,String systemVersion,String changeUser) {		
		int idToINT = Integer.parseInt(id);
		Customers customer = new Customers();
		customer = this.findCustomerById(idToINT);
		if(customer.getSystem_version().toString().equals(systemVersion)) {
		}else {
			operationLogService.saveOperationLog(changeUser, 1, 1,
					customer.getCustomer_name(), "原值："+ customer.getSystem_version() +"，修改为："+systemVersion);
			customer.setSystem_version(systemVersion);
			customersDAO.update(customer);		
		}
	}

	//不适用，数据不删除
	public int deleteCustomerService(int id) {
		// TODO Auto-generated method stub
		Customers customer = new Customers();
		customer = customersDAO.findByCustomersId(id);
		customersDAO.delete(customer);
		return id;
	}



}
