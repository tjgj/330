package com.hundsun.service;

import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.hundsun.dao.CustomersDAO;
import com.hundsun.dao.TopicDAO;
import com.hundsun.dao.TopicFieldDAO;
import com.hundsun.dao.DataSheetDAO;
import com.hundsun.dao.UserDAO;
import com.hundsun.entity.User;
import com.hundsun.entity.Topic;
import com.hundsun.entity.TopicField;
import com.hundsun.entity.Customers;
import com.hundsun.entity.DataSheet;
import com.hundsun.service.OperationLogService;
import com.hundsun.utils.SendEmailUtil;

@Service("topicService")
public class TopicServiceImpl implements TopicService{
	
	@Resource(name="topicDAO")
	private TopicDAO topicDAO;	
	@Resource(name="topicFieldDAO")
	private TopicFieldDAO topicFieldDAO;
	@Resource(name="customersDAO")
	private CustomersDAO customersDAO;
	@Resource(name="dataSheetDAO")
	private DataSheetDAO dataSheetDAO;
	@Resource(name="userDAO")
	private UserDAO userDAO;
	
	@Resource(name="operationLogService")
	private OperationLogService operationLogService;

	
	public List<Object> getAllNormalTopicAndUnfinished(User user){
		List<Object> topicList = topicDAO.getAllNormalTopicAndUnfinished(user.getDepartment(),user.getName());
		return topicList;
	}
	public List<Object> getAllExcelTopicAndMaxChangeTime(User user){
		List<Object> topicList = topicDAO.getAllExcelTopicAndMaxChangeTime(user.getDepartment(),user.getName());
		return topicList;
	}


	public List<TopicField> getTopicFieldByTopicId(int topicId) {
		// TODO Auto-generated method stub
		List<TopicField> topicFieldList = topicFieldDAO.getTopicFieldByTopicId(topicId);
		return topicFieldList;
	}

	public Topic getTopicService(int id) {
		// TODO Auto-generated method stub
		Topic topic = new Topic();
	    topic = topicDAO.findByTopicID(id);
		if(topic == null){
				//统计话题不存在,抛一个应用异常。
			throw new ApplicationException(
						"无此统计话题");
		}		
		//返回查询结果
		return topic;
	}

	public int setTopicService(String topicName, int topicStatus, String releaseUser, String releaseTime,
			String completeTime, int statisticalRange, String remark) {
		// TODO Auto-generated method stub
		int tpoicID = 0;
		Topic topic = new Topic();
		topic.setTopic(topicName, topicStatus, releaseUser, releaseTime, completeTime, statisticalRange, remark);		
		topicDAO.save(topic);
		tpoicID = topic.getId();
		return tpoicID;
	}
	//保存统计反馈结果
	public String setTopicResult(String topicId,String result) {
		// TODO Auto-generated method stub
		int topicIdToINT = 0;
		try {
			topicIdToINT = Integer.parseInt(topicId);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		
		Topic topic = topicDAO.findByTopicID(topicIdToINT);
		topic.setResult(result);
		topicDAO.update(topic);
		return topic.getResult();
	}

	public TopicField getTopicFieldService(int id) {
		// TODO Auto-generated method stub
		TopicField topicField = new TopicField();
		topicField = topicFieldDAO.findByTopicFieldID(id);
		if(topicField == null){
				//统计话题不存在,抛一个应用异常。
			throw new ApplicationException(
						"无此统计话题字段");
		}		
		//返回查询结果
		return topicField;
	}

	public void setTopicFieldService(int topicId, int fieldSequence, String fieldName, int fieldIsnull, int fieldStatus,
			String fieldContent) {
		// TODO Auto-generated method stub
		int tpoicFieldID = 0;
		TopicField topicField = new TopicField();
		topicField.setTopicField(topicId,fieldSequence ,fieldName, fieldIsnull, fieldStatus, fieldContent);	
		topicFieldDAO.save(topicField);
	}

	//手工统计发起，插入topic表，topicfield表，datasheet表
	public int bulidTopicService(String[][] fieldData, String topicName, String completeTime, String remark,
			String topicStatus, String topicRange,String fieldIsnull,String fieldStatus, String releaseUser,String system) throws Exception {
		// TODO Auto-generated method stub
		//topicStatus转换int
		int topicStatustoINT = 0;
		try {
			topicStatustoINT = Integer.parseInt(topicStatus);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		//topicRange转换int
		int topicRangetoINT = 0;
		try {
			topicRangetoINT = Integer.parseInt(topicRange);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		//fieldIsnull转换int
		int fieldIsnulltoINT = 0;
		try {
			fieldIsnulltoINT = Integer.parseInt(fieldIsnull);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		//topicRange转换int
		int fieldStatustoINT = 0;
		try {
			fieldStatustoINT = Integer.parseInt(fieldStatus);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
				
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String curdate = simpleDateFormat.format(date);
		String releaseTime = curdate;
		
		List<Customers> customerList = customersDAO.findAllNormalCustomers(system);//获取正常客户List
		DataSheet dataSheet = new DataSheet();
		String fieldValue = "";//创建datasheet表fieldValue赋予空置、


		//插入统计话题，返回统计话题编号
		int topicId = this.setTopicService(topicName, topicStatustoINT, releaseUser, 
				releaseTime, completeTime, topicRangetoINT, remark);

        //统计字段序列转义 
		int fieldSequence = 1;
		//循环数组插入统计话题字段。
		for(int a = 1; a < fieldData.length-2; a++,fieldSequence++) {
			//插入统计字段
			this.setTopicFieldService(topicId, fieldSequence, 
					fieldData[a][1], fieldIsnulltoINT, fieldStatustoINT, fieldData[a][2]);	
			//循环建立datasheet表空值数据
			for(int i = 0; i < customerList.size(); i++) {
				dataSheet.setDataSheet(topicId, topicName, customerList.get(i).getId(), system, 
						customerList.get(i).getCustomer_name(), customerList.get(i).getFirst_commissioner(),
						fieldSequence, fieldData[a][1], fieldData[a][2],fieldValue,releaseTime);	
				dataSheetDAO.save(dataSheet);
			}
		}
		
		return topicId;
	}
	
	//EXCEL导入统计发起创建topic\topicfield\datasheet表
	public int bulidTopicService(List<List<Object>> fieldData,String topicName,String completeTime,String 
			remark,String topicStatus,String topicRange,String fieldIsnull,String fieldStatus,String releaseUser,String system) throws Exception {	
		//topicStatus转换int
		int topicStatustoINT = 0;
		try {
			topicStatustoINT = Integer.parseInt(topicStatus);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		//topicRange转换int
		int topicRangetoINT = 0;
		try {
			topicRangetoINT = Integer.parseInt(topicRange);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		//fieldIsnull转换int
		int fieldIsnulltoINT = 0;
		try {
			fieldIsnulltoINT = Integer.parseInt(fieldIsnull);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		//topicRange转换int
		int fieldStatustoINT = 0;
		try {
			fieldStatustoINT = Integer.parseInt(fieldStatus);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
				
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String curdate = simpleDateFormat.format(date);
		String releaseTime = curdate;
		
		List<Customers> customerList = customersDAO.findAllNormalCustomers(system);;//获取客户List
		DataSheet dataSheet = new DataSheet();
		String fieldValue = "";//创建datasheet表fieldValue赋予空置、


		//插入统计话题，返回统计话题编号
		int topicId = this.setTopicService(topicName, topicStatustoINT, releaseUser, 
				releaseTime, completeTime, topicRangetoINT, remark);

        //统计字段序列转义 
		int fieldSequence = 1;
		//循环数组插入统计话题字段。
		List<Object> lo1 = fieldData.get(0);
		List<Object> lo2 = fieldData.get(1);
		
		for(int a = 0; a < lo1.size();a++,fieldSequence++) {
			//插入统计字段，返回字段编号
			this.setTopicFieldService(topicId, fieldSequence, 
					lo1.get(a).toString(), fieldIsnulltoINT, fieldStatustoINT, lo2.get(a).toString());	
			//循环建立datasheet表空值数据
			for(int i = 0; i < customerList.size(); i++) {
				dataSheet.setDataSheet(topicId, topicName, customerList.get(i).getId(), system, 
						customerList.get(i).getCustomer_name(), customerList.get(i).getFirst_commissioner(),
						fieldSequence, lo1.get(a).toString(), lo2.get(a).toString(),fieldValue,releaseTime);	
				dataSheetDAO.save(dataSheet);
			}
		}
		
		//发送统计发起邮件
        //取配置文件里面的邮件收件人配置
		String toMailStr = "";
		try {
            //ini文件的存放位置
            String filepath = "D:\\mailPro.ini";
            //创建文件输入流
            FileInputStream fis = new FileInputStream(filepath);

            //创建Properties属性对象用来接收ini文件中的属性
            Properties pps = new Properties();
            //从文件流中加载属性
            pps.load(fis);
            //通过getProperty("属性名")获取key对应的值
            toMailStr = pps.getProperty("toMailFor"+system);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
		String[] toMailArr = toMailStr.split(",");
		//发邮件
		String isValate = "true";//邮件授权
		String[] to = toMailArr;//收件人
		String subject = "【统计工具】"+topicName+"统计发起通告";//邮件主题
		StringBuffer str = new StringBuffer();
		str.append("<html><head>您好！新发数据中心：</head><body><table border=\"1\"><tr><td>统计编号</td><td>统计名称</td><td>截止时间</td></tr>");
		str.append("<tr><td>"+topicId+
				"</td><td>"+topicName+"</td><td>"+completeTime+"</td></tr>");
		str.append("</table>");
		str.append("<p></p><div>统计地址：http://10.20.29.51:8080/project/toLogin.do</div></body></html>");
		SendEmailUtil.sendEmails(isValate, to, subject, str.toString());
		return 0;
	}

	//根据用户和topicId获取datasheet数据
	public List<DataSheet> getDataSheetForWirteTopic(String topicId, String user,String customersId) {
		// TODO Auto-generated method stub
		int topicIdToINT = 0;
		try {
			topicIdToINT = Integer.parseInt(topicId);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		int customersIdToINT = 0;
		try {
			customersIdToINT = Integer.parseInt(customersId);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		List<DataSheet> list = dataSheetDAO.findForWriteTopic(topicIdToINT,customersIdToINT,user);
		return list;
	}
	
	//id和value循环String[]数组，保存datasheet
	public void updateDataSheetFieldValue(String[] data,String user) {
		// TODO Auto-generated method stub
		DataSheet dataSheet = new DataSheet();
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String curdate = simpleDateFormat.format(date);
		
		for(int i=0;i<data.length;i++) {	    
			dataSheet = dataSheetDAO.findById(Integer.parseInt(data[i]));
			i=i+1;
			
			//判断字段数据是否修改
			String bb = dataSheet.getField_value().toString();
			String aa = data[i].toString();
			if(dataSheet.getField_value().toString().equals(data[i].toString())) {
				//break;
			}else {
				//插入日志
				operationLogService.saveOperationLog(user, 2, 1,
						dataSheet.getTopic_name()+":"+dataSheet.getField_name(), "原值："+dataSheet.getField_value()+"，修改为："+data[i]);
				//保存修改数据
				dataSheet.setField_value(data[i]);
				dataSheet.setChange_time(curdate);
				dataSheet.setFill_user(user);
				dataSheetDAO.update(dataSheet);
			}
		}
	}
	

	
	//按部门获取未完成的统计项和名单
	public List<DataSheet> getUnfinishedDataSheet(String system) {
		// TODO Auto-generated method stub
		List<DataSheet> list = dataSheetDAO.findUnfinishedDataSheet(system);
		return list;
	}

	//根据topicId获取所有的统计记录,处理成二维数组
	public String[][] getDataSheetByTopicId(String topicId,User u) throws ParseException {
		// TODO Auto-generated method stub
		int topicIdToINT = 0;
		String system = u.getDepartment();//后面多部门使用后需要通过user关联送值
		try {
			topicIdToINT = Integer.parseInt(topicId);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		Topic topic = topicDAO.findByTopicID(topicIdToINT);//兼容超户查阅，system重topic关联获取
		List<User> userList = userDAO.findUserByUserName(topic.getRelease_user());//兼容超户查阅，system重topic关联获取
		
		List<DataSheet> dataSheetList = dataSheetDAO.findDataSheetByTopicIdAndSystem(topicIdToINT,userList.get(0).getDepartment());
		List<Customers> cusList = customersDAO.findAllNormalCustomers(userList.get(0).getDepartment());
		List<TopicField> topicFieldList = topicFieldDAO.getTopicFieldByTopicId(topicIdToINT);
		int rowLength = cusList.size();
		int ColumnLength = topicFieldList.size() + 4;
		String[][] strDatas = new String[rowLength+1][ColumnLength];
		int index= 0;
		
		Date date = new Date();			
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");		
		
		strDatas[0][0] = "客户名称";
		strDatas[0][1] = "片区";
		strDatas[0][2] = "系统版本";
		strDatas[0][3] = "维护专员";
		for(int head=0;head<topicFieldList.size();head++) {
			strDatas[0][head+4] = topicFieldList.get(head).getTopicFieldName();
		}
	
		for(int cusIndex = 0 ; cusIndex<cusList.size();cusIndex++,index++) {
			strDatas[cusIndex+1][0] = cusList.get(cusIndex).getCustomer_name();
			strDatas[cusIndex+1][1] = cusList.get(cusIndex).getCustomer_area();
			strDatas[cusIndex+1][2] = cusList.get(cusIndex).getSystem_version();
			strDatas[cusIndex+1][3] = cusList.get(cusIndex).getFirst_commissioner();
			for(int topicFieldIndex = 0;topicFieldIndex<topicFieldList.size();topicFieldIndex++) {
				if(index*topicFieldList.size() >= dataSheetList.size()) {
					strDatas[cusIndex+1][topicFieldIndex+4] = "-";
					//break;//新加客户无datasheet数据保护
				}else {
					
					Date changeDate = simpleDateFormat.parse(dataSheetList.get(index*topicFieldList.size()+topicFieldIndex).getChange_time());		
					int days = (int) ((date.getTime() - changeDate.getTime()) / (1000*3600*24));
					if (Math.abs(days)<=7) {
						strDatas[cusIndex+1][topicFieldIndex+4] = 
								dataSheetList.get(index*topicFieldList.size()+topicFieldIndex).getField_value()+"__1";
					}else {
						if(Math.abs(days)<=30) {
							strDatas[cusIndex+1][topicFieldIndex+4] = 
									dataSheetList.get(index*topicFieldList.size()+topicFieldIndex).getField_value()+"__2";
						}else {
							strDatas[cusIndex+1][topicFieldIndex+4] = 
									dataSheetList.get(index*topicFieldList.size()+topicFieldIndex).getField_value()+"__3";
						}
					}
				}

			}
		}
		return strDatas;
	}
	
	//通过fieldname\topicName模糊匹配查询统计主题
	public List<Topic> findTopicNameByFieldNameAndTopicName(String fieldName,String topicName,String range,User u) {
		// TODO Auto-generated method stub
		int rangeToINT = 0;
		String system = u.getDepartment();//后面多部门使用后需要通过user关联送值
		try {
			rangeToINT = Integer.parseInt(range);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		
		String topicStr = "(";
		List<TopicField> lo = topicFieldDAO.findTopicNameListByFieldName(fieldName,topicName,rangeToINT);
		for(int i = 0;i<lo.size();i++) {
			int ob = lo.get(i).getTopic_id();
			topicStr = topicStr +lo.get(i).getTopic_id()+",";
		}
		if(lo.size() == 0) {	
			topicStr = topicStr +"''";
		}else
		{
			topicStr = topicStr.substring(0,topicStr.length()-1); 
		}
		topicStr = topicStr +")";
		List<Topic> list = topicDAO.findTopicListByIdStr(topicStr,system);
		return list;
	}
	
	//通过topicId查询topicfield的list拼接成json返回
	public String findTopicFieldByTopicIdToJson(String topicId) {
		// TODO Auto-generated method stub
		int topicIdToINT = 0;
		try {
			topicIdToINT = Integer.parseInt(topicId);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		List<TopicField> list  = topicFieldDAO.getTopicFieldByTopicId(topicIdToINT);
		
		StringBuffer buf=new StringBuffer();
		buf.append("[");
		for (int a = 0; a < list.size();a++) {
			//json拼接""
			buf.append("{");

			buf.append("id:");
			buf.append(list.get(a).getId());
			buf.append(",");
			
			buf.append("fieldSequence:");
			buf.append("\"");
			buf.append(list.get(a).getFieldSequence());
			buf.append("\"");
			buf.append(",");
			
			buf.append("fieldName:");
			buf.append("\"");
			buf.append(list.get(a).getTopicFieldName());
			buf.append("\"");
			buf.append(",");
						
			buf.append("fieldContent:");	
			buf.append("\"");
			buf.append(list.get(a).getFieldContent());
			buf.append("\"");
			buf.append("}");
			//最后一个“,”本该去掉，但是前台控件能兼容，所以不管了
			buf.append(",");
		}
		buf.append("]");
		return buf.toString();
	}
	
	//修改topicfield\同步修改datasheet
	public String changeTopicField(String postId, String fieldSequence, String fieldName, String fieldContent) {
		// TODO Auto-generated method stub
		int id = 0;
		try {
			id = Integer.parseInt(postId);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		int fieldSequenceToINT = 0;
		try {
			fieldSequenceToINT = Integer.parseInt(fieldSequence);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		
		TopicField topicField = new TopicField();
		topicField = topicFieldDAO.findByTopicFieldID(id);
		if(topicField.getTopicFieldName().equals(fieldName)) {
		}else {
			//修改fieldName
			dataSheetDAO.updateFieldNameByTopicIdAndFieldSequence(topicField.getTopic_id(), fieldSequenceToINT, fieldName);
			topicField.setField_name(fieldName);
			topicFieldDAO.update(topicField);
		}
		if(topicField.getFieldContent().equals(fieldContent)) {
		}else {
			//修改fieldContent
			dataSheetDAO.updateFieldContentByTopicIdAndFieldSequence(topicField.getTopic_id(), fieldSequenceToINT, fieldContent);
			topicField.setField_content(fieldContent);
			topicFieldDAO.update(topicField);
		}
		if(topicField.getFieldSequence() == fieldSequenceToINT ) {
		}else {
			//修改sequ
			if(fieldSequenceToINT > topicField.getFieldSequence()) {
				List<TopicField> fieldList = topicFieldDAO.getTopicFieldCompareByFieldSequence(topicField.getTopic_id(), fieldSequenceToINT);	
				//所有大于等于fieldSequenceToINT的topicField和datasheet往后移一位
				for(int i = 0 ;i<fieldList.size();i++) {
					dataSheetDAO.updateFieldSequenceByTopicIdAndFieldSequence(topicField.getTopic_id(), 
							fieldList.get(i).getField_sequence(), fieldList.get(i).getField_sequence()+1);
					//topicField后移
					fieldList.get(i).setField_sequence(fieldList.get(i).getField_sequence()+1);
					topicFieldDAO.update(fieldList.get(i));
				}
				//正式修改sequ
				dataSheetDAO.updateFieldSequenceByTopicIdAndFieldSequence(topicField.getTopic_id(),topicField.getField_sequence(),fieldSequenceToINT);
				topicField.setField_sequence(fieldSequenceToINT);
				topicFieldDAO.update(topicField);
			}else {
				
				List<TopicField> fieldList = topicFieldDAO.getTopicFieldCompareByFieldSequence(topicField.getTopic_id(),fieldSequenceToINT,topicField.getFieldSequence());
				dataSheetDAO.updateFieldSequenceByTopicIdAndFieldSequence(topicField.getTopic_id(), topicField.getField_sequence(), topicField.getId()*100);
				topicField.setField_sequence(topicField.getId()*100);
				topicFieldDAO.update(topicField);
				//所有大于等于fieldSequenceToINT小于topicField.getFieldSequence()的datasheet往后移一位，topicfied先调整seq到1000，在改回。
				for(int i = 0 ;i<fieldList.size();i++) {
					dataSheetDAO.updateFieldSequenceByTopicIdAndFieldSequence(topicField.getTopic_id(), 
							fieldList.get(i).getField_sequence(), fieldList.get(i).getField_sequence()+1);
					//topicField后移
					fieldList.get(i).setField_sequence(fieldList.get(i).getField_sequence()+1);
					topicFieldDAO.update(fieldList.get(i));
				}
				//将1000改回fieldSequenceToINT
				dataSheetDAO.updateFieldSequenceByTopicIdAndFieldSequence(topicField.getTopic_id(), topicField.getField_sequence(),fieldSequenceToINT);
				topicField.setField_sequence(fieldSequenceToINT);
				topicFieldDAO.update(topicField);		
			}

		}
		return null;
	}
	
	//删除topicfield\同步删除datasheet
	public void deleteTopicField(String postId) {
		// TODO Auto-generated method stub
		int id = 0;
		try {
			id = Integer.parseInt(postId);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		TopicField topicField = new TopicField();
		topicField = topicFieldDAO.findByTopicFieldID(id);
		dataSheetDAO.deleteByTopicIdAndFieldSequence(topicField.getTopic_id(),topicField.getField_sequence());
		topicFieldDAO.delete(topicField);
	}
	
	public void saveTopicField(String topicId, String fieldSequence, String fieldName, int field_isnull, int field_status,
			String fieldContent,User u) {
		// TODO Auto-generated method stub
		int topicIdToINT = 0;
		try {
			topicIdToINT = Integer.parseInt(topicId);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		int fieldSequenceToINT = 0;
		try {
			fieldSequenceToINT = Integer.parseInt(fieldSequence);
		} catch (NumberFormatException e) {
		    e.printStackTrace();
		}
		
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String curdate = simpleDateFormat.format(date);
		
		TopicField topicField = new TopicField();
		//所有大于等于fieldSequenceToINT的topicField和datasheet往后移一位
		List<TopicField> fieldList = topicFieldDAO.getTopicFieldCompareByFieldSequence(topicIdToINT, fieldSequenceToINT);
		for(int i = 0 ;i<fieldList.size();i++) {
			dataSheetDAO.updateFieldSequenceByTopicIdAndFieldSequence(topicIdToINT, 
					fieldList.get(i).getField_sequence(), fieldList.get(i).getField_sequence()+1);
			fieldList.get(i).setField_sequence(fieldList.get(i).getField_sequence()+1);
			topicFieldDAO.update(fieldList.get(i));
		}
		//保存topicField
		topicField.setTopic_id(topicIdToINT);
		topicField.setField_sequence(fieldSequenceToINT);
		topicField.setField_isnull(field_isnull);
		topicField.setField_status(field_status);
		topicField.setField_name(fieldName);
		topicField.setField_content(fieldContent);
		topicFieldDAO.save(topicField);		
		//在datasheet表为每家客户生成新的topicField
		Topic topic = topicDAO.findByTopicID(topicIdToINT);
		String system = u.getDepartment();//后面多部门使用后需要通过user关联送值
		List<Customers> customerList = customersDAO.findAllNormalCustomers(system);//获取客户List
		DataSheet dataSheet = new DataSheet();
		String fieldValue = "";//创建datasheet表fieldValue赋予空置、
		//循环建立datasheet表空值数据
		for(int i = 0; i < customerList.size(); i++) {
			dataSheet.setDataSheet(topicIdToINT, topic.getTopic_name(), customerList.get(i).getId(), system, 
					customerList.get(i).getCustomer_name(), customerList.get(i).getFirst_commissioner(),
					fieldSequenceToINT,fieldName,fieldContent,fieldValue,curdate);	
			dataSheetDAO.save(dataSheet);
		}
	}
	
	//发送附件邮件
	public void sendAttachmentEmails(String topicInfo,MultipartFile file,String system) throws Exception{
        //取配置文件里面的邮件收件人配置
		String toMailStr = "";
		try {
            //ini文件的存放位置
            String filepath = "D:\\mailPro.ini";
            //创建文件输入流
            FileInputStream fis = new FileInputStream(filepath);

            //创建Properties属性对象用来接收ini文件中的属性
            Properties pps = new Properties();
            //从文件流中加载属性
            pps.load(fis);
            //通过getProperty("属性名")获取key对应的值
            toMailStr = pps.getProperty("toMailFor"+system);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
		String[] toMailArr = toMailStr.split(",");
		
		String[] info  = topicInfo.split("_");
 		String isValate = "true";//邮件授权
		String[] to = toMailArr;//收件人
		String subject = "【统计工具】"+info[1]+"统计发起通告";//邮件主题
		StringBuffer str = new StringBuffer();
		str.append("<html><head>您好！新发起我的统计：</head><body><table border=\"1\"><tr><td>统计编号</td><td>统计名称</td><td>截止时间</td><td>统计原因</td></tr>");
		str.append("<tr><td>"+info[0]+
				"</td><td>"+info[1]+"</td><td>"+info[2]+"</td><td>"+info[3]+"</td></tr>");
		str.append("</table>");
		str.append("<p></p><div>统计地址：http://10.20.29.51:8080/project/toLogin.do</div></body></html>");
		SendEmailUtil.sendAttachmentEmails(isValate, to, subject, str.toString(), file);
		
	}
	
	//发送普通邮件
	public void sendNormalEmails(String topicInfo,String system) throws Exception{
        //取配置文件里面的邮件收件人配置
		String toMailStr = "";
		try {
            //ini文件的存放位置
            String filepath = "D:\\mailPro.ini";
            //创建文件输入流
            FileInputStream fis = new FileInputStream(filepath);

            //创建Properties属性对象用来接收ini文件中的属性
            Properties pps = new Properties();
            //从文件流中加载属性
            pps.load(fis);
            //通过getProperty("属性名")获取key对应的值
            toMailStr = pps.getProperty("toMailFor"+system);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
		String[] toMailArr = toMailStr.split(",");
		
		String[] info  = topicInfo.split("_");
 		String isValate = "true";//邮件授权
		String[] to = toMailArr;//收件人
		String subject = "【统计工具】"+info[1]+"统计发起通告";//邮件主题
		StringBuffer str = new StringBuffer();
		str.append("<html><head>您好！新发起我的统计：</head><body><table border=\"1\"><tr><td>统计编号</td><td>统计名称</td><td>截止时间</td><td>统计原因</td></tr>");
		str.append("<tr><td>"+info[0]+
				"</td><td>"+info[1]+"</td><td>"+info[2]+"</td><td>"+info[3]+"</td></tr>");
		str.append("</table>");
		str.append("<p></p><div>统计地址：http://10.20.29.51:8080/project/toLogin.do</div></body></html>");
		SendEmailUtil.sendEmails(isValate, to, subject, str.toString());
		
	}
	
	
	
}
