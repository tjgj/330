package com.hundsun.service;

import com.hundsun.entity.User;

public interface LoginService {

	public User checkLogin(int userID, String pwd) throws Exception;
	public User register(int userId, String pwd, String name, String email,String department, String area, String group);
}


