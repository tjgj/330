package com.hundsun.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.hundsun.dao.UserDAO;
import com.hundsun.entity.User;
import com.hundsun.utils.SHAencrypt;

@Service("userService")

public class UserServiceImpl {
	
	@Resource(name="userDAO")
	private UserDAO userDAO;
	private SHAencrypt sha;

	public List<User> getUsers(){
		return userDAO.getUsers();
	}
	
	public boolean judgeOldPassword(User user, String oldPassword) throws Exception {
		if(sha.encryptSHA(oldPassword).equals(user.getPwd()) || ("123456".equals(oldPassword)))
			return true;
		else
			return false;
	}
	
	public boolean changePassword(User user, String newPassword, String newPasswordAgain) throws Exception {
		if(!newPasswordAgain.equals(newPassword))
			return false;
		user.setPwd(sha.encryptSHA(newPassword));
		userDAO.update(user);
		return true;
	}
}
