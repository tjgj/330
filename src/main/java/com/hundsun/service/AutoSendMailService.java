package com.hundsun.service;

import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.hundsun.utils.SendEmailUtil;
import com.hundsun.service.TopicService;
import com.hundsun.entity.DataSheet;
import com.hundsun.entity.Topic;


@Service("autoSendMailService")
public class AutoSendMailService {
	
	@Resource(name="topicService")
	private TopicService topicService;
	
	public void sendMails() throws Exception {
		//定义系统使用部门
		String useSystem = "PB";
		String[] systemArr = useSystem.split(",");
		for(int j=0;j<systemArr.length;j++) {
			//取配置文件里面的邮件收件人配置
			String toMailStr = "";
			try {
	            //ini文件的存放位置
	            String filepath = "D:\\mailPro.ini";
	            //创建文件输入流
	            FileInputStream fis = new FileInputStream(filepath);

	            //创建Properties属性对象用来接收ini文件中的属性
	            Properties pps = new Properties();
	            //从文件流中加载属性
	            pps.load(fis);
	            //通过getProperty("属性名")获取key对应的值
	            toMailStr = pps.getProperty("toMailFor"+systemArr[j]);
	        }
	        catch (Exception e) {
	            e.printStackTrace();
	        }
			String[] toMailArr = toMailStr.split(",");
			
			
			String isValate = "true";//邮件授权  
			String subject = "【统计工具】"+systemArr[j]+"统计未完成人员通告";//邮件主题  
			StringBuffer str = new StringBuffer();
			int index = 0;
			str.append("<html><head>您好！未完成统计有：</head><body><table border=\"1\"><tr><td>统计编号</td><td>统计名称</td><td>填表人</td><td>截止时间</td><td>统计原因</td></tr>");
			
			//List<DataSheet> list = topicService.getUnfinishedDataSheet();
			List<DataSheet> list = topicService.getUnfinishedDataSheet(systemArr[j]);
			for (int i=0;i<list.size();i++) {
				Topic topic = topicService.getTopicService(list.get(i).getTopic_id());
				String complete_time = topic.getComplete_time().split(" ")[0];

				Date date = new Date();			
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				
				Date completeDate = simpleDateFormat.parse(complete_time);
				
				int days = (int) ((completeDate.getTime() - date.getTime()) / (1000*3600*24));

				if (Math.abs(days)<2) {
					index++;
					str.append("<tr><td>"+list.get(i).getTopic_id()+
							"</td><td>"+list.get(i).getTopic_name()+"</td><td>"+list.get(i).getFill_user()+"</td><td>"+list.get(i).getComplete_time()+"</td><td>"+list.get(i).getRemark()+"</td></tr>");
				}
			}
			str.append("</table></body></html>");
			if(index==0) {		
			}else {
				SendEmailUtil.sendEmails(isValate, toMailArr, subject, str.toString());			
			}

			
		}

	}	
}
		

