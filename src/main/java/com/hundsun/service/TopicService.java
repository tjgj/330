package com.hundsun.service;

import java.text.ParseException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.hundsun.entity.Topic;
import com.hundsun.entity.TopicField;
import com.hundsun.entity.User;
import com.hundsun.entity.DataSheet;

public interface TopicService {
	public List<Object> getAllNormalTopicAndUnfinished(User user);
	public List<Object> getAllExcelTopicAndMaxChangeTime(User user);
	public Topic getTopicService(int id) ;
	public int setTopicService(String topicName,int topicStatus,String 
			releaseUser,String releaseTime,String completeTime,int statisticalRange ,String remark) ;
	//保存统计反馈结果
	public String setTopicResult(String topicId,String result);
	
	
	public List<TopicField> getTopicFieldByTopicId(int topicId);
	public TopicField getTopicFieldService(int id) ;
	public void setTopicFieldService(int topicId,int fieldSequence,String fieldName,int fieldIsnull,int 
			fieldStatus,String fieldContent) ;
	
	//手工统计发起创建topic\topicfield\datasheet表
	public int bulidTopicService(String[][] fieldData,String topicName,String completeTime,String 
			remark,String topicStatus,String topicRange,String fieldIsnull,String fieldStatus,String releaseUser,String sysyem) throws Exception ;
	
	//EXCEL导入统计发起创建topic\topicfield\datasheet表
	public int bulidTopicService(List<List<Object>> fieldData,String topicName,String completeTime,String 
			remark,String topicStatus,String topicRange,String fieldIsnull,String fieldStatus,String releaseUser,String system) throws Exception;
	
	//根据user/topicId/customersId获取datasheet数据
	public List<DataSheet> getDataSheetForWirteTopic(String topicId,String user,String customersId);
	
	//id和value循环String[]数组，保存datasheet
	public void updateDataSheetFieldValue(String[] data,String user);
	
	//按部门获取未完成的统计项和名单
	public List<DataSheet> getUnfinishedDataSheet(String system);
	
	//根据topicId获取所有的统计记录,处理成二维数组
	public String[][] getDataSheetByTopicId(String topicId,User u) throws ParseException;
	
	//通过fieldname\topicName模糊匹配查询统计主题
	public List<Topic> findTopicNameByFieldNameAndTopicName(String fieldName,String topicName,String range,User u);
	
	//通过topicId查询topicfield的list拼接成json返回
	public String findTopicFieldByTopicIdToJson(String topicId);
	
	//修改topicfield\同步修改datasheet
	public String changeTopicField(String postId,String fieldSequence,String fieldName, String fieldContent);
	
	//删除topicfield\同步删除datasheet
	public void deleteTopicField(String postId);
	
	//保存topicfield\同步创建datasheet
	public void saveTopicField(String topicId,String fieldSequence,String fieldName,int field_isnull,int field_status, String fieldContent,User u);
	
	//发送附件邮件
	public void sendAttachmentEmails(String topicInfo,MultipartFile file,String system) throws Exception;
	public void sendNormalEmails(String topicInfo,String system) throws Exception;
}

