package com.hundsun.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.hundsun.dao.PrivilegeDAO;
import com.hundsun.entity.OpPrivilege;
import com.hundsun.entity.Privilege;
import com.hundsun.utils.Page;

@Service("privilegeService")
public class PrivilegeService {
	
	public static final int PRIVILEGE_SET=1000;
	public static final int START_STATISTICS=1100;
	public static final int PRIVATE_STATISTICS=1101;
	public static final int CUSTOMER_MODIFY=1102;
	public static final int STATISTICS_MODIFY=1103;
	
	@Resource(name="privilegeDAO")
	private PrivilegeDAO privilegeDAO;
	
	public void setPrivilege(int userId, int privId) {
		OpPrivilege opPriv = new OpPrivilege();
		opPriv.setUserId(userId);
		opPriv.setPrivId(privId);
		privilegeDAO.privilegeSave(opPriv);
	}
	
	public void delPrivilege(int userId, int privId) {
		OpPrivilege opPriv = new OpPrivilege();
		opPriv.setUserId(userId);
		opPriv.setPrivId(privId);
		privilegeDAO.privilegeDelete(opPriv);
	}

	public boolean privJudge(int userId, int privId) {
		if(privilegeDAO.privilegeJudge(userId, privId)) {
			return true;
		}
		return false;
	}
	/**
	 * 获取后台所有的权限
	 * @return
	 */
	public List<Object> getPrivileges(){
		List<Object> privList = privilegeDAO.getPrivileges();
		return privList;
	}
	
	public List<Object> getPrivilegesBySearch(String name){
		List<Object> privList = privilegeDAO.getPrivilegesBySearch(name);
		return privList;
	}
	
	public List<Object> getPrivilegesBySearch(String name, String module){
		List<Object> privList = privilegeDAO.getPrivilegesBySearch(name, module);
		return privList;
	}
	
	public List<Privilege> getModules(){
		List<Privilege> privList = privilegeDAO.getModules();
		return privList;
	}
	/**
	 * 分页进行权限查询
	 * @return
	 */
	public List<Object> getPrivilegesByPage(Page page){
		List<Object> privList = privilegeDAO.getPrivilegesByPage(page);
		return privList;
	}
	/**
	 * 获取数据总行数
	 * @return
	 */
	public int getTotalCount() {
        // TODO Auto-generated method stub
        return privilegeDAO.getTotalCount();
    }
}
