package com.hundsun.dao;

import java.util.List;

import com.hundsun.entity.DataSheet;

public interface DataSheetDAO {
	
	//根据ID查找datasheet
	public DataSheet findById(int id);

	//增删改
	public void save(DataSheet dataSheet);
	public void update(DataSheet dataSheet);
	public void delete(DataSheet dataSheet);
	//hql查询
	public List<DataSheet> findForWriteTopic(int topicId,int customersId,String user);		
	public List<DataSheet> findUnfinishedDataSheet(String system);
	public List<DataSheet> findDataSheetByTopicIdAndSystem(int topicId,String system);	
	public void deleteByTopicIdAndFieldSequence(int topicId ,int fieldSequence);
	public void updateFieldNameByTopicIdAndFieldSequence(int topicId ,int fieldSequence,String fieldName);
	public void updateFieldContentByTopicIdAndFieldSequence(int topicId, int fieldSequence,String fieldContent);
	public void updateFieldSequenceByTopicIdAndFieldSequence(int topicId, int fieldSequence,int fieldSequenceNext);
	public void updateSystemByCusId(int cusId ,String system);
}
