package com.hundsun.dao;

import java.util.List;

import com.hundsun.entity.TopicField;

public interface TopicFieldDAO {
	
	public TopicField findByTopicFieldID(int id);
	public void save(TopicField topicField);
	public void update(TopicField topicField);
	public void delete(TopicField topicField);
	public List<TopicField> getTopicFieldByTopicId(int topicId);
	public List<TopicField> getTopicFieldCompareByFieldSequence(int topicId,int fieldSequence);
	public List<TopicField> getTopicFieldCompareByFieldSequence(int topicId,int fieldSequence,int fieldSequenceBig);
	public List<TopicField> findTopicNameListByFieldName(String fieldName,String topicName,int range);
}
