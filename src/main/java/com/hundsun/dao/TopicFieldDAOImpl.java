package com.hundsun.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.query.Query;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hundsun.entity.TopicField;

@Repository("topicFieldDAO")
@Transactional
public class TopicFieldDAOImpl implements TopicFieldDAO {
	
	@Resource(name="hibernateTemplate")
	private HibernateTemplate template;

	public TopicField findByTopicFieldID(int id) {
		// TODO Auto-generated method stub
		TopicField topicField= new TopicField();
		topicField = (TopicField)template.get(TopicField.class, id);
		return topicField;
	}


	public void save(TopicField topicField) {
		// TODO Auto-generated method stub
		template.save(topicField);
	}

	
	public void update(TopicField topicField) {
		// TODO Auto-generated method stub
		template.update(topicField);
	}

	
	public void delete(TopicField topicField) {
		// TODO Auto-generated method stub
		template.delete(topicField);
	}


	public List<TopicField> getTopicFieldByTopicId(int topicId) {
		// TODO Auto-generated method stub
		String hql = "from TopicField where topic_id =" + topicId + " order by field_sequence";
		Query<TopicField> query = template.getSessionFactory().getCurrentSession().createQuery(hql);
		List<TopicField> list = query.list();
		return list;
	}


	public List<TopicField> findTopicNameListByFieldName(String fieldName,String topicName,int range) {
		// TODO Auto-generated method stub
		String hql = "select distinct new TopicField(a.topic_id) from TopicField as a,Topic as b where a.topic_id = b.id and a.field_name like '%"+fieldName
				+"%' and b.topic_name like '%"+topicName+"%' and b.statistical_range = "+range;
		//需要使用createSQLQuery 方法
		Query<TopicField> query = template.getSessionFactory().getCurrentSession().createQuery(hql);
		List<TopicField> list = new ArrayList<TopicField>();
		list = query.list();
		return list;
	}
	

	public List<TopicField> getTopicFieldCompareByFieldSequence(int topicId,int fieldSequence) {
		// TODO Auto-generated method stub
		String hql = "from TopicField where topic_id = "+topicId+
				" and field_sequence >=" + fieldSequence+" order by field_sequence desc";
		Query<TopicField> query = template.getSessionFactory().getCurrentSession().createQuery(hql);
		List<TopicField> list = query.list();
		return list;
	}

	public List<TopicField> getTopicFieldCompareByFieldSequence(int topicId,int fieldSequence,int fieldSequenceBig) {
		// TODO Auto-generated method stub
		String hql = "from TopicField where topic_id = "+topicId+
				" and  field_sequence >=" + fieldSequence+" and field_sequence<"+fieldSequenceBig+" order by field_sequence desc";
		Query<TopicField> query = template.getSessionFactory().getCurrentSession().createQuery(hql);
		List<TopicField> list = query.list();
		return list;
	}
}
