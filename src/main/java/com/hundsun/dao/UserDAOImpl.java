package com.hundsun.dao;


import java.util.List;

import javax.annotation.Resource;

import org.hibernate.query.Query;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hundsun.entity.User;

@Repository("userDAO")
@Transactional
public class UserDAOImpl implements UserDAO {
	
	@Resource(name="hibernateTemplate")
	private HibernateTemplate hibernateTemplate;

	public User findByUserCode(int userID) {
		User user = null;
		user = (User)hibernateTemplate.get(User.class, userID);
//		Configuration cfg = new Configuration();
//		cfg.configure("hibernate.cfg.xml");
//		
//		SessionFactory factory = cfg.buildSessionFactory();
//		Session session = factory.openSession();
//		user = (User)session.get(User.class, userID);
//		System.out.println(user);
//		session.close();
		return user;
	}
	
	public void save(User user) {
		hibernateTemplate.save("User", user);
	}

	public void update(User user) {
		hibernateTemplate.update(user);
	}

	public void delete(User user) {
		hibernateTemplate.delete(user);
	}

	public List<User> getUsers() {
		String hql = "from User";
		Query<User> query = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery(hql);
		List<User> userList = query.list();
		return userList;
	}
	
	public List<User> findUserByUserName(String userName) {
		String hql = "from User where name = '"+userName+"'";
		Query<User> query = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery(hql);
		List<User> userList = query.list();
		return userList;
	}

}
