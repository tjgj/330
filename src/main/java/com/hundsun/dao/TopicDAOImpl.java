package com.hundsun.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.query.Query;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hundsun.entity.Topic;
import com.hundsun.entity.User;

@Repository("topicDAO")
@Transactional
public class TopicDAOImpl implements TopicDAO {
	
	@Resource(name="hibernateTemplate")
	private HibernateTemplate template;

	public Topic findByTopicID(int id) {
		Topic topic = new Topic();
		topic = (Topic)template.get(Topic.class, id);
		return topic;
	}

	
	public void save(Topic topic) {
		// TODO Auto-generated method stub
		template.save(topic);
		
	}

	
	public void update(Topic topic) {
		// TODO Auto-generated method stub
		template.update(topic);
	}

	
	public void delete(Topic topic) {
		// TODO Auto-generated method stub
		template.delete(topic);
	}


	public List<Topic> getAllNormalTopic(String system) {
		// TODO Auto-generated method stub
		String hql="SELECT t from Topic as t ,User as u where t.statistical_range=1 and t.release_user = u.name and u.department like '"+system+"%' order by t.id desc";
		Query<Topic> query = template.getSessionFactory().getCurrentSession().createQuery(hql);
		List<Topic> topicList = query.list();
		return topicList;
	}
	
	public List<Object> getAllNormalTopicAndUnfinished(String system,String user) {
		// TODO Auto-generated method stub		
		String hql="select t.id, (select count(1) from DataSheet d where d.field_value ='' and d.topic_id=t.id and d.fill_user='"+user
		+"') as num,t.release_time,t.topic_name,t.complete_time from Topic as t ,User as u where t.statistical_range=1 and t.release_user = u.name and u.department like '"
		+system+"%' order by t.id desc";		
		Query query = template.getSessionFactory().getCurrentSession().createQuery(hql);
		List<Object> topicList = query.list();
		return topicList;
	}
	
	
	public List<Topic> getAllExcelTopic(String system) {
		// TODO Auto-generated method stub
		String hql="SELECT t from Topic as t ,User as u where t.statistical_range=2 and t.release_user = u.name and u.department like '"+system+"%' order by t.id desc";
		Query<Topic> query = template.getSessionFactory().getCurrentSession().createQuery(hql);
		List<Topic> topicList = query.list();
		return topicList;
	}
	
	public List<Object> getAllExcelTopicAndMaxChangeTime(String system,String user) {
		// TODO Auto-generated method stub
		String hql="select t.id, ifnull((select max(d.change_time) from DataSheet d where d.topic_id=t.id and d.fill_user='"+user
		+"'),(select max(d.change_time) from DataSheet d where d.topic_id=t.id)) as maxtime,t.release_time,t.topic_name from Topic as t ,Tuser as u where t.statistical_range=2 and t.release_user = u.user_name and u.user_department like '"
		+system+"%' order by t.id desc";	
		Query query = template.getSessionFactory().getCurrentSession().createSQLQuery(hql);
		List<Object> topicList = query.list();
		return topicList;
	}
	
	public List<Topic> findTopicListByIdStr(String topicIdStr,String system){
		// TODO Auto-generated method stub
		String hql="SELECT t from Topic as t ,User as u where t.id in " + topicIdStr +" and t.release_user = u.name and u.department like '"+system+"%'";
		Query<Topic> query = template.getSessionFactory().getCurrentSession().createQuery(hql);
		List<Topic> topicList = query.list();
		return topicList;
	}
	
}
