package com.hundsun.dao;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hundsun.entity.OperationLog;

@Repository("operationLogDAO")
@Transactional
public class OperationLogDAOImpl implements OperationLogDAO {
	
	@Resource(name="hibernateTemplate")
	private HibernateTemplate template;

	public void save(OperationLog operationLog) {
		// TODO Auto-generated method stub
		template.save(operationLog);
	}

	public void update(OperationLog operationLog) {
		// TODO Auto-generated method stub
		template.update(operationLog);
	}

	public void delete(OperationLog operationLog) {
		// TODO Auto-generated method stub
		template.delete(operationLog);
	}

	
}
