package com.hundsun.dao;

import java.util.List;

import com.hundsun.entity.User;

public interface UserDAO {
	
	public User findByUserCode(int userID);
	public void save(User user);
	public void update(User user) ;
	public void delete(User user);
	
	public List<User> getUsers();
	public List<User> findUserByUserName(String userName);
}
