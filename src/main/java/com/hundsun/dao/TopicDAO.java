package com.hundsun.dao;

import java.util.List;


import com.hundsun.entity.Topic;
import com.hundsun.entity.TopicField;

public interface TopicDAO {
	
	//根据主题ID查找主题
	public Topic findByTopicID(int id);
	//获得全部主题
	public List<Topic> getAllNormalTopic(String system);
	public List<Object> getAllNormalTopicAndUnfinished(String system,String user);
	public List<Topic> getAllExcelTopic(String system);
	public List<Object> getAllExcelTopicAndMaxChangeTime(String system,String user);
	public List<Topic> findTopicListByIdStr(String topicIdStr,String system);
	//增删改
	public void save(Topic topic);
	public void update(Topic topic);
	public void delete(Topic topic);
}
