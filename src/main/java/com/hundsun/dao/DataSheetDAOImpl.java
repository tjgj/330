package com.hundsun.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hundsun.entity.Customers;
import com.hundsun.entity.DataSheet;
import com.hundsun.entity.TopicField;;

@Repository("dataSheetDAO")
@Transactional
public class DataSheetDAOImpl implements DataSheetDAO {
	
	@Resource(name="hibernateTemplate")
	private HibernateTemplate template;

	public DataSheet findById(int id) {
		DataSheet dataSheet = new DataSheet();
		dataSheet = (DataSheet)template.get(DataSheet.class, id);
		return dataSheet;
	}

	public void save(DataSheet dataSheet) {
		// TODO Auto-generated method stub
		template.save(dataSheet);
	}

	public void update(DataSheet dataSheet) {
		// TODO Auto-generated method stub
		template.update(dataSheet);
	}

	public void delete(DataSheet dataSheet) {
		// TODO Auto-generated method stub
		template.delete(dataSheet);
	}

	public List<DataSheet> findForWriteTopic(int topicId,int customersId,String user) {
		// TODO Auto-generated method stub
	    String Find_By_Topic_And_User = "from DataSheet where topic_id = " + topicId+" and customer_id = " + customersId +" order by field_sequence";
	    //fill_user = '"+ user+"' and 去掉填表人条件，维护专员跟换后获取不到数据。
	    Query query = template.getSessionFactory().getCurrentSession().createQuery(Find_By_Topic_And_User);
	    List<DataSheet> list = query.list(); 
		return list;
	}
	
	public List<DataSheet> findUnfinishedDataSheet(String system) {
		// TODO Auto-generated method stub
	    String Find_Unfinished_DIS = "select distinct new DataSheet(d.topic_id,d.topic_name,d.fill_user,t.complete_time,t.remark) from DataSheet d,Topic t where d.field_value is null or d.field_value = '' and d.topic_id=t.id and d.system='"+system+"'";
	    Query query = template.getSessionFactory().getCurrentSession().createQuery(Find_Unfinished_DIS);
	    List<DataSheet> list = query.list(); 
		return list;
	}

	public List<DataSheet> findDataSheetByTopicIdAndSystem(int topicId,String system){
		// TODO Auto-generated method stub
	    String Find_By_TopicId = "from DataSheet where system = '"+system+"' and topic_id = " + topicId + " order by customer_id,field_sequence";
	    Query query = template.getSessionFactory().getCurrentSession().createQuery(Find_By_TopicId);
	    List<DataSheet> list = query.list(); 
		return list;
	}

	public void deleteByTopicIdAndFieldSequence(int topicId, int fieldSequence) {
		// TODO Auto-generated method stub
		String hql1 = "SET SQL_SAFE_UPDATES = 0";
		String hql2 = "delete from  DataSheet where topic_id = "+topicId+" and field_sequence = "+fieldSequence;
		//需要使用createSQLQuery 方法
		template.getSessionFactory().getCurrentSession().createSQLQuery(hql1);
		template.getSessionFactory().getCurrentSession().createSQLQuery(hql2).executeUpdate();

	}

	public void updateFieldNameByTopicIdAndFieldSequence(int topicId, int fieldSequence,String fieldName) {
		// TODO Auto-generated method stub
		String hql1 = "SET SQL_SAFE_UPDATES = 0";
		String hql2 = "update  DataSheet set field_name = '"+fieldName+"' where topic_id = "+ topicId + " and field_sequence="+fieldSequence;
		//需要使用createSQLQuery 方法
		template.getSessionFactory().getCurrentSession().createSQLQuery(hql1);
		template.getSessionFactory().getCurrentSession().createSQLQuery(hql2).executeUpdate();
	}
	
	public void updateFieldContentByTopicIdAndFieldSequence(int topicId, int fieldSequence,String fieldContent) {
		// TODO Auto-generated method stub
		String hql1 = "SET SQL_SAFE_UPDATES = 0";
		String hql2 = "update  DataSheet set field_content = '"+fieldContent+"' where topic_id = "+ topicId + " and field_sequence="+fieldSequence;
		//需要使用createSQLQuery 方法
		template.getSessionFactory().getCurrentSession().createSQLQuery(hql1);
		template.getSessionFactory().getCurrentSession().createSQLQuery(hql2).executeUpdate();
	}

	public void updateFieldSequenceByTopicIdAndFieldSequence(int topicId, int fieldSequence, int fieldSequenceNext) {
		// TODO Auto-generated method stub
		String hql1 = "SET SQL_SAFE_UPDATES = 0";
		String hql2 = "update  DataSheet set field_sequence = "+fieldSequenceNext+" where topic_id = "+ topicId + " and field_sequence="+fieldSequence;
		//需要使用createSQLQuery 方法
		template.getSessionFactory().getCurrentSession().createSQLQuery(hql1);
		template.getSessionFactory().getCurrentSession().createSQLQuery(hql2).executeUpdate();
	}
	
	public void updateSystemByCusId(int cusId ,String system) {
		String hql1 = "SET SQL_SAFE_UPDATES = 0";
		String hql2 = "update  DataSheet set system = '"+system+"' where customer_id = "+ cusId;
		//需要使用createSQLQuery 方法
		template.getSessionFactory().getCurrentSession().createSQLQuery(hql1);
		template.getSessionFactory().getCurrentSession().createSQLQuery(hql2).executeUpdate();
	}
}
