package com.hundsun.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.query.Query;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hundsun.entity.OpPrivilege;
import com.hundsun.entity.Privilege;
import com.hundsun.utils.Page;


@Repository("privilegeDAO")
@Transactional
public class PrivilegeDAOImpl implements PrivilegeDAO{

	@Resource(name="hibernateTemplate")
	private HibernateTemplate template;
	
	public boolean privilegeJudge(int userId, int privId) {
		String hql = "from OpPrivilege where userId=" + userId + " and privId=" + privId;
		Query<OpPrivilege> query = template.getSessionFactory().getCurrentSession().createQuery(hql);
		if(query.list().size() > 0) {
			return true;
		}
		return false;
	}

	public List<OpPrivilege> privilegeQuery(int userId) {
		String hql = "from topprivilege where user_id=" + userId;
		Query<OpPrivilege> query = template.getSessionFactory().getCurrentSession().createQuery(hql);
		List<OpPrivilege> opPrivilegeList = query.list();
		return opPrivilegeList;
	}

	public void privilegeSave(OpPrivilege opPrivilege) {
		template.save(opPrivilege);
		
	}

	public void privilegeDelete(OpPrivilege opPrivilege) {
		template.delete(opPrivilege);
		
	}

	public List<Object> getPrivileges() {
		String hql = "from User as a, Privilege as b";
		Query query = template.getSessionFactory().getCurrentSession().createQuery(hql);
		List<Object> privList = query.list();
		return privList;
	}

	public List<Object> getPrivilegesBySearch(String name) {
		String hql = "from User as a, Privilege as b where a.name = '" + name + "'";
		Query query = template.getSessionFactory().getCurrentSession().createQuery(hql);
		List<Object> privList = query.list();
		return privList;
	}

	public List<Object> getPrivilegesBySearch(String name, String module) {
		String hql = "from User as a, Privilege as b where a.name = '" + name + "' and b.sub_system_name = '" + module + "'";
		Query query = template.getSessionFactory().getCurrentSession().createQuery(hql);
		List<Object> privList = query.list();
		return privList;
	}

	public List<Privilege> getModules() {
		String hql = "select distinct sub_system_name from Privilege";
		Query<Privilege> query = template.getSessionFactory().getCurrentSession().createQuery(hql);
		List<Privilege> privList = query.list();
		return privList;
	}
	
	public List<Object> getPrivilegesByPage(Page page){
	    
	    //2.定义查询最大记录数的hql
	    String hql="from User as a, Privilege as b";
	    
	    //3.定义查询最大记录数的Query对象
	    Query querypage=template.getSessionFactory().getCurrentSession().createQuery(hql);
	    
	    //4.查询最大记录数的数据
	    querypage.setMaxResults(page.getPagesize());
	    
	    //5.确定查询起点
	    querypage.setFirstResult(page.getStartrow());
	    
	    //6.分页查询
	    List<Object> list=querypage.list();
	    
	    return list;
	}
	
	public int getTotalCount() {
	    
	    //2.定义查询总条数hql语句
	    String hqlcount="select count(*) from User as a, Privilege as b";
	    
	    //3.利用Session创建Query对象
	    Query querycount=template.getSessionFactory().getCurrentSession().createQuery(hqlcount);
	    
	    //4.获取总条数(返回单行数据uniqueResult())
	    Integer totalCount=Integer.parseInt(querycount.uniqueResult().toString());
	    
	    return totalCount;
	}

}
