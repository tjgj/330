package com.hundsun.dao;

import java.util.List;

import com.hundsun.entity.Customers;;

public interface CustomersDAO {
	
	//根据主题ID查找主题
	public Customers findByCustomersId(int customersId);

	//增删改
	public void save(Customers customer);
	public void update(Customers customer);
	public void delete(Customers customer);
	//hql查询
	public List<Customers> findAllNormalCustomers(String system);
	public List<Customers> findByCommissioner(String firstCommissioner,String secondCommissioner);	

}
