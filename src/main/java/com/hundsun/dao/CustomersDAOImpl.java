package com.hundsun.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hundsun.entity.Customers;

@Repository("customersDAO")
@Transactional
public class CustomersDAOImpl implements CustomersDAO {
	
	@Resource(name="hibernateTemplate")
	private HibernateTemplate template;

	public Customers findByCustomersId(int id) {
		Customers customer = new Customers();
		customer = (Customers)template.get(Customers.class, id);
		return customer;
	}

	public void save(Customers customer) {
		// TODO Auto-generated method stub
		template.save(customer);
	}

	public void update(Customers customer) {
		// TODO Auto-generated method stub
		template.update(customer);
	}

	public void delete(Customers customer) {
		// TODO Auto-generated method stub
		template.delete(customer);
	}

	public List<Customers> findAllNormalCustomers(String system) {
		// TODO Auto-generated method stub
//		Configuration cfg = new Configuration();
//		SessionFactory sessionFactory = cfg.buildSessionFactory();
//		Session session = sessionFactory.getCurrentSession(); 
	    //from后面是对象，不是表名  
	    String Find_All_Customers = "from Customers where system_status = 1 and system like '"+system+"%'";  
	    Query query = template.getSessionFactory().getCurrentSession().createQuery(Find_All_Customers);
//	    Query query = session.createQuery(Find_All_Customers);
	    List<Customers> list = query.list();  
	    return list;
	}
	
	public List<Customers> findByCommissioner(String firstCommissioner,String secondCommissioner) {
		// TODO Auto-generated method stub
	    String FIND_BY_FirstCommissioner = "from Customers where system_status = 1 and first_commissioner ='" + firstCommissioner+
	    		"' or second_commissioner ='"+secondCommissioner+"'";  
	    Query query = template.getSessionFactory().getCurrentSession().createQuery(FIND_BY_FirstCommissioner);
	    List<Customers> list = query.list();
		return list;
	}
	
}
