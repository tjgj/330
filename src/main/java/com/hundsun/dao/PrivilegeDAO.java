package com.hundsun.dao;

import java.util.List;

import com.hundsun.entity.OpPrivilege;
import com.hundsun.entity.Privilege;
import com.hundsun.utils.Page;

public interface PrivilegeDAO {
	public boolean privilegeJudge(int userId, int privId);
	public List<OpPrivilege> privilegeQuery(int userId); 
	public void privilegeSave(OpPrivilege opPrivilege);
	public void privilegeDelete(OpPrivilege opPrivilege);
	public List<Object> getPrivileges();
	/**
	 * 按照名字进行权限查询
	 * @param name 姓名
	 * @return 
	 */
	public List<Object> getPrivilegesBySearch(String name);
	/**
	 * 按照名字和板块进行权限查询
	 * @param name 姓名
	 * @param module 板块
	 * @return
	 */
	public List<Object> getPrivilegesBySearch(String name,String module);
	public List<Privilege> getModules();
	public List<Object> getPrivilegesByPage(Page page);
	public int getTotalCount();
}
