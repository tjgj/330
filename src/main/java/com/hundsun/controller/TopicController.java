package com.hundsun.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.hundsun.entity.User;
import com.hundsun.entity.Topic;
import com.hundsun.entity.TopicField;
import com.hundsun.entity.DataSheet;
import com.hundsun.entity.Customers;
import com.hundsun.service.TopicService;

import com.hundsun.utils.ImportExcelUtil;
import com.hundsun.service.ApplicationException;
import com.hundsun.service.CustomersService;
import com.hundsun.service.PrivilegeService;

@Controller("topicController")
public class TopicController {
	
	@Resource(name="topicService")
	private TopicService topicService;
	@Resource(name="customersService")
	private CustomersService customersService;
	@Resource(name="privilegeService")
	private PrivilegeService privilegeService;

	@ExceptionHandler
	public String handleEx(Exception e,
			HttpServletRequest request){
		System.out.println("handleEx()");
		if(e instanceof ApplicationException){
			//应用异常
			request.setAttribute(
					"customersService_failed",
					e.getMessage());
			return "begin";
		}
		//系统异常
		return "error";
	}
	
	//跳转统计主页
	@RequestMapping("/toTopicIndex.do")
	public String toStatisticIndex(HttpServletRequest request, HttpSession session) throws ParseException {
		User u = (User)session.getAttribute("user");
		String userName = u.getName();
		request.setAttribute("user_name", userName);
		
		return "topicIndex";
	}
	
	//统计主页ajax
	@RequestMapping("/getTopic.do")
	@ResponseBody
	public Object getStatistics(HttpSession session) {
		User u = (User)session.getAttribute("user");
		return topicService.getAllNormalTopicAndUnfinished(u);
	}
	
	//统计主页查询功能toFindField-ajax
	@RequestMapping(value="/toFindField.do")
	@ResponseBody
	public Object toFindField(@RequestParam("field")  String field
			,@RequestParam("topicName")  String topicName
			,@RequestParam("range")  String range, HttpSession session) {	
		User u = (User)session.getAttribute("user");
		return topicService.findTopicNameByFieldNameAndTopicName(field,topicName,range,u);
	}
	
	
	
	//跳转统计大数据
	@RequestMapping("/toBigData.do")
	public String toBigData(HttpServletRequest request, HttpSession session) throws ParseException {
		User u = (User)session.getAttribute("user");
		String userName = u.getName();
		request.setAttribute("user_name", userName);
		
		return "bigData";
	}
	
	//统计大数据ajax
	@RequestMapping("/getBigTopic.do")
	@ResponseBody
	public Object getBigTopic(HttpSession session) {
		User u = (User)session.getAttribute("user");
		return topicService.getAllExcelTopicAndMaxChangeTime(u);
	}
	
	
    //跳转统计编写界面
	@RequestMapping(value="/toWriteTopic.do")
	public String toWriteStatistic(@RequestParam("id")  String topicId,HttpServletRequest request, HttpSession session) {

		int tId = Integer.parseInt(topicId);
		User u = (User)session.getAttribute("user");
		String userName = u.getName();
		Topic topic = topicService.getTopicService(tId);
		
		request.setAttribute("topic_id", topic.getId());
		request.setAttribute("topic_name", topic.getTopic_name());
		request.setAttribute("complete_time", topic.getComplete_time());
		request.setAttribute("user_name", userName);
	    //Map<String,Object> data = new HashMap<String,Object>();  
	    //data.put("user",user);  
		//return new ModelAndView("writeTopic");
		return "writeTopic";
	}
	
	//获取统计编写界加载的客户数量
	@RequestMapping(value="/toGetCustomersForWrite.do")
	@ResponseBody
	public Object getCustomers(HttpServletRequest request, HttpSession session) {
		User u = (User)session.getAttribute("user");
		String userName = u.getName();
		return customersService.getCustomersByCommissionerService(userName,userName);
	}
	
	//创建统计编写界面
	//produces="text/html;charset=UTF-8"去掉前台ajax请求就没有406了，没知道什么鬼
	@RequestMapping(value="/toFillWriteTopic.do")
	@ResponseBody
	public Object buildWriteTopic(@RequestParam("cusId")  String customersId,@RequestParam("topicId")  String topicId,HttpServletRequest request, HttpSession session) {
		User u = (User)session.getAttribute("user");
		String userName = u.getName();
		List<DataSheet> dataSheetList = topicService.getDataSheetForWirteTopic(topicId, userName,customersId);
		return dataSheetList;
	}
	
	//统计信息提交
	@RequestMapping(value="/toSubmitTopicData.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public void saveWriteTopic(@RequestParam (value = "data") String[] postData, HttpSession session)
	{
		User u = (User)session.getAttribute("user");
		String userName = u.getName();
		topicService.updateDataSheetFieldValue(postData,userName);
	}	
	
    //跳转统计查询界面
	@RequestMapping(value="/toCheckTopic.do")
	public String toCheckTopic(@RequestParam("id")  String topicId,HttpServletRequest request, HttpSession session) {

		int tId = Integer.parseInt(topicId);
		User u = (User)session.getAttribute("user");
		String userName = u.getName();
		Topic topic = topicService.getTopicService(tId);
		//获取topic.getTopic_status(),2-非公开情况判断权限,进行判断是否跳转
		if((topic.getTopic_status() == 2) && privilegeService.privJudge(u.getId(), PrivilegeService.PRIVATE_STATISTICS) 
				|| (topic.getTopic_status() == 1)) {
		
			request.setAttribute("topic_id", topic.getId());
			request.setAttribute("topic_name", topic.getTopic_name());
			request.setAttribute("user_name", userName);
			request.setAttribute("topic_remark", topic.getRemark());
			
			return "checkTopic";
		}
		return "redirect:noPrivilege.do";
	}
	
	//统计查询界面数据返回string[][]
	//produces="text/html;charset=UTF-8"去掉前台ajax请求就没有406了，没知道什么鬼
	@RequestMapping(value="/toCheckTopicData.do")
	@ResponseBody
	public Object CheckTopicData(@RequestParam("topicId")  String topicId,HttpServletRequest request, HttpSession session) throws ParseException{
		User u = (User)session.getAttribute("user");
		String[][] strData = topicService.getDataSheetByTopicId(topicId,u);
		return strData;
	}
	
	//跳转统计发起界面
	@RequestMapping("/toNewTopic.do")
	public String toNewTopic(HttpServletRequest request, HttpSession session){
		System.out.println("toTtest()");
		User u = (User)session.getAttribute("user");
		if(privilegeService.privJudge(u.getId(), privilegeService.START_STATISTICS)) {
			String userName = u.getName();
			request.setAttribute("user_name", userName);
			System.out.println("pass");
			return "newTopic";
		}
		return "redirect:noPrivilege.do";
	}
	
     //统计发起界面ajax
	@RequestMapping(value="/postTopic.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String startTopic(@RequestParam (value = "data") String[][] postData
			,@RequestParam("topicName") String postTopicName
			,@RequestParam("completeTime") String postCompleteTime

			,@RequestParam("remark") String postRemark
			,@RequestParam("topicStatus") String postTopicStatus,
			HttpServletRequest request, HttpSession session) throws Exception
	{			



		//统计方式
		String topicRange = "1";
		//统计字段是否可以为空，备用
		String fieldIsnull ="1";
		//统计字段类型，备用
		String fieldStatus = "1";
		//后买有session赋值
		User u = (User)session.getAttribute("user");
		String releaseUser = u.getName();
		String system = u.getDepartment();
	   int topic = topicService.bulidTopicService(postData, postTopicName, postCompleteTime, postRemark, 
			   postTopicStatus, topicRange, fieldIsnull, fieldStatus, releaseUser,system);
			   
		String a = Integer.toString(topic);
		return a;
	}	

    //统计发起界面ajax-success后提交的附件from
	@RequestMapping(value="/uploadAttachment.do",method={RequestMethod.GET,RequestMethod.POST})
	public  String  uploadAttachment(HttpServletRequest request, HttpSession session) throws Exception {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;  
		//System.out.println("通过传统方式form表单提交方式导入excel文件！");
		String topicInfo = request.getParameter("topicInfo");
		MultipartFile file = multipartRequest.getFile("attachment");
		User u = (User)session.getAttribute("user");
		String userName = u.getName();
		String system = "PB";//user增加部门属性
		request.setAttribute("user_name", userName);
		if(file.isEmpty()){
			topicService.sendNormalEmails(topicInfo,system);
		}else {
			topicService.sendAttachmentEmails(topicInfo, file,system);
		}
		
		return "topicIndex";
	}

    //跳转统计修改界面
	@RequestMapping(value="/toChangeTopic.do")
	public String toChangeTopic(@RequestParam("id")  String topicId,HttpServletRequest request, HttpSession session) {

		int tId = Integer.parseInt(topicId);
		User u = (User)session.getAttribute("user");
		String userName = u.getName();
		if(privilegeService.privJudge(u.getId(), privilegeService.STATISTICS_MODIFY)) {
			List<TopicField> topicFieldList = topicService.getTopicFieldByTopicId(tId);
			Topic topic = topicService.getTopicService(tId);
			
			request.setAttribute("topic_id", topic.getId());
			request.setAttribute("topic_name", topic.getTopic_name());
			request.setAttribute("user_name", userName);
			return "changeTopic";
		}
		return "redirect:noPrivilege.do";
	}

	//统计修改主页toChangeTopicFieldData-ajax
	@RequestMapping(value="/toChangeTopicData.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public Object toChangeTopicData(@RequestParam("topicId")  String topicId, HttpSession session) {		
		return topicService.findTopicFieldByTopicIdToJson(topicId);
	}
	
	//保存TopicField
	@RequestMapping(value="/toSaveTopic.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public void toSaveTopic(@RequestParam("postTopicId") String topicId,
			@RequestParam("postFieldSequence") String fieldSequence,
			@RequestParam("postFieldName") String fieldName,
			@RequestParam("postFieldContent") String fieldContent, HttpSession session) {
		User u = (User)session.getAttribute("user");
		topicService.saveTopicField(topicId, fieldSequence, fieldName, 1, 1, fieldContent,u);
	}
	
	//更新TopicField
	@RequestMapping(value="/toUpdateTopic.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public void toUpdateTopic(@RequestParam("postId") String postId,
			@RequestParam("postFieldSequence") String fieldSequence,
			@RequestParam("postFieldName") String fieldName,
			@RequestParam("postFieldContent") String fieldContent) {
		
		topicService.changeTopicField(postId, fieldSequence, fieldName, fieldContent);
	}
	
	//删除TopicField
	@RequestMapping(value="/toDeleteTopic.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public void toDeleteTopic(@RequestParam("postId") String pId) {
		topicService.deleteTopicField(pId);
	}
	
    //统计修改界面结果反馈ajax
	@RequestMapping(value="/toTopicResult.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String toTopicResult(@RequestParam("topicId") String topicId
			,@RequestParam("result") String result,
			HttpServletRequest request, HttpSession session)
	{	
		int tId = Integer.parseInt(topicId);
		if (result.equals("查询"))
		{
			Topic topic = topicService.getTopicService(tId);
			return topic.getResult();
		}else {
			return topicService.setTopicResult(topicId, result);
		}
	}
	
}
