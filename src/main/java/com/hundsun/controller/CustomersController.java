package com.hundsun.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hundsun.entity.Customers;
import com.hundsun.entity.User;
import com.hundsun.service.ApplicationException;
import com.hundsun.service.CustomersService;
import com.hundsun.service.PrivilegeService;


@Controller
public class CustomersController {

	@Resource(name="customersService")
	private CustomersService customersService;
	@Resource(name="privilegeService")
	private PrivilegeService privilegeService;

	
	
	//测试
	@RequestMapping("/toUpload.do")
	public String toUpload(){
		return "uploadExText";
	}
	
	
	@ExceptionHandler
	public String handleEx(Exception e,
			HttpServletRequest request){
		System.out.println("handleEx()");
		if(e instanceof ApplicationException){
			//应用异常
			request.setAttribute(
					"customersService_failed",
					e.getMessage());
			return "begin";
		}
		//系统异常
		return "error";
	}
	
	//进入客户关系网页
	@RequestMapping("/toCustomers.do")
	public String toTtest(HttpServletRequest request, HttpSession session){
		System.out.println("toTtest()");
		User u = (User)session.getAttribute("user");
		String userName = u.getName();
		if(privilegeService.privJudge(u.getId(), privilegeService.CUSTOMER_MODIFY)) {
			request.setAttribute("user_name", userName);
			return "customers";
		}
		return "redirect:noPrivilege.do";
	}
	
//	@RequestMapping("/noPrivilege.do")
//	public void noPrivilege(HttpServletResponse response) {
//		response.setContentType("text/html;charset=utf-8");    
//		try {
//			response.getWriter().write("<script>alert('无此权限，网页将返回我的统计'); window.location='toTopicIndex.do';window.close();</script>");
//			response.getWriter().flush();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
	//展示客户关系数据
	@RequestMapping(value="/toAllCustomer.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String getAllCustomer(HttpSession session) {
		User u = (User)session.getAttribute("user");
		String customersJson = "";
		customersJson = customersService.getAllCustomersToJson(u);
		return customersJson;	
	}	
	
	//保存客户关系
	@RequestMapping(value="/toSaveCustomer.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String saveCustomer(@RequestParam("postCustomerId") String customerId,
			@RequestParam("postCustomerName") String customerName,
			@RequestParam("postCustomerArea") String customerArea,
			@RequestParam("postSystem") String system,
			@RequestParam("postSystemVersion") String systemVersion,
			@RequestParam("postFirstCommissioner") String firstCommissioner,
			@RequestParam("postSecondCommissioner") String secondCommissioner,
			@RequestParam("username")  String userNmae) {
//		String aa = customers.get(0).getCustomerName();
		int systemStatus = 1;//用户状态，备用
		int cusId = Integer.parseInt(customerId);
        int id = customersService.saveCustomerService(cusId, customerName, customerArea, system, 
        		systemVersion, systemStatus, firstCommissioner, secondCommissioner);
		return Integer.toString(id);	
	}
	
	//更新客户关系
	@RequestMapping(value="/toUpdateCustomer.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String updateCustomer(@RequestParam("postId") String postId,
			@RequestParam("postCustomerId") String customerId,
			@RequestParam("postCustomerName") String customerName,
			@RequestParam("postCustomerArea") String customerArea,
			@RequestParam("postSystem") String system,
			@RequestParam("postSystemVersion") String systemVersion,
			@RequestParam("postFirstCommissioner") String firstCommissioner,
			@RequestParam("postSecondCommissioner") String secondCommissioner,
			@RequestParam("username")  String userNmae) {
//		String aa = customers.get(0).getCustomerName();
		int systemStatus = 1;//用户状态，备用
		int cusId = Integer.parseInt(customerId);
		int id = Integer.parseInt(postId);
        int reId = customersService.updateCustomerService(id, cusId, customerName, customerArea, system, 
        		systemVersion, systemStatus, firstCommissioner, secondCommissioner);
		return Integer.toString(reId);	
	}
	
	//删除客户关系
	@RequestMapping(value="/toDeleteCustomer.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public void deleteCustomer(@RequestParam("postId") String pId,
			@RequestParam("username")  String userNmae) {
//		String aa = customers.get(0).getCustomerName();
		//保证后面datasheeet数据的整齐性，将delete改成停用客户，前台不展示。
		int id = Integer.parseInt(pId);
		int systemStatus = 2;
		Customers customer = new Customers();
		customer = customersService.findCustomerById(id);
		customer.setSystem_status(systemStatus);
		customersService.stopCustomerService(customer);
        //int reid = customersService.deleteCustomerService(id);
		//return Integer.toString(reid);	
	}
	
	//Topic编辑界面更新客户信息版本
	@RequestMapping(value="/toChangeVersion.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public void updateCustomerVersion(@RequestParam("cusId") String cid,
			@RequestParam("systemVersion")  String systemVersion,HttpSession session) {
		User u = (User)session.getAttribute("user");
		String changeUserName = u.getName();
		customersService.updateCustomerSystemVersion(cid, systemVersion, changeUserName);
	}
	
}
