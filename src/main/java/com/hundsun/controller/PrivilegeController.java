package com.hundsun.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hundsun.entity.Privilege;
import com.hundsun.entity.User;
import com.hundsun.service.ApplicationException;
import com.hundsun.service.PrivilegeService;
import com.hundsun.utils.Page;

@Controller
public class PrivilegeController {
	
	@Resource(name="privilegeService")
	private PrivilegeService privilegeService;
	
	@ExceptionHandler
	public String handleEx(Exception e,
			HttpServletRequest request){
		System.out.println("handleEx()");
		if(e instanceof ApplicationException){
			//应用异常
			request.setAttribute(
					"customersService_failed",
					e.getMessage());
			return "begin";
		}
		//系统异常
		return "error";
	}

	@RequestMapping("/toPrivilegeSet.do")
	public String toPrivilegeSet(HttpServletRequest request, HttpSession session){
		User u = (User)session.getAttribute("user");
		if(privilegeService.privJudge(u.getId(), privilegeService.PRIVILEGE_SET)) {
			String userName = u.getName();
			request.setAttribute("user_name", userName);
			System.out.println("pass");
			return "privilegeSet";
		}
		return "redirect:noPrivilege.do";
	}
	
	@RequestMapping("/noPrivilege.do")
	public void noPrivilege(HttpServletResponse response) {
		response.setContentType("text/html;charset=utf-8");    
		try {
			response.getWriter().write("<script>alert('无此权限，网页将返回'); window.history.back(-1);window.close();</script>");
			response.getWriter().flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/getPrivilege.do")
	@ResponseBody
	public Object getPrivileges(HttpServletRequest request, HttpServletResponse response) throws Exception {
		/**
         * ☆
         * 在使用ajax往前台通过输出流对象的print方法发送json时
         * 该行获取输出流对象的代码必须放在
         * response.setContentType("text/html;charset=UTF-8");
         * 之后,否则ajax回调时,页面拿到的中文数据就永远都是乱码,
         * 原因是:如果将改行代码写在处理客户端乱码之前,表示编码格式已经确定,
         * 所以,编码格式的处理应该放在获取PrintWriter对象之前
         * 
         */
        PrintWriter out=response.getWriter();
        
        // 2.接受参数
        String no = request.getParameter("pageNo");
        
        int pageSize=10; //页面大小
        int pageNo=1; //默认的pageNo为1
        if(no!=null && no!=""){
            pageNo=Integer.valueOf(no);
        }
        //获取总条数
        int totalCount=privilegeService.getTotalCount();
        //封装分页所需字段
        Page page=new Page(pageSize, pageNo, totalCount);
        //分页查询
        List<Object> list = privilegeService.getPrivilegesByPage(page);
        
        ObjectMapper om=new ObjectMapper();
        String str=om.writeValueAsString(list);
        String pagestr=om.writeValueAsString(page);
        str="{\"page\":"+pagestr+",\"list\":"+str+"}";
        System.out.println(str);
        
        out.print(str);
        /*if (list != null) {
            System.out.println("find page success");
            request.setAttribute("list", list);
            request.setAttribute("page", page);
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } else {
            System.out.println("find page fail!");

        }*/

    
		return privilegeService.getPrivileges();
	}
	
	@RequestMapping("/getModules.do")
	@ResponseBody
	public List<Privilege> getModules() {
		return privilegeService.getModules();
	}
	
	@RequestMapping("/getPrivilegeBySearch.do")
	@ResponseBody
	public Object getPrivilegeBySearch(HttpServletRequest request) {
		String name = request.getParameter("name");
		String module = request.getParameter("module");
		if(module.contains("请选择")) {
			return privilegeService.getPrivilegesBySearch(name);
		}else {
			return privilegeService.getPrivilegesBySearch(name, module);
		}
	}
	
	@RequestMapping("/judgePriv.do")
	@ResponseBody
	public String judgePriv(HttpServletRequest request) {
		String userId = request.getParameter("userId");
		String privId = request.getParameter("privId");
		
		if(privilegeService.privJudge(Integer.parseInt(userId), Integer.parseInt(privId))) {
			return "true";
		}
		return "false";
	}
	
	@RequestMapping("/setPrivilege.do")
	public void setPrivilege(HttpServletRequest request) {
		String userId = request.getParameter("userId");
		String privId = request.getParameter("privId");
		
		privilegeService.setPrivilege(Integer.parseInt(userId), Integer.parseInt(privId));
	}
	
	@RequestMapping("/deletePrivilege.do")
	public void delPrivilege(HttpServletRequest request) {
		String userId = request.getParameter("userId");
		String privId = request.getParameter("privId");
		
		privilegeService.delPrivilege(Integer.parseInt(userId), Integer.parseInt(privId));
	}
	
	
}
