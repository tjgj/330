package com.hundsun.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hundsun.entity.User;
import com.hundsun.service.ApplicationException;
import com.hundsun.service.LoginService;
import com.hundsun.utils.SHAencrypt;

@Controller("loginController")
public class LoginController {

	@Resource(name="loginService")
	private LoginService loginService;
	
	@ExceptionHandler
	public String handleEx(Exception e,
			HttpServletRequest request){
		System.out.println("handleEx()");
		if(e instanceof ApplicationException){
			//应用异常
			request.setAttribute("login_failed", e.getMessage());
//			if(e.getMessage().equals("账号错误")) {
//				request.setAttribute("account_failed", e.getMessage());
//			}else if(e.getMessage().equals("密码错误")) {
//				request.setAttribute("pwd_failed", e.getMessage());
//			}else if(e.getMessage().equals("账号已存在")) {
//				request.setAttribute("account_exits", e.getMessage());
//			}
			System.out.println(e.getMessage());
			return "login";
		}
		e.printStackTrace();
		return "error";
	}
	
	@RequestMapping("/toLogin.do")
	public String toLogin(){
		System.out.println("toLogin()");
		return "login";
	}
	
	@RequestMapping("/login.do")
	public String login(HttpServletRequest request, HttpSession session) throws  Exception{
		//调试用
		System.out.println("login()");
		String func = request.getParameter("request_type");
		System.out.println(func);
		if(func.equals("登录")) {
		//读取帐号，密码
			String user = request.getParameter("userAccount");
			System.out.println("adminCode:"+ user);
			String pwd = request.getParameter("password");
			//调用业务层提供的服务
			User u = loginService.checkLogin(Integer.valueOf(user).intValue(), pwd);
			//登录成功，将一些数据绑订到
			//session对象上。
			session.setAttribute("user", u);
			return "redirect:toIndex.do";
		}else{
			return "redirect:toRegister.do";
		}
	}
	
	@RequestMapping("toRegister.do")
	public String toRegister(HttpServletRequest request, HttpSession session) {
		User u = (User)session.getAttribute("user");
		String userName = u.getName();
		request.setAttribute("user_name", userName);
		return "register";
	}
	
	@RequestMapping("register.do")
	public String register(HttpServletRequest request, HttpSession session) throws Exception {
		//获取表单提交上来的注册信息，并调用注册服务
		String userID = request.getParameter("useraccount");
		String userName = request.getParameter("username");
		String userEmail = request.getParameter("useremail");
		String department = request.getParameter("userdepartment");//后续支持多部门
		String area = request.getParameter("userarea");
		String group = request.getParameter("usergroup");
		User registerUser = loginService.register(Integer.valueOf(userID).intValue(), "123456", userName, userEmail,department, area, group);
		if(registerUser == null) {
			request.setAttribute("warn_message", "账号已存在");
		}
		User u = (User)session.getAttribute("user");
		String uName = u.getName();
		request.setAttribute("user_name", uName);
		return "register";
	}
	
	@RequestMapping("/toIndex.do")
	public String toIndex(HttpServletRequest request, HttpSession session){
		//通过session绑定的对象来获取对象的名字
		User u = (User)session.getAttribute("user");
		String userName = u.getName();
		request.setAttribute("user_name", userName);
		System.out.println("toIndex()");
		return "index";
	}
}
