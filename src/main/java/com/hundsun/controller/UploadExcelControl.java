package com.hundsun.controller;

import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.hundsun.utils.ImportExcelUtil;
import com.hundsun.entity.User;
import com.hundsun.service.CustomersService;
import com.hundsun.service.TopicService;

@Controller
//@RequestMapping("/uploadExcel/*")  
public class UploadExcelControl {
	
	@Resource(name="topicService")
	private TopicService topicService;
	
	@Resource(name="customersService")
	private CustomersService customersService;
	
	
	/**
	 * 描述：通过传统方式form表单提交方式导入excel文件
	 * @param request
	 * @throws Exception
	 */
	@RequestMapping(value="upload.do",method={RequestMethod.GET,RequestMethod.POST})
	public  String  uploadExcel(HttpServletRequest request, HttpSession session) throws Exception {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;  
		//System.out.println("通过传统方式form表单提交方式导入excel文件！");
	    //审批状态备用
		String topicStatus = "1";
		//统计范围备用
		String topicRange = "2";
		//统计字段是否可以为空，备用
		String fieldIsnull ="1";
		//统计字段类型，备用
		String fieldStatus = "1";
		User u = (User)session.getAttribute("user");
		String releaseUser = u.getName();
		String system = u.getDepartment();
		String remark = "EXCEL导入";
				
		InputStream in =null;
		List<List<Object>> listob = null;
		MultipartFile file = multipartRequest.getFile("upfile");
		if(file.isEmpty()){
			throw new Exception("文件不存在！");
		}
 		in = file.getInputStream();
		listob = new ImportExcelUtil().getBankListByExcel(in,file.getOriginalFilename());
		in.close();
		
		String topicName = file.getOriginalFilename();
		String completeTime = "2099-12-12";
		//该处可调用service相应方法进行数据保存到数据库中，现只对数据输出
		topicService.bulidTopicService(listob, topicName, completeTime, remark, 
				topicStatus, topicRange, fieldIsnull, fieldStatus, releaseUser,system);		
				
		request.setAttribute("user_name", releaseUser);
		return "bigData";
	}
	
	/**
	 * 描述：通过传统方式form表单提交方式导入excel文件
	 * @param request
	 * @throws Exception
	 */
	@RequestMapping(value="uploadCustomers.do",method={RequestMethod.GET,RequestMethod.POST})
	public  String  uploadCustomersExcel(HttpServletRequest request, HttpSession session) throws Exception {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;  
		//System.out.println("通过传统方式form表单提交方式导入excel文件！");
				
		InputStream in =null;
		List<List<Object>> listob = null;
		MultipartFile file = multipartRequest.getFile("upfile");
		if(file.isEmpty()){
			throw new Exception("文件不存在！");
		}
 		in = file.getInputStream();
		listob = new ImportExcelUtil().getBankListByExcel(in,file.getOriginalFilename());
		in.close();
		//该处可调用service相应方法进行数据保存到数据库中，现只对数据输出
		customersService.saveCustomerService(listob);	
			
		User u = (User)session.getAttribute("user");
		String releaseUser = u.getName();
		request.setAttribute("user_name", releaseUser);
		return "customers";
	}
}