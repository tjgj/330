package com.hundsun.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hundsun.entity.User;
import com.hundsun.service.ApplicationException;
import com.hundsun.service.UserServiceImpl;

@Controller
public class UserController {

	@Resource(name="userService")
	private UserServiceImpl userService;
	
	@RequestMapping("/getUsers.do")
	@ResponseBody
	public List<User> getUsers(){
		List<User> userList = userService.getUsers();
		return userList;
	}
	
	@ExceptionHandler
	public String handleEx(Exception e,
			HttpServletRequest request){
		System.out.println("handleEx()");
		if(e instanceof ApplicationException){
		}
		e.printStackTrace();
		return "error";
	}
	
	@RequestMapping("/toPwdChange.do")
	public String toPwdChange(HttpServletRequest request, HttpSession session) {
		User u = (User)session.getAttribute("user");
		String userName = u.getName();
		request.setAttribute("user_name", userName);
		return "pwdChange";
	}
	
	@RequestMapping("/pwdChange.do")
	public String pwdChange(HttpServletRequest request, HttpSession session) throws Exception {
		//获取表单中的三个密码
		String oldPassword = request.getParameter("oldPassword");
		String newPassword = request.getParameter("newPassword");
		String newPasswordAgain = request.getParameter("newPasswordAgain");
		User u = (User)session.getAttribute("user");
		request.setAttribute("user_name", u.getName());
		//判断原密码是否正确，不正确即返回
		if(!userService.judgeOldPassword(u, oldPassword)) {
			request.setAttribute("warn_message", "旧登录密码错误");
			return "pwdChange";
		}
		//判断两次输入的密码是否一致，不一致即返回
		if(!userService.changePassword(u, newPassword, newPasswordAgain)) {
			request.setAttribute("warn_message", "两次输入密码不一致");
			
		}else {
		//修改成功
			request.setAttribute("warn_message", "修改成功");
			
		}
		return "pwdChange";
	}
}
