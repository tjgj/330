package com.hundsun.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "operationLog")
public class OperationLog implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8888850463837092422L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
	private int id;
	@Column(name = "operation_user")
	private String operation_user;
	@Column(name = "operation_style")
	private int operation_style;	
	@Column(name = "operation_status")
	private int operation_status;	
    @Column(name = "operation_target")
	private String operation_target;
    @Column(name = "operation_log")
	private String operation_log;
    @Column(name = "operation_time")
	private String operation_time;      
	@Column(name = "remark")
	private String remark;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOperation_user() {
		return operation_user;
	}
	public void setOperation_user(String operation_user) {
		this.operation_user = operation_user;
	}
	public int getOperation_style() {
		return operation_style;
	}
	public void setOperation_style(int operation_style) {
		this.operation_style = operation_style;
	}
	public int getOperation_status() {
		return operation_status;
	}
	public void setOperation_status(int operation_status) {
		this.operation_status = operation_status;
	}
	public String getOperation_target() {
		return operation_target;
	}
	public void setOperation_target(String operation_target) {
		this.operation_target = operation_target;
	}
	public String getOperation_log() {
		return operation_log;
	}
	public void setOperation_log(String operation_log) {
		this.operation_log = operation_log;
	}
	public String getOperation_time() {
		return operation_time;
	}
	public void setOperation_time(String operation_time) {
		this.operation_time = operation_time;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
}
