package com.hundsun.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "customers")
public class Customers implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8421094651803398850L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
	private int id;
	@Column(name = "customer_id")
	private int customer_id;
	@Column(name = "customer_name")
	private String customer_name;	
	@Column(name = "customer_area")
	private String customer_area;	
    @Column(name = "system")
	private String system;
    @Column(name = "system_version")
	private String system_version;
    @Column(name = "system_status")
	private int system_status;      
	@Column(name = "first_commissioner")
	private String first_commissioner;
	@Column(name = "second_commissioner")
	private String second_commissioner;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the customer_id
	 */
	public int getCustomer_id() {
		return customer_id;
	}
	/**
	 * @param customer_id the customer_id to set
	 */
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	/**
	 * @return the customer_name
	 */
	public String getCustomer_name() {
		return customer_name;
	}
	/**
	 * @param customer_name the customer_name to set
	 */
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	/**
	 * @return the customer_area
	 */
	public String getCustomer_area() {
		return customer_area;
	}
	/**
	 * @param customer_area the customer_area to set
	 */
	public void setCustomer_area(String customer_area) {
		this.customer_area = customer_area;
	}
	/**
	 * @return the system
	 */
	public String getSystem() {
		return system;
	}
	/**
	 * @param system the system to set
	 */
	public void setSystem(String system) {
		this.system = system;
	}
	/**
	 * @return the system_version
	 */
	public String getSystem_version() {
		return system_version;
	}
	/**
	 * @param system_version the system_version to set
	 */
	public void setSystem_version(String system_version) {
		this.system_version = system_version;
	}
	/**
	 * @return the system_status
	 */
	public int getSystem_status() {
		return system_status;
	}
	/**
	 * @param system_status the system_status to set
	 */
	public void setSystem_status(int system_status) {
		this.system_status = system_status;
	}
	/**
	 * @return the first_commissioner
	 */
	public String getFirst_commissioner() {
		return first_commissioner;
	}
	/**
	 * @param first_commissioner the first_commissioner to set
	 */
	public void setFirst_commissioner(String first_commissioner) {
		this.first_commissioner = first_commissioner;
	}
	/**
	 * @return the second_commissioner
	 */
	public String getSecond_commissioner() {
		return second_commissioner;
	}
	/**
	 * @param second_commissioner the second_commissioner to set
	 */
	public void setSecond_commissioner(String second_commissioner) {
		this.second_commissioner = second_commissioner;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Customers(int id, int customer_id, String customer_name, String customer_area, String system,
			String system_version, int system_status, String first_commissioner, String second_commissioner) {
		super();
		this.id = id;
		this.customer_id = customer_id;
		this.customer_name = customer_name;
		this.customer_area = customer_area;
		this.system = system;
		this.system_version = system_version;
		this.system_status = system_status;
		this.first_commissioner = first_commissioner;
		this.second_commissioner = second_commissioner;
	}
	public Customers() {
		super();
	}
	

}
