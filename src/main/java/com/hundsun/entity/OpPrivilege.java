package com.hundsun.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "topprivilege")
@IdClass(OpPrivilegeKey.class)
public class OpPrivilege {

    @Column(name = "user_id")
    @Id
	private int userId;
	@Column(name = "priv_id")
	@Id
	private int privId;
	@Column(name = "remark")
	private String remark;
	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}
	/**
	 * @return the privId
	 */
	public int getPrivId() {
		return privId;
	}
	/**
	 * @param privId the privId to set
	 */
	public void setPrivId(int privId) {
		this.privId = privId;
	}
	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public OpPrivilege() {
		super();
	}
	public OpPrivilege(int userId, int privId, String remark) {
		super();
		this.userId = userId;
		this.privId = privId;
		this.remark = remark;
	}
	
	
}
