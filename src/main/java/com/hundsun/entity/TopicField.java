package com.hundsun.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "topicfield")
public class TopicField implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9192125316983900577L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
	private int id;
	@Column(name = "topic_id")
	private int topic_id;
	@Column(name = "field_sequence")
	private int field_sequence;	
	@Column(name = "field_name")
	private String field_name;	
    @Column(name = "field_isnull")
	private int field_isnull;
    @Column(name = "field_status")
	private int field_status;
	@Column(name = "field_content")
	private String field_content;
	
	public TopicField() {
		super();
	}
	
	public TopicField(int topic_id) {
		setTopic_id(topic_id);
	}
	
	public void setTopicField(int topicId,int fieldSequence,String fieldName,int fieldIsnull,int fieldStatus,
			String fieldContent ) {
	        //super();
	        this.topic_id = topicId;
	        this.field_sequence = fieldSequence;
	        this.field_name = fieldName;
	        this.field_isnull = fieldIsnull;
	        this.field_status = fieldStatus;
	        this.field_content = fieldContent;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the field_sequence
	 */
	public int getFieldSequence() {
		return field_sequence;
	}
	/**
	 * @return the name
	 */
	public String getTopicFieldName() {
		return field_name;
	}
	/**
	 * @return the status
	 */
	public int getFieldStatus() {
		return field_status;
	}

	/**
	 * @return the getFieldIsnull
	 */
	public int getFieldIsnull() {
		return field_isnull;
	}
	
	/**
	 * @return the field_content
	 */	
	public String getFieldContent() {
		return field_content;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	
	public int getTopic_id() {
		return topic_id;
	}
	public void setTopic_id(int topic_id) {
		this.topic_id = topic_id;
	}
	public int getField_sequence() {
		return field_sequence;
	}
	public void setField_sequence(int field_sequence) {
		this.field_sequence = field_sequence;
	}
	public String getField_name() {
		return field_name;
	}
	public void setField_name(String field_name) {
		this.field_name = field_name;
	}
	public int getField_isnull() {
		return field_isnull;
	}
	public void setField_isnull(int field_isnull) {
		this.field_isnull = field_isnull;
	}
	public int getField_status() {
		return field_status;
	}
	public void setField_status(int field_status) {
		this.field_status = field_status;
	}
	public String getField_content() {
		return field_content;
	}
	public void setField_content(String field_content) {
		this.field_content = field_content;
	}
	public void setId(int id) {
		this.id = id;
	}	
}
