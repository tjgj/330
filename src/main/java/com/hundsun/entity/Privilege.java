package com.hundsun.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tprivilege")
public class Privilege {

	@Id
    @Column(name = "priv_id")
	private int privId;
	@Column(name = "priv_name")
	private String privName;
	@Column(name = "remark")
	private String remark;
	@Column(name = "sub_module_name")
	private String sub_system_name;
	
	
	/**
	 * @return the privId
	 */
	public int getPrivId() {
		return privId;
	}
	/**
	 * @param privId the privId to set
	 */
	public void setPrivId(int privId) {
		this.privId = privId;
	}
	/**
	 * @return the privName
	 */
	public String getPrivName() {
		return privName;
	}
	/**
	 * @param privName the privName to set
	 */
	public void setPrivName(String privName) {
		this.privName = privName;
	}
	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	/**
	 * @return the sub_system_name
	 */
	public String getSub_system_name() {
		return sub_system_name;
	}
	/**
	 * @param sub_system_name the sub_system_name to set
	 */
	public void setSub_system_name(String sub_system_name) {
		this.sub_system_name = sub_system_name;
	}
	public Privilege() {
		super();
	}
	public Privilege(int privId, String privName, String remark, String sub_system_name) {
		super();
		this.privId = privId;
		this.privName = privName;
		this.remark = remark;
		this.sub_system_name = sub_system_name;
	}
	
	
	
}
