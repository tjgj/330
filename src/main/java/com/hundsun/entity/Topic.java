package com.hundsun.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "topic")
public class Topic implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7352730100319890389L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
	private int id;
	@Column(name = "topic_name")
	private String topic_name;
    @Column(name = "topic_status")
	private int topic_status;
	@Column(name = "release_user")
	private String release_user;
	@Column(name = "release_time")
	private String release_time;
	@Column(name = "complete_time")
	private String complete_time;
    @Column(name = "statistical_range")
	private int statistical_range;
	@Column(name = "remark")
	private String remark;
	@Column(name = "result")
	private String result;
	
	
	public Topic() {
		super();
	}

	public void setTopic(String topicName,int topicStatus,String releaseUser,String releaseTime,
			String completeTime,int statisticalRange,String remark ) {
	        //super();
	        this.topic_name = topicName;
	        this.topic_status = topicStatus;
	        this.release_user = releaseUser;
	        this.release_time = releaseTime;
	        this.complete_time = completeTime;
	        this.statistical_range = statisticalRange;
	        this.remark = remark;
	}
	
	

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the topic_name
	 */
	public String getTopic_name() {
		return topic_name;
	}

	/**
	 * @param topic_name the topic_name to set
	 */
	public void setTopic_name(String topic_name) {
		this.topic_name = topic_name;
	}

	/**
	 * @return the topic_status
	 */
	public int getTopic_status() {
		return topic_status;
	}

	/**
	 * @param topic_status the topic_status to set
	 */
	public void setTopic_status(int topic_status) {
		this.topic_status = topic_status;
	}

	/**
	 * @return the release_user
	 */
	public String getRelease_user() {
		return release_user;
	}

	/**
	 * @param release_user the release_user to set
	 */
	public void setRelease_user(String release_user) {
		this.release_user = release_user;
	}

	/**
	 * @return the release_time
	 */
	public String getRelease_time() {
		return release_time;
	}

	/**
	 * @param release_time the release_time to set
	 */
	public void setRelease_time(String release_time) {
		this.release_time = release_time;
	}

	/**
	 * @return the complete_time
	 */
	public String getComplete_time() {
		return complete_time;
	}

	/**
	 * @param complete_time the complete_time to set
	 */
	public void setComplete_time(String complete_time) {
		this.complete_time = complete_time;
	}

	/**
	 * @return the statistical_range
	 */
	public int getStatistical_range() {
		return statistical_range;
	}

	/**
	 * @param statistical_range the statistical_range to set
	 */
	public void setStatistical_range(int statistical_range) {
		this.statistical_range = statistical_range;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	

}
