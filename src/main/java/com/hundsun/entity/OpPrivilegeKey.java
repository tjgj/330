package com.hundsun.entity;

import java.io.Serializable;




public class OpPrivilegeKey implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    private int userId ;
    
    private int privId ;
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + privId;
		result = prime * result + userId;
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OpPrivilegeKey other = (OpPrivilegeKey) obj;
		if (privId != other.privId)
			return false;
		if (userId != other.userId)
			return false;
		return true;
	} 
    
    
}
