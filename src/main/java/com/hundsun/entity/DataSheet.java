package com.hundsun.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "datasheet")
public class DataSheet implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 867267063197374846L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
	private int id;	
	@Column(name = "topic_id")
	private int topic_id;	
	@Column(name = "topic_name")
	private String topic_name;
	@Column(name = "customer_id")
	private int customer_id;	
    @Column(name = "system")
	private String system;
	@Column(name = "customer_name")
	private String customer_name;
	@Column(name = "fill_user")
	private String fill_user;
	@Column(name = "field_sequence")
	private int field_sequence;
	@Column(name = "field_name")
	private String field_name;
	@Column(name = "field_content")
	private String field_content;
	@Column(name = "field_value")
	private String field_value;
	@Column(name = "change_time")
	private String change_time;
	@Transient
	private String complete_time;
	@Transient
	private String remark;
	
	public DataSheet() {
		super();
	}
	
	public DataSheet(int topic_id,String topic_name,String fill_user,String complete_time,String remark ){
		setTopic_id(topic_id);
		setTopic_name(topic_name);
		setFill_user(fill_user);
		setComplete_time(complete_time);
		setRemark(remark);
		}
	
	public void setDataSheet(int topicId,String topicName,int customerId,String system,
			String customerName,String fillUser,int fieldSequence,String fieldName,String fieldContent,String fieldValue,String change_time ) {
	        //super();
	        this.topic_id = topicId;
	        this.topic_name = topicName;
	        this.customer_id = customerId;
	        this.system = system;
	        this.customer_name = customerName;
	        this.fill_user = fillUser;
	        this.field_sequence = fieldSequence;
	        this.field_name = fieldName;
	        this.field_content = fieldContent;
	        this.field_value = fieldValue;
	        this.change_time = change_time;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTopic_id() {
		return topic_id;
	}

	public void setTopic_id(int topic_id) {
		this.topic_id = topic_id;
	}

	public String getTopic_name() {
		return topic_name;
	}

	public void setTopic_name(String topic_name) {
		this.topic_name = topic_name;
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getFill_user() {
		return fill_user;
	}

	public void setFill_user(String fill_user) {
		this.fill_user = fill_user;
	}

	public int getField_sequence() {
		return field_sequence;
	}

	public void setField_sequence(int field_sequence) {
		this.field_sequence = field_sequence;
	}

	public String getField_name() {
		return field_name;
	}

	public void setField_name(String field_name) {
		this.field_name = field_name;
	}

	public String getField_value() {
		return field_value;
	}

	public void setField_value(String field_value) {
		this.field_value = field_value;
	}

	public String getField_content() {
		return field_content;
	}

	public String getChange_time() {
		return change_time;
	}

	public void setChange_time(String change_time) {
		this.change_time = change_time;
	}

	public void setField_content(String field_content) {
		this.field_content = field_content;
	}

	public String getComplete_time() {
		return complete_time;
	}

	public void setComplete_time(String complete_time) {
		this.complete_time = complete_time;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}


}
